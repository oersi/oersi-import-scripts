from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.DcxLrmiAudience as Audience
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class AoeMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("license", {
            "CCBY4.0": "https://creativecommons.org/licenses/by/4.0/",
            "CCBYSA4.0": "https://creativecommons.org/licenses/by-sa/4.0/",
            "CCBYNC4.0": "https://creativecommons.org/licenses/by-nc/4.0/",
            "CCBYND4.0": "https://creativecommons.org/licenses/by-nd/4.0/",
            "CCBYNCND4.0": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "CCBYNCSA4.0": "https://creativecommons.org/licenses/by-nc-sa/4.0/"
        })

        self.add_mapping("lrt", {
            "video": Lrt.VIDEO,
            "esitys": Lrt.SLIDE,
            "teksti": Lrt.TEXT,
            "simulaatio": Lrt.SIMULATION,
            "peli": Lrt.EDUCATIONAL_GAME,
            "sovellus": Lrt.APPLICATION,
            "audio": Lrt.AUDIO,
            # TODO
            # "opas": Lrt.OTHER, # Guide
            "blogi": Lrt.WEB_PAGE, # Blog
            # "ty\u00f6kalu": Lrt.OTHER, # Tool
            # "projekti": Lrt.OTHER, # Project
            "harjoitus": Lrt.DRILL_AND_PRACTICE,  # exercise
            "kuva": Lrt.IMAGE,  # picture
            # "luentotallenne": "",   # lecture recording
            "kaavio": Lrt.DIAGRAM,  # diagram
            "datasetti": Lrt.DATA,  # data set
            "kokeellinen työskentely": Lrt.EXPERIMENT,  # experimental work
            "sanasto": Lrt.INDEX,  # glossary
            "kartta": Lrt.MAP,  # map

            "activity": Lrt.EXPERIMENT,  # experimental work
            "exercise": Lrt.DRILL_AND_PRACTICE,
            "game": Lrt.EDUCATIONAL_GAME,
            # "lecture": "",   # lecture recording
            "picture": Lrt.IMAGE,
            "presentation": Lrt.SLIDE,
            #"project": "",
            "text": Lrt.TEXT,
            # "tool": "", # tool
            "weblog": Lrt.WEB_PAGE,  # Blog
            # "wiki": "", # wiki
        }, Lrt.OTHER)

        self.add_mapping("audience", {
            "Asiantuntija tai ammattilainen": Audience.PROFESSIONAL,
            # "Erityisopetus": "TODO Special Education",  # TODO Besondere Bildung / Special Education
            "Kansalainen": Audience.GENERAL_PUBLIC,
            "Huoltaja": Audience.PARENT,
            "Ohjaaja tai mentori": Audience.MENTOR,
            "Opetuksen ja kasvatuksen johtaja": Audience.ADMINISTRATOR,
            "Opettaja": Audience.TEACHER,
            "Oppija": Audience.STUDENT,
            "Vertaistutor": Audience.PEER_TUTOR,
            # english
            "Administrator": Audience.ADMINISTRATOR,
            "General Public": Audience.GENERAL_PUBLIC,
            "Mentor": Audience.MENTOR,
            "Parent": Audience.PARENT,
            "Peer tutor": Audience.PEER_TUTOR,
            "Professional": Audience.PROFESSIONAL,
            # "Special Education": "TODO Special Education",  # TODO Besondere Bildung / Special Education
            "Student": Audience.STUDENT,
            "Teacher": Audience.TEACHER,
        }, None)

        self.add_mapping("subject", {
            "https://www.yso.fi/onto/yso/p10746": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p11477": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p12814": Subject.N04_PHILOSOPHY,
            "https://www.yso.fi/onto/yso/p13158": None,
            "https://www.yso.fi/onto/yso/p13762": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p1584": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p16969": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p178": None,
            "https://www.yso.fi/onto/yso/p18355": None,
            "https://www.yso.fi/onto/yso/p19327": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p20245": Subject.N04_PHILOSOPHY,
            "https://www.yso.fi/onto/yso/p20743": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p21012": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "https://www.yso.fi/onto/yso/p21846": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p2339": Subject.N8_ENGINEERING_SCIENCES,
            "https://www.yso.fi/onto/yso/p2445": None,
            "https://www.yso.fi/onto/yso/p25094": Subject.N200_COMPUTER_AND_COMMUNICATION_TECHNOLOGY,
            "https://www.yso.fi/onto/yso/p2518": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p2615": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p2616": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p2630": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p27583": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p28225": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "https://www.yso.fi/onto/yso/p2910": Subject.N200_COMPUTER_AND_COMMUNICATION_TECHNOLOGY,
            "https://www.yso.fi/onto/yso/p2945": None,
            "https://www.yso.fi/onto/yso/p3597": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p415": None,
            "https://www.yso.fi/onto/yso/p4532": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "https://www.yso.fi/onto/yso/p456": Subject.N04_PHILOSOPHY,
            "https://www.yso.fi/onto/yso/p4835": None,
            "https://www.yso.fi/onto/yso/p4887": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p5103": Subject.N32_PSYCHOLOGY,
            "https://www.yso.fi/onto/yso/p5140": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p5461": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p6071": Subject.N160_COMPUTER_LINGUISTICS,
            "https://www.yso.fi/onto/yso/p6621": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p7292": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p7401": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p9885": Subject.N33_EDUCATIONAL_SCIENCES,
            "https://www.yso.fi/onto/yso/p2964": Subject.N06_INFORMATION_AND_LIBRARY_SCIENCES,
            "https://www.yso.fi/onto/yso/p28601": Subject.N30_BUSINESS_AND_ECONOMICS,
            "https://www.yso.fi/onto/yso/p27250": Subject.N71_COMPUTER_SCIENCE,
            "https://www.yso.fi/onto/yso/p21004": Subject.N064_NATURE_CONSERVATION,
            "https://www.yso.fi/onto/yso/p22123": Subject.N30_BUSINESS_AND_ECONOMICS,
            "https://www.yso.fi/onto/yso/p13486": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "https:digipedagogiikka": Subject.N33_EDUCATIONAL_SCIENCES,
            "Kasvatustieteet": Subject.N33_EDUCATIONAL_SCIENCES,  # Erziehungswissenschaften
            "Maatalousalan perustutkinto": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE, # Landwirtschaftliche Berufsqualifikation
            "Maataloustiede": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,  # Agrarwissenschaft
            "3D-valmistusmenetelmän käyttö": Subject.N202_MANUFACTURING_TECHNOLOGY_PRODUCTION_ENGINEERING,  # Einsatz des 3D-Fertigungsverfahrens
            "Ajoneuvoalan ammattitutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING, # Professional qualification in the automotive sector
            "Ajoneuvoalan erikoisammattitutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,    #Specialist vocational qualification in the automotive sector
            "Ajoneuvoalan perustutkinto (Tuleva)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Ammatillisen opettajan/vapaansivistystyön opettajien oman osaamisen kehittäminen": Subject.N33_EDUCATIONAL_SCIENCES,
            "Animal science, dairy science": Subject.N220_DAIRY_FARMING,
            "Arboristin ammattitutkinto (Siirtymäajalla)": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "Autoalan perustutkinto (Siirtymäajalla)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Akuuttihoitotyössä toimiminen": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,  # Arbeiten in der Akutversorgung
            "Arkkitehtuuri": Subject.N013_ARCHITECTURE,  # Architektur
            "Avaruustieteet ja tähtitiede": Subject.N014_ASTROPHYSICS_ASTRONOMY,  # Weltraum und Astronomie
            "Bioenergia-alan ammattitutkinto (Siirtymäajalla)": Subject.N310_RENEWABLE_ENERGY,
            "Biokemia, solu- ja molekyylibiologia": Subject.N025_BIOCHEMISTRY,  # Biochemie, Zell- und Molekularbiologie
            "Biologia": Subject.N026_BIOLOGY,  # Biologie
            "Biolääketieteet": Subject.N300_BIOMEDICINE,  # Biomedizin
            "Demo - luovasti yhdessä": None,  # Demo - gemeinsam kreativ
            "Ekologia": Subject.N42_BIOLOGY,
            "Ekologia, evoluutiobiologia": Subject.N026_BIOLOGY,  # Ökologie, Evolutionsbiologie
            "Eläinlääketiede": Subject.N51_VETERINARY_MEDICINE,  # Veterinärmedizin
            "Elämänkatsomustieto": Subject.N136_RELIGIOUS_STUDIES,  # Leben und Religionswissenschaft
            "Eläintenhoidon ammattitutkinto": None, # animal care
            "Eläintenhoidon erikoisammattitutkinto": None, # animal care
            "Erä- ja luonto-oppaan ammattitutkinto (Siirtymäajalla)": None,
            "Ensihoidossa toimiminen": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,  # Arbeiten in der Notfallversorgung
            "Esitys- ja teatteritekniikan ammattitutkinto (Siirtymäajalla)": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Esitys- ja teatteritekniikan ammattitutkinto": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Esitys- ja teatteritekniikan erikoisammattitutkinto (Siirtymäajalla)": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Esitys- ja teatteritekniikan erikoisammattitutkinto": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Farmasia": Subject.N126_PHARMACY,  # Pharmazie
            "Filosofia": Subject.N127_PHILOSOPHY,  # Philosophie
            "Forestry": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Fysiikka": Subject.N128_PHYSICS,  # Physik
            "Geoinformatiikka": Subject.N71_COMPUTER_SCIENCE,
            "Geomedia - tutki, osallistu ja vaikuta": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Geomedia": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Genetiikka, kehitysbiologia, fysiologia": Subject.N026_BIOLOGY,  # Genetik, Entwicklungsbiologie, Physiologie
            "Geotieteet": Subject.N039_GEOSCIENCES_GENERAL,  # Geowissenschaften
            "GIS": None,
            "Hammaslääketieteet": Subject.N185_DENTISTRY,  # Zahnmedizin
            "Historia": Subject.N068_HISTORY,  # Geschichte
            "Historia ja arkeologia": Subject.N012_ARCHAEOLOGY,  # Geschichte und Archäologie
            "Hitsaus": Subject.N108_METALS_TECHNOLOGY,  # Schweißen
            "Hitsaus- ja levytyöt": Subject.N108_METALS_TECHNOLOGY,  # Schweißen und Blechbearbeitung
            "Hoitotiede": Subject.N234_NURSING_SCIENCE_NURSING_MANAGEMENT,  # Pflege
            "Hotelli-, ravintola- ja catering-alan perustutkinto (Siirtymäajalla)": Subject.N274_TOURISM_MANAGEMENT,  # Bachelor-Abschluss im Hotel-, Restaurant- und Cateringbereich (übergangsweise)
            "Hyvinvoinnin ja toimintakyvyn edistäminen": None,  # Förderung des Wohlbefindens und der Funktionsfähigkeit
            "Intro – kaikki soimaan": None,
            "Jatko-opinnot, työelämä ja tulevaisuus": None,  # Aufbaustudium, Berufsleben und die Zukunft
            "Kartografia": Subject.N280_CARTOGRAPHY,
            "Kasvatus- ja ohjausalan perustutkinto": Subject.N33_EDUCATIONAL_SCIENCES,
            "Kasvatustiede": Subject.N33_EDUCATIONAL_SCIENCES,
            "Kasvintuotanto": Subject.N353_PLANT_PRODUCTION,
            "Kansantaloustiede": Subject.N184_BUSINESS_AND_ECONOMICS,  # Wirtschaft
            "Kansanterveystiede, ympäristö ja työterveys": Subject.N232_HEALTH_SCIENCE_HEALTH_MANAGEMENT,  # Wissenschaft, Umwelt und Gesundheit am Arbeitsplatz
            "Kasvibiologia, mikrobiologia, virologia": Subject.N026_BIOLOGY,  # Pflanzenbiologie, Mikrobiologie, Virologie
            "Kehitysvamma-alan ammattitutkinto": Subject.N190_SPECIAL_NEEDS_EDUCATION,
            "Kemia": Subject.N032_CHEMISTRY,  # Chemie
            "Kemia ja kestävä tulevaisuus": Subject.N40_CHEMISTRY,
            "Kielitieteet": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,  # Linguistik
            "Kiinteistöpalvelujen perustutkinto (Siirtymäajalla)": None,  # Grundqualifikation Immobiliendienstleistungen (übergangsweise)
            "Kirjallisuuden tutkimus": Subject.N188_GENERAL_LITERARY_STUDIES,  # Literarische Forschung
            "Kirurgia, anestesiologia, tehohoito, radiologia": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Chirurgie, Anästhesie, Intensivmedizin, Radiologie
            "Kone- ja tuotantotekniikan perustutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,  # Bachelor-Abschluss in Maschinenbau und Produktionstechnik
            "Kone- ja valmistustekniikka": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,  # Maschinenbau und Fertigungstechnik
            "Kone- ja tuotantotekniikan perustutkinto (Siirtymäajalla)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Kone- ja tuotantotekniikan perustutkinto (Tuleva)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Koneasennus": Subject.N104_MECHANICAL_ENGINEERING,  # Maschinenbau
            "Koneistus": Subject.N224_ENGINEERING_PHYSICS_MECHANICAL_PROCESS_ENGINEERING,  # Mechanismus / Mechanik
            "Korva- nenä- ja kurkkutaudit, silmätaudit": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Hals-, Nasen- und Ohrenkrankheiten, Ophthalmologie
            "Kotieläintiede, maitotaloustiede": Subject.N220_DAIRY_FARMING,  # Tierwissenschaft, Molkereiwirtschaft
            "Kotona asumisen ja elämänhallinnan tukeminen": None,  # Unterstützung für das Wohnen zu Hause und das Lebensmanagement
            "Kotieläintuotanto": Subject.N371_ANIMAL_PRODUCTION,
            "Kotihoito": Subject.N48_HEALTH_SCIENCES_GENERAL,
            "Koulutusteknologia": Subject.N33_EDUCATIONAL_SCIENCES,
            "Kuvataide": Subject.N023_FINE_ARTS_GRAPHICS,  # Bildende Kunst
            "Kuvataide ja muotoilu": Subject.N023_FINE_ARTS_GRAPHICS,  # Bildende Kunst und Design
            "Käsityö": Subject.N007_APPLIED_ART,  # Kunsthandwerk
            "Laboratorioalan perustutkinnon perusteet (Tuleva)": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "Lasten ja nuorten erityisohjaajan ammattitutkinto (Siirtymäajalla)": Subject.N33_EDUCATIONAL_SCIENCES,
            "Laboratorioalan perustutkinto": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Grundlegende Qualifikation in der Laborwissenschaft
            "Laboratorioalan perustutkinto (Siirtymäajalla)": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Bachelor-Abschluss in Laborwissenschaften (übergangsweise)
            "Lakitieto": Subject.N28_LAW,  # Rechtliche Informationen
            "Lapsen ja nuoren mielenterveystaitojen edistäminen": None,  # Förderung der psychischen Gesundheitskompetenz von Kindern und Jugendlichen
            "Lapsen, nuoren ja perheen terveyden ja hyvinvoinnin edistäminen": None,  # Förderung der Gesundheit und des Wohlbefindens von Kindern, Jugendlichen und Familien
            "Lapsi- ja perhetyön perustutkinto (Siirtymäajalla)": None,  # Bachelor-Abschluss in Kinder- und Familienarbeit (übergangsweise)
            "Levytyökeskuksen käyttö": None,  # Verwendung des Plattenarbeitsplatzes
            "Liiketalouden perustutkinto (Siirtymäajalla)": Subject.N021_BUSINESS_ADMINISTRATION,  # Bachelor-Abschluss in Betriebswirtschaftslehre (übergangsweise)
            "Liiketaloustiede": Subject.N021_BUSINESS_ADMINISTRATION,  # Betriebswirtschaft
            "Liikunta": Subject.N22_SPORTS_SPORTS_SCIENCE,  # Sportunterricht
            "Liikuntatiede": Subject.N029_SPORTS_SCIENCE,  # Sportwissenschaft
            "Liikunnan ammattitutkinto (Siirtymäajalla)": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "Liikunnan ja valmennuksen ammattitutkinto": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "Liikunnanohjauksen perustutkinto (Siirtymäajalla)": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "Liikunnanohjauksen perustutkinto (Tuleva)": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "Liikunnanohjauksen perustutkinto": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "Logistiikan perustutkinto": Subject.N210_TRANSPORT_ECONOMICS,
            "Luonto- ja ympäristöalan perustutkinto": Subject.N064_NATURE_CONSERVATION,  # Bachelor-Abschluss in Natur und Umwelt
            "Luonnonhoito": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Luonnonsuojelu": Subject.N064_NATURE_CONSERVATION,
            "Luonto- ja ympäristöalan perustutkinto (Siirtymäajalla)": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Luonto- ja ympäristöalan perustutkinto (Tuleva)": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Luontoalan ammattitutkinto": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Luontoalan erikoisammattitutkinto": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Luontokartoittajan erikoisammattitutkinto (Siirtymäajalla)": Subject.N69_SURVEYING,
            "Luvut ja yhtälöt": Subject.N37_MATHEMATICS,
            "Lääkealan perustutkinto": Subject.N126_PHARMACY,  # Bachelor-Abschluss in Pharmazie
            "Lääkealan perustutkinto (Siirtymäajalla)": Subject.N126_PHARMACY,  # Pharmazeutische Grundqualifikation (übergangsweise)
            "Lääketieteen bioteknologia": Subject.N282_BIOTECHNOLOGY,  # Medizinische Biotechnologie
            "Lääketieteen tekniikka": Subject.N215_HEALTH_TECHNOLOGY,  # Medizintechnik
            "Maailma muutoksessa": None,
            "Maanmittausalan perustutkinto": Subject.N69_SURVEYING,
            "Maantiede": Subject.N44_GEOGRAPHY,
            "Maantieto": Subject.N44_GEOGRAPHY,
            "Maatalousalan ammattitutkinto": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Maatalousalan erikoisammattitutkinto": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Maidontuotanto": Subject.N220_DAIRY_FARMING,
            "Maatalouden bioteknologia": Subject.N282_BIOTECHNOLOGY,  # Landwirtschaftliche Biotechnologie
            "Maatalousalan perustutkinto (Siirtymäajalla)": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,  # Bachelor's Degree in Agriculture
            "Manuaalikoneistus": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,  # Maschinenbau / Manuelle Bearbeitung
            "Matematiikka": Subject.N105_MATHEMATICS,  # Mathematik
            "Matematiikan yhteinen opintokokonaisuus": Subject.N37_MATHEMATICS,
            "Materiaalitekniikka": Subject.N177_MATERIALS_TECHNOLOGY,  # Werkstofftechnik
            "Materials engineering": Subject.N177_MATERIALS_TECHNOLOGY,  # Werkstofftechnik
            "Mathematics": Subject.N105_MATHEMATICS,
            "Matkailualan perustutkinto": Subject.N274_TOURISM_MANAGEMENT,  # Tourismus Bachelor-Abschluss
            "Matkailualan perustutkinto (Siirtymäajalla)": Subject.N274_TOURISM_MANAGEMENT,  # Tourismus-Bachelor-Abschluss (übergangsweise)
            "Media- ja viestintätieteet": Subject.N302_MEDIA_SCIENCE,  # Medien- und Kommunikationswissenschaften
            "Mekanisoitu ja automatisoitu hitsaus": Subject.N108_METALS_TECHNOLOGY,  # Mechanisiertes und automatisiertes Schweißen
            "Metsien hoito ja puunkorjuu": Subject.N59_FORESTRY_WOOD_SCIENCE,  # Forstwirtschaft und Holzeinschlag
            "Metsäalan perustutkinto": Subject.N058_FOREST_SCIENCE_FORESTRY,  # Bachelor of Forestry / Forstliche Grundqualifikation
            "Metsäalan perustutkinto (Siirtymäajalla)": Subject.N058_FOREST_SCIENCE_FORESTRY,  # Forstliche Grundqualifikation (übergangsweise)
            "Metsän hoito ja hyödyntäminen": Subject.N075_WOOD_SCIENCE,  # Waldbewirtschaftung und -ausbeutung
            "Metsänhoitotyöt": Subject.N058_FOREST_SCIENCE_FORESTRY,  # Forstwirtschaftliche Arbeiten
            "Metsätiede": Subject.N058_FOREST_SCIENCE_FORESTRY,  # Forstwirtschaft
            "Metsätiedon keruu ja sen hyödyntäminen": Subject.N058_FOREST_SCIENCE_FORESTRY,  # Erfassung und Nutzung von Waldinformationen
            "Metsäalan ammattitutkinto": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsäalan erikoisammattitutkinto": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsäalan perustutkinto (Tuleva)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsäekologia": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsäkoneenkuljettajan ammattitutkinto (Siirtymäajalla)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsämestarin erikoisammattitutkinto (Siirtymäajalla)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Metsätalousyrittäjän ammattitutkinto (Siirtymäajalla)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Mielenterveys- ja päihdetyössä työskenteleminen": Subject.N232_HEALTH_SCIENCE_HEALTH_MANAGEMENT,  # Mental health and substance abuse work
            "Minä ja hyvä elämä": None,  # Ich und das gute Leben
            "Minä ja yhteiskunta": None,  # Ich und die Gesellschaft
            "Minä opiskelijana": None,  # Ich als Student
            "Musiikki": Subject.N78_MUSIC_MUSICOLOGY,  # Musik
            "Musiikki viestii ja vaikuttaa": Subject.N78_MUSIC_MUSICOLOGY,  # Musik kommuniziert und beeinflusst
            "Musiikkialan perustutkinto (Siirtymäajalla)": Subject.N78_MUSIC_MUSICOLOGY,
            "Musiikkialan perustutkinto (Tuleva)": Subject.N78_MUSIC_MUSICOLOGY,
            "Musiikkialan toimintaympäristön tunteminen": Subject.N78_MUSIC_MUSICOLOGY,
            "Musiikkialan toimintaympäristössä toimiminen": Subject.N78_MUSIC_MUSICOLOGY,
            "Musiikkialan yrittäjänä toimiminen": Subject.N78_MUSIC_MUSICOLOGY,
            "Musiikkituotannon ammattitutkinto": Subject.N78_MUSIC_MUSICOLOGY,
            "Muu tekniikka": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,  # Andere Technologie
            "Muut humanistiset tieteet": Subject.N01_HUMANITIES_GENERAL,  # Andere Geisteswissenschaften
            "Muut luonnontieteet": Subject.N36_MATHEMATICS_NATURAL_SCIENCES_GENERAL,  # Andere Naturwissenschaften
            "Muut maataloustieteet": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,  # Andere Agrarwissenschaften
            "Muut yhteiskuntatieteet": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,  # Andere Sozialwissenschaften
            "Myynti ja markkinointi musiikkialalla": Subject.N78_MUSIC_MUSICOLOGY,
            "Naisten- ja lastentaudit": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Gynäkologische und pädiatrische Erkrankungen
            "Nanoteknologia": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,  # Nanotechnologie
            "Neurologia ja psykiatria": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Neurologie und Psychiatrie
            "Neurotieteet": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Neurosciences
            "Nuoriso- ja vapaa-ajanohjauksen perustutkinto (Siirtymäajalla)": Subject.N27_APPLIED_SOCIAL_SCIENCE,
            "Ohutlevytyöt": None,  # Dünnblecharbeiten
            "Oikeuslääketiede ja muut lääketieteet": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Forensische und andere Medizin
            "Oikeustiede": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Forensische Wissenschaft
            "Opastuspalvelut": None,  # Anleitungsdienste
            "Opinto-ohjaus": None,  # Guidance
            "Opiskelu- ja urasuunnitteluvalmiudet": None,  # Fähigkeiten zur Studien- und Karriereplanung
            "Oppilaanohjaus": None,  # Schülerführung
            "Paikkatieto": Subject.N44_GEOGRAPHY,
            "Paikkatieto-ohjelma": Subject.N44_GEOGRAPHY,
            "Paikkatietoaineistot": Subject.N44_GEOGRAPHY,
            "Pedagogik": Subject.N33_EDUCATIONAL_SCIENCES,
            "Plant biology, microbiology, virology": Subject.N42_BIOLOGY,
            "Prosessiteollisuuden erikoisammattitutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Prosessiteollisuuden perustutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Psykologia": Subject.N132_PSYCHOLOGY,  # Psychologie
            "Puhtaus- ja kiinteistöpalvelualan perustutkinto": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Puualan perustutkinto (Siirtymäajalla)": Subject.N197_TIMBER_CONSTRUCTION,  # Hochschulabschluss in Holzbearbeitung (im Übergang)
            "Puunkorjuun erikoisammattitutkinto (Siirtymäajalla)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Puutarha-alan perustutkinto": Subject.N060_HORTICULTURE,  # Grundlegendes Gartenbaudiplom
            "Puutarha-alan perustutkinto (Siirtymäajalla)": Subject.N060_HORTICULTURE,  # Horticulture
            "Puutarha-alan perustutkinto (Tuleva)": Subject.N060_HORTICULTURE,  # Horticultural undergraduate degree
            "Puutarhatalouden perustutkinto (Siirtymäajalla)": Subject.N060_HORTICULTURE,  # Bachelorstudiengang Gartenbau (übergangsweise)
            "Puuteollisuuden perustutkinto": Subject.N197_TIMBER_CONSTRUCTION,  # Bachelor-Abschluss in Holzbearbeitung
            "Puuteollisuuden perustutkinto (Tuleva)": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "QGIS": None,
            "Rakennus- ja yhdyskuntatekniikka": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Hoch- und Tiefbau
            "Rakennusalan perustutkinto (Tuleva)": Subject.N68_CIVIL_ENGINEERING,
            "Rakennusalan perustutkinto": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Bachelor-Abschluss im Bauwesen
            "Rakennusalan perustutkinto (Siirtymäajalla)": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Bachelor-Abschluss im Bauwesen (übergangsweise)
            "Rakennusalan työmaajohdon erikoisammattitutkinto": Subject.N68_CIVIL_ENGINEERING,
            "Rakennusalan työmaapäällikön erikoisammattitutkinto (Siirtymäajalla)": Subject.N68_CIVIL_ENGINEERING,
            "Rakennustuotannon erikoisammattitutkinto (Siirtymäajalla)": None,
            "Ravintola- ja catering-alan perustutkinto": None,  # Bachelor-Abschluss in Restaurant und Catering
            "Riistamestarin erikoisammattitutkinto (Siirtymäajalla)": None,
            "Robotin käyttö": None,  # Verwendung des Roboters
            "Saattohoidossa toimiminen": None,  # Confinement
            "Sairaanhoitotyössä toimiminen": Subject.N234_NURSING_SCIENCE_NURSING_MANAGEMENT,  # Pflege
            "Sociologi": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Sosiaali- ja terveysala": Subject.N253_APPLIED_SOCIAL_SCIENCE,  # Sozial- und Gesundheitssektor
            "Sosiaali- ja terveysalan perustutkinto": Subject.N253_APPLIED_SOCIAL_SCIENCE,  # Bachelor-Abschluss im Sozial- und Gesundheitswesen
            "Sosiaali- ja terveysalan perustutkinto (Tuleva)": Subject.N27_APPLIED_SOCIAL_SCIENCE,
            "Sosiaali- ja terveysalan perustutkinto (Siirtymäajalla)": Subject.N253_APPLIED_SOCIAL_SCIENCE,  # Bachelor-Abschluss im Sozial- und Gesundheitswesen (übergangsweise)
            "Sosiaali- ja yhteiskuntapolitiikka": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,  # Soziales und Sozialpolitik
            "Sosiaaliala, hoitotyö, fysioterapia, kuntoutus": Subject.N208_SOCIAL_WORK,  # Sozialarbeit, Pflege, Physiotherapie, Rehabilitation
            "Sosiologia": Subject.N149_SOCIOLOGY,  # Soziologie
            "Suomi, Eurooppa ja muuttuva maailma": Subject.N05_HISTORY,
            "Suomalainen yhteiskunta": Subject.N036_OTHER_REGIONAL_STUDIES,  # Finnische Gesellschaft
            "Syöpätaudit": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Krebs
            "Sähkö": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Sähkö- ja automaatioalan ammattitutkinto": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Sähkö- ja automaatioalan perustutkinto (Siirtymäajalla)": Subject.N048_ELECTRICAL_ENGINEERING_ELECTRONICS,  # Bachelorstudiengang Elektrotechnik und Automatisierungstechnik (übergangsweise)
            "Sähkö- ja automaatioalan perustutkinto (Tuleva)": Subject.N048_ELECTRICAL_ENGINEERING_ELECTRONICS,  # Bachelor-Abschluss in Elektro- und Automatisierungstechnik (Zukunft)
            "Sähkö- ja automaatiotekniikan perustutkinto (Siirtymäajalla)": Subject.N048_ELECTRICAL_ENGINEERING_ELECTRONICS,  # Bachelor-Abschluss in Elektro- und Automatisierungstechnik (übergangsweise)
            "Sähkö-, automaatio- ja tietoliikennetekniikka, elektroniikka": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,  # Elektro-, Automatisierungs- und Kommunikationstechnik, Elektronik
            "Talonrakennusalan ammattitutkinto": Subject.N68_CIVIL_ENGINEERING,
            "Talonrakennusalan erikoisammattitutkinto": Subject.N68_CIVIL_ENGINEERING,
            "Talotekniikan perustutkinto": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Bachelor-Abschluss in Gebäudetechnik
            "Talotekniikan perustutkinto (Siirtymäajalla)": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Bachelor-Abschluss in Gebäudetechnik (übergangsweise)
            "Taloustieto": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Tanssialan perustutkinto (Siirtymäajalla)": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Tanssialan perustutkinto (Tuleva)": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Tanssialan perustutkinto": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Teatteri, tanssi, musiikki, muut esittävät": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,  # Theater, Tanz, Musik, andere darstellende Künste
            "Teknillinen kemia, kemian prosessitekniikka": Subject.N033_CHEMICAL_ENGINEERING_CHEMICAL_PROCESS_ENGINEERING,  # Technische Chemie, chemische Verfahrenstechnik
            "Teknisen suunnittelun perustutkinto": Subject.N61_ENGINEERING_GENERAL,  # Bachelor-Abschluss in Ingenieurwesen
            "Teknisen suunnittelun perustutkinto (Siirtymäajalla)": Subject.N61_ENGINEERING_GENERAL,  # Bachelor of Engineering
            "Tekstiili- ja muotialan perustutkinto": Subject.N116_TEXTILE_DESIGN,  # Bachelor-Abschluss in Textil und Mode
            "Tekstiili- ja muotialan perustutkinto (Tuleva)": Subject.N116_TEXTILE_DESIGN,
            "Teollinen bioteknologia": Subject.N282_BIOTECHNOLOGY,  # Industrielle Biotechnologie
            "Teologia": Subject.N02_PROTESTANT_THEOLOGY_PROTESTANT_RELIGIOUS_EDUCATION,  # Theologie
            "Terveysalan ammattitutkinto": Subject.N48_HEALTH_SCIENCES_GENERAL,
            "Terveystiede": Subject.N232_HEALTH_SCIENCE_HEALTH_MANAGEMENT,  # Gesundheitswissenschaft
            "Terveystieto": Subject.N195_HEALTH_EDUCATION,  # Gesundheitserziehung
            "Teräsrakennetyöt": Subject.N017_CIVIL_ENGINEERING_STRUCTURAL_ENGINEERING,  # Stahlhochbau
            "Tieto- ja kirjastopalvelujen ammattitutkinto": Subject.N06_INFORMATION_AND_LIBRARY_SCIENCES,
            "Tieto- ja viestintätekniikan perustutkinto (Tuleva)": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "Tieto- ja viestintätekniikan perustutkinto": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,  # Bachelor Informations- und Kommunikationstechnik
            "Tieto- ja viestintätekniikan perustutkinto (Siirtymäajalla)": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,  # Bachelor Informations- und Kommunikationstechnik (übergangsweise)
            "Tietojenkäsittely ja informaatiotieteet": Subject.N200_COMPUTER_AND_COMMUNICATION_TECHNOLOGY,  # Computer- und Informationswissenschaften
            "Tilastotiede": Subject.N312_STATISTICS,  # Statistik
            "Toiminta digitaalisessa ympäristössä": None,
            "Toimintakyvyn ylläpitäminen ja edistäminen vammaistyössä": None,  # Erhaltung und Förderung der Funktionsfähigkeit in der Behindertenarbeit
            "Tulevaisuus": None,  # Zukunft
            "Tulevaisuustaidot": None,  # Zukünftige Fähigkeiten
            "Turvallisuusalan perustutkinto": None,  # Grundlegende Qualifikation in der Sicherheit
            "Turvallisuusalan perustutkinto (Siirtymäajalla)": None,  # Grundqualifikation Sicherheit
            "Työelämässä toimiminen": None,
            "Työelämätaidot": None,  # Fähigkeiten im Arbeitsleben
            "Työtehtävän suunnittelu": None,
            "Valmennuksen erikoisammattitutkinto": None,
            "Valtio-oppi, hallintotiede": Subject.N129_POLITICAL_SCIENCE,  # Politikwissenschaft, Verwaltung
            "Vammaisalan ammattitutkinto": Subject.N190_SPECIAL_NEEDS_EDUCATION,
            "Verkostotyö": None,  # Networking
            "Välinehuoltoalan perustutkinto": None,  # Grunddiplom in Gerätewartung
            "Viheralan ammattitutkinto (Siirtymäajalla)": Subject.N060_HORTICULTURE,
            "Viheralan erikoisammattitutkinto (Siirtymäajalla)": Subject.N060_HORTICULTURE,
            "Yhteinen maailma": None,
            "Yhteiskunta- ja työelämäosaaminen": None,  # Soziale Fähigkeiten und Fähigkeiten im Arbeitsleben
            "Yhteiskuntamaantiede, talousmaantiede": Subject.N178_ECONOMIC_SOCIAL_GEOGRAPHY,  # Sozioökonomische Geographie
            "Yhteiskuntaoppi": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,  # Sozialwissenschaften
            "Yleislääketiede, sisätaudit ja muut kliiniset lääketieteet": Subject.N107_MEDICINE_GENERAL_MEDICINE,  # Allgemeine, innere und andere klinische Medizin
            "Ympäristöalan ammattitutkinto": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Ympäristöalan erikoisammattitutkinto (Siirtymäajalla)": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Ympäristöalan erikoisammattitutkinto": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Ympäristön bioteknologia": Subject.N282_BIOTECHNOLOGY,  # Umweltbiotechnologie
            "Ympäristöoppi": Subject.N458_ENVIRONMENTAL_PROTECTION,  # Umweltwissenschaft
            "Ympäristötekniikka": Subject.N457_ENVIRONMENTAL_TECHNOLOGY_INCLUDING_RECYCLING,  # Umwelttechnik
            "Ympäristötiede": Subject.N457_ENVIRONMENTAL_TECHNOLOGY_INCLUDING_RECYCLING,  # Umweltwissenschaft
            "Yrittäjyys ja yrittäjämäinen toiminta": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Yrityksessä toimiminen": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Yritystoiminnan suunnittelu": Subject.N30_BUSINESS_AND_ECONOMICS,
            "biologia": Subject.N42_BIOLOGY,
            "ekologia": Subject.N42_BIOLOGY,
            "esiintyminen": None,
            "esitystekniikka": None,
            "filosofia": Subject.N127_PHILOSOPHY,  # Philosophie
            "informaatioala": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "innovaatiot": None,
            "kasvatustiede": Subject.N33_EDUCATIONAL_SCIENCES,
            "kasvihuone": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "kasvintuotanto": Subject.N353_PLANT_PRODUCTION,
            "kasvituotanto": Subject.N353_PLANT_PRODUCTION,
            "katsomuskasvatuksen pedagogiikka": Subject.N544_PROTESTANT_RELIGIOUS_PEDAGOGY_CHURCH_EDUCATIONAL_WORK,
            "katsomuskasvatus": None,
            "kasvatustieteet": Subject.N33_EDUCATIONAL_SCIENCES,  # Bildung
            "kirjastoala": Subject.N262_LIBRARIANSHIP,
            "kotiekäintuotanto": None,
            "kotihoito": Subject.N48_HEALTH_SCIENCES_GENERAL,
            "koulutusteknologia": Subject.N33_EDUCATIONAL_SCIENCES,
            "kulttuuri": Subject.N14_CULTURAL_STUDIES_IN_THE_NARROWER_SENSE,
            "käsikirjoittaminen": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "lajintuntemus": Subject.N42_BIOLOGY,
            "lannoitus": Subject.N42_BIOLOGY,
            "liikenne": Subject.N268_TRANSPORTATION_SYSTEMS,
            "liikunnanohjaus": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "liikunnanopettaja": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "liikunta": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "liikunta-ala": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "luomutuotanto": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "maaseutuyrittäminen": Subject.N30_BUSINESS_AND_ECONOMICS,
            "maidontuotanto": Subject.N220_DAIRY_FARMING,
            "matematiikka": Subject.N37_MATHEMATICS,
            "matkailu": Subject.N274_TOURISM_MANAGEMENT,
            "monialaisuus": None,  # Multidisziplinarität
            "moniammatillinen työ": None,  # multidisziplinäres Arbeiten
            "musiikin teoria": Subject.N78_MUSIC_MUSICOLOGY,
            "musiikki": Subject.N78_MUSIC_MUSICOLOGY,
            "opiskelu- ja urasuunnitteluvalmiudet": None,  # Fähigkeiten zur Studien- und Karriereplanung
            "opo-opinnot, uraohjauksen suuntautumisvaihtoehto, opettajankoulutus": None,  # Lehrplanstudium, Berufsorientierung, Lehrerausbildung
            "opettajat": Subject.N33_EDUCATIONAL_SCIENCES,
            "opetus": Subject.N33_EDUCATIONAL_SCIENCES,
            "opinto-ohjaus": None,
            "opintojen ohjaus": None,
            "osaamisen kehittäminen": None,
            "osaamistarpeet": None,
            "pedagogiikka": Subject.N33_EDUCATIONAL_SCIENCES,
            "peliala": None,
            "robotiikka": Subject.N8_ENGINEERING_SCIENCES,
            "sosiaali- ja terveysala": Subject.N253_APPLIED_SOCIAL_SCIENCE,  # Sozial- und Gesundheitsbereich
            "suomen kieli": Subject.N1_HUMANITIES,
            "suomi toisena kielenä": Subject.N1_HUMANITIES,
            "suomi vieraana kielenä": Subject.N1_HUMANITIES,
            "tapahtumat": None,
            "teknologia": None,
            "teknologinen kehitys": None,
            "tekniikka": Subject.N201_HANDICRAFTS_TECHNICAL_TECHNOLOGY,  # Technologie
            "tietojenkäsittely- ja informaatiotieteet": Subject.N200_COMPUTER_AND_COMMUNICATION_TECHNOLOGY,  # Computer- und Informationswissenschaften
            "tieto- ja viestintätekniikka": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "tietopalveluala": None,
            "tulevaisuusvalmiudet": None,  # zukünftige Fähigkeiten
            "tutkimusaineistonhallinta": None,  # Forschungsdatenmanagement
            "tutkimusetiikka": None,  # Forschungsethik
            "työelämätaidot": None,  # Work-Life-Skills,
            "uskonnonpedagogiikka": Subject.N544_PROTESTANT_RELIGIOUS_PEDAGOGY_CHURCH_EDUCATIONAL_WORK,
            "valmentaminen": None,
            "varhaiskasvatus": Subject.N365_EARLY_CHILDHOOD_EDUCATION,
            "varhaiskasvatustiede": Subject.N365_EARLY_CHILDHOOD_EDUCATION,
            "vertikaaliviljely": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "viestintä": Subject.N303_COMMUNICATION_SCIENCE_JOURNALISM,
            "virtuaalitapahtumat": None,

            "Agronomy": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "Biochemistry, cell and molecular biology": Subject.N025_BIOCHEMISTRY,
            "Biomedicine": Subject.N300_BIOMEDICINE,
            "Business and management": Subject.N184_BUSINESS_AND_ECONOMICS,
            "Civil and Construction engineering": Subject.N68_CIVIL_ENGINEERING,
            "Computer and information sciences": Subject.N079_COMPUTER_SCIENCE,
            "Economics": Subject.N184_BUSINESS_AND_ECONOMICS,
            "Educational sciences": Subject.N33_EDUCATIONAL_SCIENCES,
            "Electronic, automation and communications engineering, electronics": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Environmental sciences": Subject.N457_ENVIRONMENTAL_TECHNOLOGY_INCLUDING_RECYCLING,
            "General medicine, internal medicine and other clinical medicine": Subject.N49_HUMAN_MEDICINE_EXCL_DENTISTRY,
            "Genetics, developmental biology, physiology": Subject.N026_BIOLOGY,
            "Health care science": Subject.N232_HEALTH_SCIENCE_HEALTH_MANAGEMENT,
            "Industrial biotechnology": Subject.N282_BIOTECHNOLOGY,
            "Law": Subject.N28_LAW,
            "Media and communications": Subject.N3_LAW_ECONOMICS_AND_SOCIAL_SCIENCES,
            "Medical biotechnology": Subject.N282_BIOTECHNOLOGY,
            "Nursing": Subject.N234_NURSING_SCIENCE_NURSING_MANAGEMENT,
            "Other social sciences": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Pharmacy": Subject.N126_PHARMACY,
            "Physical sciences": Subject.N128_PHYSICS,
            "Political science": Subject.N25_POLITICAL_SCIENCE,
            "Programming": Subject.N71_COMPUTER_SCIENCE,
            "Psychology": Subject.N32_PSYCHOLOGY,
            "Public health care science, environmental and occupational health": Subject.N232_HEALTH_SCIENCE_HEALTH_MANAGEMENT,
            "Social and economic geography": Subject.N44_GEOGRAPHY,
            "Social policy": Subject.N27_APPLIED_SOCIAL_SCIENCE,
            "Sociology": Subject.N149_SOCIOLOGY,
            "Software Development": Subject.N71_COMPUTER_SCIENCE,
            "Visual arts and design": Subject.N023_FINE_ARTS_GRAPHICS,
            "events": None,
            "innovations": None,
            "marketing": Subject.N30_BUSINESS_AND_ECONOMICS,
            "service management": Subject.N30_BUSINESS_AND_ECONOMICS,

            "Ekologian perusteet": Subject.N42_BIOLOGY,
            "Elämä ja evoluutio": Subject.N42_BIOLOGY,
            "Energia-alan erikoisammattitutkinto": Subject.N211_ENERGY_PROCESS_ENGINEERING,
            "Geomedia – tutki, osallistu ja vaikuta": Subject.N039_GEOSCIENCES_GENERAL,
            "Ihmisen vaikutukset ekosysteemeihin": Subject.N42_BIOLOGY,
            "Matematiikan opetus": Subject.N37_MATHEMATICS,
            "Metsänhoito": Subject.N59_FORESTRY_WOOD_SCIENCE,
            "Sininen planeetta": None,
            "Tuotantotekniikan erikoisammattitutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Tuotekehitystyön erikoisammattitutkinto": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "biodiversiteetti": Subject.N42_BIOLOGY
        }, None)
