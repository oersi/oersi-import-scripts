import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.DcxLrmiAudience as Audience
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class PhetMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("subject", {
            "Biology": Subject.N42_BIOLOGY,
            "Chemistry": Subject.N032_CHEMISTRY,
            "Earth Science": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Electricity, Magnets & Circuits": Subject.N048_ELECTRICAL_ENGINEERING_ELECTRONICS,
            "General Chemistry": Subject.N40_CHEMISTRY,
            "Heat & Thermo": Subject.N128_PHYSICS,
            "Light & Radiation": Subject.N128_PHYSICS,
            "Math Applications": Subject.N105_MATHEMATICS,
            "Math Concepts": Subject.N105_MATHEMATICS,
            "Math": Subject.N105_MATHEMATICS,
            "Motion": Subject.N128_PHYSICS,
            "Physics": Subject.N128_PHYSICS,
            "Quantum Chemistry": Subject.N40_CHEMISTRY,
            "Quantum Phenomena": Subject.N128_PHYSICS,
            "Sound & Waves": Subject.N128_PHYSICS,
            "Work, Energy & Power": Subject.N8_ENGINEERING_SCIENCES,
        })