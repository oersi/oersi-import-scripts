from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class HessenHubMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "Arbeitsblatt, Arbeitsmaterial": Lrt.WORKSHEET,
            "Lernkurs": Lrt.COURSE,
            "Skript": Lrt.SCRIPT,
            "Simulation": Lrt.SIMULATION,
            "Tonaufnahme": Lrt.AUDIO,
            "Unterrichtsplanung, Unterrichtsgestaltung": Lrt.LESSON_PLAN,
            "Übung": Lrt.DRILL_AND_PRACTICE,
            "Video": Lrt.VIDEO,
            "Webseite": Lrt.WEB_PAGE,
            "sonstiges": Lrt.OTHER
        }, None)

        self.add_mapping("language", {
            "Deutsch": "de",
            "Englisch": "en",
            "Französisch": "fr"
        }, None)

        self.add_mapping("subject", {
            "Agrarwissenschaften, Lebensmittel- und Getränketechnologie": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "Anglistik/Englisch": Subject.N008_ENGLISH_STUDIES,
            "Architektur": Subject.N013_ARCHITECTURE,
            "Architektur, Innenarchitektur": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "Bauingenieurwesen": Subject.N68_CIVIL_ENGINEERING,
            "Brauwesen/Getränketechnologie": Subject.N028_BREWING_SCIENCE_BEVERAGE_TECHNOLOGY,
            "Chemie": Subject.N032_CHEMISTRY,
            "Darstellende Kunst, Film und Fernsehen, Theaterwissenschaft": Subject.N77_PERFORMING_ARTS_FILM_AND_TELEVISION_STUDIES_THEATRE_STUDIES,
            "Deutsch als Fremdsprache oder als Zweitsprache": Subject.N271_GERMAN_AS_A_FOREIGN_OR_SECOND_LANGUAGE,
            "Dokumentationswissenschaft": Subject.N037_ARCHIVAL_AND_DOCUMENTATION_SCIENCE,
            "Elektrotechnik und Informationstechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Erziehungswissenschaften": Subject.N33_EDUCATIONAL_SCIENCES,
            "Ethnologie": Subject.N173_ETHNOLOGY,
            "Französisch": Subject.N059_FRENCH,
            "Gartenbau": Subject.N060_HORTICULTURE,
            "Geisteswissenschaften allgemein": Subject.N01_HUMANITIES_GENERAL,
            "Geographie": Subject.N44_GEOGRAPHY,
            "Geographie/Erdkunde": Subject.N050_GEOGRAPHY,
            "Industriedesign/Produktgestaltung": Subject.N203_INDUSTRIAL_DESIGN_PRODUCT_DESIGN,
            "Informatik": Subject.N079_COMPUTER_SCIENCE,
            "Ingenieurinformatik/Technische Informatik": Subject.N123_COMPUTATIONAL_ENGINEERING_TECHNICAL_COMPUTER_SCIENCE,
            "Ingenieurwesen allgemein": Subject.N61_ENGINEERING_GENERAL,
            "Ingenieurwissenschaften": Subject.N8_ENGINEERING_SCIENCES,
            "Interdisziplinäre Studien (Schwerpunkt Ingenieurwissenschaften)": Subject.N072_INTERDISC_STUDIES_SPECIALISING_IN_ENGINEERING,
            "Kommunikations- und Informationstechnik": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "Maschinenbau/Verfahrenstechnik": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Mathematik, Naturwissenschaften": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Mathematische Statistik/Wahrscheinlichkeitsrechnung": Subject.N312_STATISTICS,
            "Medienwissenschaft": Subject.N302_MEDIA_SCIENCE,
            "Physik": Subject.N128_PHYSICS,
            "Politikwissenschaften": Subject.N25_POLITICAL_SCIENCE,
            "Psychologie": Subject.N132_PSYCHOLOGY,
            "Rechts-, Wirtschafts- und Sozialwissenschaften": Subject.N3_LAW_ECONOMICS_AND_SOCIAL_SCIENCES,
            "Sozialwissenschaft": Subject.N148_SOCIAL_SCIENCES,
            "Sozialwissenschaften": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Soziologie": Subject.N149_SOCIOLOGY,
            "Studienbereich Informatik": Subject.N71_COMPUTER_SCIENCE,
            "Studienbereich Mathematik": Subject.N37_MATHEMATICS,
            "Studienbereich Philosophie": Subject.N04_PHILOSOPHY,
            "Studienbereich Psychologie": Subject.N32_PSYCHOLOGY,
            "Studienbereich Wirtschaftswissenschaften": Subject.N30_BUSINESS_AND_ECONOMICS,
        }, None)
