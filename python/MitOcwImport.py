import re
import requests
from MitOcwMapping import MitOcwMapping
from search_index_import_commons.Helpers import parse_entity_name
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as LearningResourceType


def to_date(datetime_string):
    if datetime_string is not None:
        return datetime_string[:10]
    return None


def get_all(array, field_name, default=None):
    return list(filter(None, map(lambda r: r[field_name] if r[field_name] else default, array)))


def flatten(array):
    return [item for sublist in array for item in sublist]


class MitOcwImport(OersiImporter):

    domain = "https://ocw.mit.edu"
    items_per_request = 100

    def __init__(self):
        super().__init__(mapping=MitOcwMapping())
        self.resources_loaded = False
        self.courses_loaded = False
        self.course_record_count = 0
        self.resource_file_record_count = 0
        self.course_metadata_by_id = {}

    def get_name(self):
        return "MIT OpenCourseWare"

    def load_next(self):
        if len(self.course_metadata_by_id) == 0:
            self.load_all_course_metadata_into_cache()
        records = self.load_next_resource_files()
        if records is None and not self.courses_loaded:
            records = self.course_metadata_by_id.values()
            self.courses_loaded = True
        return records

    def load_all_course_metadata_into_cache(self):
        while True:
            query = {
                "from": self.course_record_count,
                "post_filter": {
                    "bool": {
                        "must": [
                            {"bool": {"should": [{"term": {"object_type.keyword": "course"}}]}},
                            {"bool": {"should": [{"term": {"offered_by": "OCW"}}]}}
                        ]
                    }
                },
                "query": {
                    "bool": {
                        "filter": [
                            {"term": {"object_type": "course"}},
                        ]
                    }
                },
                "sort": ["created", "_id"]
            }
            result = self.process_next_request(query)
            self.course_record_count += self.items_per_request
            if result is None:
                return
            else:
                nr = len(result)
                result = list(filter(lambda r: len(r["runs"]) > 0, result))
                self.stats.skipped += nr - len(result)
                for record in result:
                    self.course_metadata_by_id[record["course_id"]] = record

    def load_next_resource_files(self):
        if self.resources_loaded:
            return None
        query = {
            "from": self.resource_file_record_count,
            "_source": {"exclude": ["content"]},
            "post_filter": {
                "bool": {
                    "must": [
                        {"bool": {"should": [{"term": {"object_type.keyword": "resourcefile"}}]}},
                        {"bool": {"should": [{"has_parent": {"parent_type": "resource", "query": {"bool": {"should": [{"term": {"offered_by": "OCW"}}]}}}}]}},
                        {"bool": {"should": [{"exists": {"field": "resource_type"}}]}}
                    ]
                }
            },
            "query": {
                "bool": {
                    "filter": [
                        {"term": {"object_type": "resourcefile"}},
                        {"terms": {"resource_type": ["Exams", "Lecture Audio", "Lecture Videos", "Online Textbooks", "Videos"]}}
                    ]
                }
            },
            "sort": ["_id"]
        }
        self.resource_file_record_count += self.items_per_request
        result = self.process_next_request(query)
        if result is None:
            self.resources_loaded = True
            return None
        return result

    def process_next_request(self, query):
        query["size"] = self.items_per_request
        headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent}
        resp = requests.post("https://open.mit.edu/api/v0/search/", json=query, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        json = resp.json()
        records = list(map(lambda r: r["_source"], json["hits"]["hits"]))
        if len(records) == 0:
            return None
        return records

    def to_search_index_metadata(self, record):
        if record["object_type"] == "course":
            ocw_url = self.domain + "/" + get_all(record["runs"], "slug")[0]
            languages = list(set(get_all(record["runs"], "language", default="en")))
            instructors = flatten(get_all(record["runs"], "instructors"))
            lrts = [LearningResourceType.COURSE]
            image = record["image_src"]
        else:
            course_metadata = self.course_metadata_by_id[record["course_id"]] if record["course_id"] in self.course_metadata_by_id else None
            ocw_url = self.domain + re.sub("^../", "/", record["url"])
            if course_metadata:
                has_part = course_metadata["oersi_has_part"] if "oersi_has_part" in course_metadata else []
                has_part.append({"id": ocw_url, "name": record["title"]})
                course_metadata["oersi_has_part"] = has_part
            course_url = self.domain + "/" + get_all(course_metadata["runs"], "slug")[0] if course_metadata else None
            languages = [record["content_language"]] if record["content_language"] else ["en"]
            instructors = flatten(get_all(course_metadata["runs"], "instructors")) if course_metadata is not None else []
            lrts = set(filter(None, map(lambda s: self.mapping.get("lrt", s), record["resource_type"])))
            if LearningResourceType.AUDIO in lrts and record["content_type"] == "pdf":
                lrts.remove(LearningResourceType.AUDIO)
            image = course_metadata["image_src"] if not record["image_src"] else record["image_src"]
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), record["topics"])))
        languages = list(map(lambda l: re.sub("[_-].*", "", l), languages))
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": languages[0]}
            ],
            "id": ocw_url,
            "name": record["title"],
            "image": (self.domain + image) if image and image.startswith("/") else image,
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "description": record["short_description"],
            "inLanguage": languages,
            "license": {"id": "https://creativecommons.org/licenses/by-nc-sa/4.0/"},
            "creator": [{"type": "Organization", "name": "Massachusetts Institute of Technology", "id": "https://ror.org/042nb2s44"}] + list(map(lambda s: parse_entity_name(s), instructors)),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "keywords": record["topics"],
            "sourceOrganization": [{"type": "Organization", "name": "Massachusetts Institute of Technology", "id": "https://ror.org/042nb2s44"}],
            "mainEntityOfPage": [
                {
                    "id": ocw_url,
                    "dateCreated": to_date(record["created"]) if "created" in record else None,
                    "provider": {
                        "id": self.domain,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ],
        }
        if record["object_type"] != "course" and course_url and course_metadata:
            meta.update({"isPartOf": [{
                "id": course_url,
                "name": course_metadata["title"]
            }]})
        else:
            meta["hasPart"] = record["oersi_has_part"] if "oersi_has_part" in record else None
        return meta


if __name__ == "__main__":
    MitOcwImport().process()
