from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
from mapping import hcrt2hcrt
from mapping import oefos2hochschulfaechersystematik


class OerRepoInnsbruckMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", hcrt2hcrt.mapping, Lrt.OTHER)

        subject_mapping = {}
        for key in oefos2hochschulfaechersystematik.mapping:
            subject_mapping[key.replace("https://vocabs.acdh.oeaw.ac.at/oefosdisciplines/", "https://w3id.org/oerbase/vocabs/oefos2012/")] = oefos2hochschulfaechersystematik.mapping[key]
        self.add_mapping("subject", subject_mapping, None)
