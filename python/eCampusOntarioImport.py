import logging
from datetime import datetime, date
import re
import requests

import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from eCampusOntarioMapping import ECampusOntarioMapping
from common.OersiImporter import OersiImporter


def build_entity(entity_name):
    entity_type = "Person"
    name = entity_name
    if "College" in entity_name or "go2HR" == entity_name or "University" in entity_name or "Committee" in entity_name:
        entity_type = "Organization"
    sep_name_match = re.match("^([^,]+), ?(.+)", entity_name)
    if sep_name_match:
        name = sep_name_match.group(2) + " " + sep_name_match.group(1)
    return {"type": entity_type, "name": name}


def build_date(source_date):
    return str(datetime.strptime(source_date, "%B %d, %Y").date()) if source_date else None


def build_date_from_array(source_date):
    if not source_date or source_date[0] < 100:
        return None
    size = len(source_date)
    year = source_date[0]
    month = source_date[1] if size >= 2 and source_date[1] != 0 else 1
    day = source_date[2] if size >= 3 and source_date[2] != 0 else 1
    return str(date(year=year, month=month, day=day))


def filter_resource(item):
    return "Ontario-Commons-License" not in item["licence"]


class ECampusOntarioImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=ECampusOntarioMapping())
        self.page = 0
        self.hasMore = True

    def get_name(self):
        return "eCampusOntario"

    def load_next(self):
        if not self.hasMore:
            return None
        headers = {'User-Agent': self.user_agent}
        params = {"itemTypes": ["6"], "sortCol": "2", "page": str(self.page), "nextPage": "true",
                  "educationLevels":
                      ["College", "University - Graduate & Post-Graduate", "University - Undergraduate", "Other"]}
        resp = requests.get("https://platform.ecampusontario.ca/unified-search-service/ItemSearch/Search",
                            headers=headers, params=params)
        self.page += 1  # starts with page=0
        if resp.status_code != 200:
            raise IOError("Could not fetch records from {}: {}".format(self.get_name(), resp))
        json = resp.json()
        self.hasMore = json["isLoadMore"]
        if json["totalCount"] == 0:
            self.hasMore = False
            return None
        filtered_items = list(filter(lambda r: filter_resource(r), json["indexItems"]))
        self.stats.skipped += len(json["indexItems"]) - len(filtered_items)
        ids = list(map(lambda r: r["itemIdentifier"], filtered_items))
        # load details
        records = list(filter(None, map(lambda i: self.load_details(i), ids)))
        self.stats.failures += len(ids) - len(records)
        return records

    def load_details(self, record_id):
        headers = {'User-Agent': self.user_agent}
        params = {"clientType": "3", "itemType": "6", "itemIdentifier": record_id}
        resp = requests.get("https://platform.ecampusontario.ca/search-service/ItemSearch",
                            headers=headers, params=params)
        if resp.status_code != 200:
            logging.info("failed to load details for record: %s", record_id)
            return None
        return resp.json()

    def to_search_index_metadata(self, record):
        record_id = str(record["itemIdentifier"])
        record_url = record["openlibraryBookDetail"]["readOnlineLink"] if record["openlibraryBookDetail"]["readOnlineLink"] else "https://openlibrary.ecampusontario.ca/item-details/#/" + record_id
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), record["subjects"]))) if record["subjects"] else []
        lrts = set(filter(None, map(lambda s: self.mapping.get("lrt", s), record["openlibraryBookDetail"]["learningResourceType"]))) if record["openlibraryBookDetail"]["learningResourceType"] else []
        languages = self.mapping.get("language", record["language"])
        license_url = re.sub(r"^.*(http)", "http", record["licence"])
        if license_url == "https://creativecommons.org/share-your-work/public-domain/pdm/":
            license_url = "https://creativecommons.org/publicdomain/mark/1.0"
        publisher_name = record["openlibraryBookDetail"]["publisher"]
        source_organizations_name = record["openlibraryBookDetail"]["institutionAffiliations"].split(", ") if record["openlibraryBookDetail"]["institutionAffiliations"] else []
        if publisher_name:
            source_organizations_name.append(publisher_name)
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": languages[0] if len(languages) > 0 else "en"}
            ],
            "type": ["LearningResource"],
            "id": record_url,
            "name": record["title_EN"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": record["imageUrl"] if record["imageUrl"] else "https://openlibrary.ecampusontario.ca/wp-content/themes/eco-openlibrary-portal/images/open-library-logo.svg",
            "description": record["abstract_EN"],
            "inLanguage": languages,
            "license": {"id": license_url} if license_url else None,
            "contributor": list(map(lambda s: build_entity(s), record["contributors"])) if record["contributors"] else None,
            "creator": list(map(lambda s: build_entity(s), record["authorNames"])) if record["authorNames"] else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "keywords": record["subjectKeywords"],
            "sourceOrganization": list(map(lambda s: {"type": "Organization", "name": s}, set(source_organizations_name))) if source_organizations_name else None,
            "dateCreated": build_date(record["createdOn"]),
            "datePublished": build_date_from_array(record["publishedDateParts"]),
            "publisher": [{"type": "Organization", "name": publisher_name}] if publisher_name else None,
            "mainEntityOfPage": [
                {
                    "id": "https://openlibrary.ecampusontario.ca/item-details/#/" + record_id,
                    "dateCreated": build_date(record["createdOn"]),
                    "dateModified": build_date(record["modifiedOn"]),
                    "provider": {
                        "id": "https://openlibrary.ecampusontario.ca",
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ],
            "encoding": list(map(lambda d: {"contentUrl": "https://openlibrary-repo.ecampusontario.ca/rest/bitstreams/" + d["uuId"] + "/retrieve", "contentSize": str(d["size"]), "encodingFormat": d["mimeType"]}, record["openlibraryBookDetail"]["downloadLinks"]))
        }
        return meta


if __name__ == "__main__":
    ECampusOntarioImport().process()
