from EduSharingHelpers import EduSharingImport
import search_index_import_commons.constants.KimHcrt as Lrt


def video_lrt_replacement(lrt):
    if lrt["id"] == "Video":
        lrt["id"] = Lrt.VIDEO
    return lrt

class ZoerrImport(EduSharingImport):
    def __init__(self, config=None):
        super().__init__("ZOERR", "www.zoerr.de", use_freetext_authors=False, config=config)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if meta is not None:
            meta["learningResourceType"] = list(map(lambda x: video_lrt_replacement(x), meta["learningResourceType"]))
        return meta


if __name__ == "__main__":
    ZoerrImport().process()
