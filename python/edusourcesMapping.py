from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.DcxLrmiAudience as Audience
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class edusourcesMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("license", {
            "cc-by-40": "https://creativecommons.org/licenses/by/4.0/",
            "cc-by-sa-40": "https://creativecommons.org/licenses/by-sa/4.0/",
            "cc-by-nc-40": "https://creativecommons.org/licenses/by-nc/4.0/",
            "cc-by-nd-40": "https://creativecommons.org/licenses/by-nd/4.0/",
            "cc-by-nc-nd-40": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "cc-by-nc-sa-40": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
            "cc-by-30": "https://creativecommons.org/licenses/by/3.0/",
            "cc-by-sa-30": "https://creativecommons.org/licenses/by-sa/3.0/",
            "cc-by-nc-sa": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
            "cc-by-nc-sa-30": "https://creativecommons.org/licenses/by-nc-sa/3.0/",
            "cc-by-nc-nd": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "cc-by-nc-nd-30": "https://creativecommons.org/licenses/by-nc-nd/3.0/",
            "cc-by": "https://creativecommons.org/licenses/by/4.0/",
            "cc-by-sa": "https://creativecommons.org/licenses/by-sa/4.0/",
            "cc-by-nc": "https://creativecommons.org/licenses/by-nc/4.0/",
            "cc-by-nd": "https://creativecommons.org/licenses/by-nd/4.0/",
            "cc-by-nd-30": "https://creativecommons.org/licenses/by-nd/3.0/",
        })

        self.add_mapping("subject", {
            "aarde_milieu": "https://w3id.org/kim/hochschulfaechersystematik/n57",  # earth, environment
            "economie_bedrijf": "https://w3id.org/kim/hochschulfaechersystematik/n30",  # economy, business
            "exact_informatica": "https://w3id.org/kim/hochschulfaechersystematik/n71",  # exact informatics
            "gedrag_maatschappij": "https://w3id.org/kim/hochschulfaechersystematik/n26",  # behavior, society
            "gezondheid": "https://w3id.org/kim/hochschulfaechersystematik/n5",  # health
            "interdisciplinair": "https://w3id.org/kim/hochschulfaechersystematik/n0",  # interdisciplinary
            "kunst_cultuur": "https://w3id.org/kim/hochschulfaechersystematik/n9",  # arts, culture
            "onderwijs_opvoeding": "https://w3id.org/kim/hochschulfaechersystematik/n33",  # education
            "recht_bestuur": "https://w3id.org/kim/hochschulfaechersystematik/n3",  # law, governance
            "taal_communicatie": "https://w3id.org/kim/hochschulfaechersystematik/n1",  # language, communication
            "techniek": "https://w3id.org/kim/hochschulfaechersystematik/n8"  # engineering
        })

        self.add_mapping("lrt", {
            "video": Lrt.VIDEO,
            "website": Lrt.WEB_PAGE,
            "app": Lrt.APPLICATION,
            "audio": Lrt.AUDIO,
            "document": Lrt.TEXT,
            "image": Lrt.IMAGE,
            "openaccess-textbook": Lrt.TEXTBOOK,
            "presentation": Lrt.SLIDE,
            "printable-object": Lrt.OTHER,
            "spreadsheet": Lrt.DATA,
            "virtual-reality": Lrt.SIMULATION,
        })

        self.add_mapping("organization", {
            "TU Delft": "Delft University of Technology",
            "Universiteit van Amsterdam": "University of Amsterdam",
            "Avans Hogeschool": "Avans University of Applied Sciences",
            "Bibliotheek Wageningen University & Research": "Wageningen University & Research",
            "Erasmus University Rotterdam": "Erasmus University Rotterdam",
            "HAN University of Applied Sciences": "HAN University of Applied Sciences",
            "HZ University of Applied Sciences": "HZ University of Applied Sciences",
            "Hanze": "Hanze University of Applied Sciences",
            "Hogeschool van Amsterdam": "Amsterdam University of Applied Sciences",
            "Maastricht University": "Maastricht University",
            "NHL Stenden Hogeschool": "NHL Stenden",
            "Open Press Tilburg University": "Tilburg University",
            "Radboud Universiteit": "Radboud University Nijmegen",
            "Radboud University Press": "Radboud University Nijmegen",
            "Rijksuniversiteit Groningen": "Academia Groningana",
            "SURF": "SURF",
            "SURFnet": "SURFnet",
            "Storytelling Centre": "Scottish Storytelling Centre",
            "TU Eindhoven": "Eindhoven University of Technology",
            "TU/e innovation Space": "Eindhoven University of Technology",
            "Technological University of the Shannon": "Technological University of the Shannon: Midlands Midwest",
            "Tilburg University": "Tilburg University",
            "Unesco": "United Nations Educational, Scientific and Cultural Organization",
            "Universiteit Leiden": "Leiden University",
            "Universiteit Twente": "University of Twente",
            "Universiteit Utrecht": "Utrecht University",
            "University of Copenhagen": "University of Copenhagen",
            "Vrije Universiteit Amsterdam": "Vrije Universiteit Amsterdam",
            "Wageningen University & Research": "Wageningen University & Research",
            "Zuyd Hogeschool": "Zuyd University of Applied Sciences",
            "Medical Library of Radboud University": "Radboud University Nijmegen",
            "UBC Library": "University of British Columbia",
            "Universiteitsbibliotheek van de Vrije Universiteit": "Vrije Universiteit Amsterdam",
            "TU Delft Library": "Delft University of Technology",
            "TU Delft OpenCourseWare": "Delft University of Technology",
            "British Red Cross EUROPEAID": "British Red Cross",
            "Universiteitsbibliotheek Utrecht": "Utrecht University",
            "FOSTER consortium": "FOSTER consortium",
            "Maastricht University Library": "Maastricht University",
            "Universitaire Bibliotheken Leiden": "Leiden University",
            "Bibliotheek HvA": "Amsterdam University of Applied Sciences",
            "Wageningen University & Research Library": "Wageningen University & Research",
            "Bibliotheek UvA": "University of Amsterdam",
            "Tilburg University Library": "Tilburg University",
            "University Library Nijmegen": "Radboud University Nijmegen",
            "Red Cross Red Crescent Climate Centre": "International Federation of Red Cross and Red Crescent Societies",
            "Red Cross Red Crescent Societies": "International Federation of Red Cross and Red Crescent Societies",
            "HCAIM Consortium": "HCAIM Consortium",
            "Community Development Agency van Rappard": "Community Development Agency van Rappard",
            "Medische Bibliotheek van de Radboud Universiteit Nijmegen": "Radboud University Nijmegen",
            "Erasmus University Library": "Erasmus University Rotterdam",
            "Koninklijke Bibliotheek": "National Library of the Netherlands",
            "Universiteitsbibliotheek Radboud": "Radboud University Nijmegen",
            "Programma Interdisciplinair Onderwijs": "",
            "Urban Sustainability Directors Network": "",
            "Stimuleringsregeling Open en Online Onderwijs": "",
            "Project APA 7": "",
            "MOOC Shaping Urban Climate": "",
        })

        self.add_mapping("organization_id", {
            "TU Delft": "https://ror.org/02e2c7k09",
            "Universiteit van Amsterdam": "https://ror.org/04dkp9463",
            "Avans Hogeschool": "https://ror.org/015d5s513",
            "Bibliotheek Wageningen University & Research": "https://ror.org/04qw24q55",
            "Erasmus University Rotterdam": "https://ror.org/057w15z03",
            "HAN University of Applied Sciences": "https://ror.org/0500gea42",
            "HZ University of Applied Sciences": "https://ror.org/047cqa323",
            "Hanze": "https://ror.org/00xqtxw43",
            "Hogeschool van Amsterdam": "https://ror.org/00y2z2s03",
            "Maastricht University": "https://ror.org/02jz4aj89",
            "NHL Stenden Hogeschool": "https://ror.org/02xgxme97",
            "Open Press Tilburg University": "https://ror.org/04b8v1s79",
            "Radboud Universiteit": "https://ror.org/016xsfp80",
            "Radboud University Press": "https://ror.org/016xsfp80",
            "Rijksuniversiteit Groningen": "https://ror.org/012p63287",
            "SURF": "https://ror.org/009vhk114",
            "Storytelling Centre": "https://ror.org/04pd5sn65",
            "TU Eindhoven": "https://ror.org/02c2kyt77",
            "TU/e innovation Space": "https://ror.org/02c2kyt77",
            "Technological University of the Shannon": "https://ror.org/04efm0253",
            "Tilburg University": "https://ror.org/04b8v1s79",
            "Unesco": "https://ror.org/04h4z8k05",
            "Universiteit Leiden": "https://ror.org/027bh9e22",
            "Universiteit Twente": "https://ror.org/006hf6230",
            "Universiteit Utrecht": "https://ror.org/04pp8hn57",
            "University of Copenhagen": "https://ror.org/035b05819",
            "Vrije Universiteit Amsterdam": "https://ror.org/008xxew50",
            "Wageningen University & Research": "https://ror.org/04qw24q55",
            "Zuyd Hogeschool": "https://ror.org/02m6k0m40",
            "British Red Cross": "https://ror.org/00mn56c32",
            "National Library of the Netherlands": "https://ror.org/02w4jbg70",
            "University of British Columbia": "https://ror.org/03rmrcq20",
            "International Federation of Red Cross and Red Crescent Societies": "https://ror.org/040at4140",
            "Amsterdam University of Applied Sciences": "https://ror.org/00y2z2s03",
            "Delft University of Technology": "https://ror.org/02e2c7k09",
            "Leiden University": "https://ror.org/027bh9e22",
            "Radboud University Nijmegen": "https://ror.org/016xsfp80",
            "University of Amsterdam": "https://ror.org/04dkp9463",
            "Utrecht University": "https://ror.org/04pp8hn57",
            "Vrije Universiteit Amsterdam": "https://ror.org/008xxew50",
        })
