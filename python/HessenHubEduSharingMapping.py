from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class HessenHubEduSharingMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("subject", {
            "https://w3id.org/edu-sharing/vocabs/uni-kassel/hochschulfaechersystematik/n900": "https://w3id.org/kim/hochschulfaechersystematik/n0"
        }, None)

    def get(self, group, key):
        if group == "subject":
            if key.startswith("https://w3id.org/kim/hochschulfaechersystematik/"):
                return None
            prepared_key = self.mappings.get(group)["mapping"].get(key)
            return prepared_key if prepared_key else key.replace("https://w3id.org/edu-sharing/vocabs/uni-kassel/hochschulfaechersystematik/", "https://w3id.org/kim/hochschulfaechersystematik/")
        return key