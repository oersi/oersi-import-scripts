from oerPortalUniGrazMapping import OerPortalUniGrazMapping
from EduSharingHelpers import EduSharingImport


class OerPortalUniGrazImport(EduSharingImport):
    def __init__(self):
        super().__init__(
            "OER Portal Uni Graz",
            "oer-portal.uni-graz.at",
            mapping=OerPortalUniGrazMapping(),
            freetext_author_organizations=[
                {"regex": ".*Universität Graz.*", "name": "Universität Graz", "id": "https://ror.org/01faaaf77"},
                {"regex": ".*4students.+"},
                {"regex": "Habitus.Macht.Bildung.+"},
                {"regex": "Internationales Zentrum für Professionalisierung der Elementarpädagogik"},
            ]
        )

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if meta is not None and not meta["inLanguage"]:
            meta["inLanguage"] = ["de"]
        return meta


if __name__ == "__main__":
    OerPortalUniGrazImport().process()
