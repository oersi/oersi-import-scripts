import logging
import requests
from urllib.parse import parse_qs, urlparse

import search_index_import_commons.constants.KimHcrt as LearningResourceType


def get_metadata_entries_for_playlist(playlist_url, config, default_metadata=None):
    if default_metadata is None:
        default_metadata = {}
    playlist_id = parse_qs(urlparse(playlist_url).query)["list"][0]
    logging.debug("fetch metadata for yt-playlist " + playlist_id)
    connector = YouTubeConnector(config)
    try:
        playlist_items = connector.get_playlist_items("snippet", playlist_id)
        # videos-endpoint provides more details -> load for each video
        video_metadata = connector.get_videos("snippet", list(map(lambda x: x["snippet"]["resourceId"]["videoId"], playlist_items)))
        playlist_items = list(map(lambda x: merge_video_metadata_to_playlist_item(x, video_metadata), playlist_items))
        metadata = list(map(lambda x: convert_from_snippet(x, default_metadata), playlist_items))
        return metadata
    except Exception as err:
        logging.exception(err)
    return None


def merge_video_metadata_to_playlist_item(playlist_item, video_metadata):
    data = next((x for x in video_metadata if x["id"] == playlist_item["snippet"]["resourceId"]["videoId"]), None)
    if data is None:
        return playlist_item
    playlist_item["snippet"] = {**data["snippet"], **playlist_item["snippet"]}
    return playlist_item


def convert_from_snippet(yt_snippet, default_metadata):
    meta = {
        **default_metadata,
        "id": "https://www.youtube.com/watch?v=" + yt_snippet["snippet"]["resourceId"]["videoId"],
        "name": yt_snippet["snippet"]["title"],
        "description": yt_snippet["snippet"]["description"],
        "datePublished": yt_snippet["snippet"]["publishedAt"],
        "image": yt_snippet["snippet"]["thumbnails"]["high"]["url"],
        "learningResourceType": [{"id": LearningResourceType.VIDEO}],
        "encoding": [{"embedUrl": "https://www.youtube-nocookie.com/embed/" + yt_snippet["snippet"]["resourceId"]["videoId"]}]
    }
    if "tags" in yt_snippet["snippet"]:
        meta["keywords"] = yt_snippet["snippet"]["tags"]
    return meta


class YouTubeConnector:
    def __init__(self, config):
        self.config = config
        self.user_agent = config['http']['useragent']
        self.api_url = "https://youtube.googleapis.com/youtube/v3"
        self.api_key = config["youtube"]["api_key"] if "youtube" in config and "api_key" in config["youtube"] else None

    def get_default_headers(self):
        return {"User-Agent": self.user_agent}

    def get_playlist_items(self, part, playlist_id):
        headers = self.get_default_headers()
        items = []
        next_page_token = None
        while True:
            params = {"key": self.api_key, "part": part, "playlistId": playlist_id, "pageToken": next_page_token}
            resp = requests.get(self.api_url + "/playlistItems", headers=headers, params=params)
            if resp.status_code != 200:
                raise IOError("Could not fetch data from youtube-API: {}".format(resp))
            json = resp.json()
            items.extend(json["items"])
            if "nextPageToken" in json:
                next_page_token = json["nextPageToken"]
            else:
                break
        return items

    def get_videos(self, part, video_ids):
        headers = self.get_default_headers()
        items = []
        next_page_token = None
        while True:
            params = {"key": self.api_key, "part": part, "id": video_ids, "pageToken": next_page_token}
            resp = requests.get(self.api_url + "/videos", headers=headers, params=params)
            if resp.status_code != 200:
                raise IOError("Could not fetch data from youtube-API: {}".format(resp))
            json = resp.json()
            items.extend(json["items"])
            if "nextPageToken" in json:
                next_page_token = json["nextPageToken"]
            else:
                break
        return items
