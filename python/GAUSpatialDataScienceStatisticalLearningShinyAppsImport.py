from bs4 import BeautifulSoup
import html
import json
import logging
import requests
import time
from search_index_import_commons.Helpers import get, get_list
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess


# NOTE: requires installation of non-standard lib 'python3-bs4'


def to_date(datetime_string):
    if datetime_string is not None:
        return datetime_string[:10]
    return None


def get_creator_name(creator):
    if "name" in creator:
        return creator["name"]
    names = []
    if "givenName" in creator:
        names.append(creator["givenName"])
    if "familyName" in creator:
        names.append(creator["familyName"])
    return " ".join(names) if len(names) > 0 else None


class GAUSpatialDataScienceStatisticalLearningShinyAppsImport(OersiImporter):
    def __init__(self):
        super().__init__()
        self.resources_loaded = False

    def get_name(self):
        return "GAU Spatial Data Science and Statistical Learning Shiny Apps"

    def contains_record(self, record_id):
        return record_id.startswith("https://lehre-rdusl.shinyapps.io/")

    def load_single_record(self, record_id):
        metadata = self.get_json_ld_metadata_from_html_page(record_id)
        return {"response_status_code": 404} if metadata is None else metadata

    def get_json_ld_metadata_from_html_page(self, url):
        headers = {"User-Agent": self.user_agent}
        resp = None
        for i in range(5):
            resp = requests.get(url, headers=headers)
            if resp.status_code == 202:
                logging.debug("status code was 202 with response headers: %s -> %s retry...", resp.headers, i)
                time.sleep(1)
            else:
                break
        html_record = BeautifulSoup(resp.text, "html.parser")
        metadata_tag = html_record.find(attrs={"type": "application/ld+json"})
        if metadata_tag:
            return json.loads(html.unescape(metadata_tag.string))
        return None

    def load_next(self):
        if self.resources_loaded:
            return None
        overview_metadata = self.get_json_ld_metadata_from_html_page("https://lehre-rdusl.shinyapps.io/Overview/")
        records = []
        for part in overview_metadata["hasPart"]:
            metadata = None
            try:
                metadata = self.get_json_ld_metadata_from_html_page(part["@id"])
            except Exception:
                logging.debug("Cannot load metadata from %s", part["@id"])
            if metadata is None:
                self.stats.add_failure()
            else:
                metadata["originalId"] = part["@id"]
                records.append(metadata)
        self.resources_loaded = True
        return records

    def to_search_index_metadata(self, record):
        creator = list(map(lambda c: {"type": c["type"] if "type" in c else "Person", "name": get_creator_name(c), "id": get(c, "id"), "affiliation": get(c, "affiliation")}, get_list(record, "creator")))
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "type": ["LearningResource"],
            "id": record["id"],
            "name": get(record, "name"),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": get(record, "image"),
            "description": get(record, "description"),
            "inLanguage": get(record, "inLanguage"),
            "license": {"id": record["license"]} if "license" in record else None,
            "creator": creator,
            "datePublished": to_date(get(record, "datePublished")),
            "about": list(map(lambda s: {"id": s}, get(record, "about"))) if get(record, "about") else None,
            "learningResourceType": list(map(lambda s: {"id": s}, get(record, "learningResourceType"))) if get(record, "learningResourceType") else None,
            "audience": list(map(lambda s: {"id": s}, get(record, "audience"))) if get(record, "audience") else None,
            "keywords": get(record, "keywords"),
            "sourceOrganization": get(record, "sourceOrganization"),
            "mainEntityOfPage": [
                {
                    "id": record["originalId"],
                    "provider": {
                        "id": "https://lehre-rdusl.shinyapps.io/Overview/",
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    GAUSpatialDataScienceStatisticalLearningShinyAppsImport().process()
