from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
from mapping import hcrt2hcrt
from mapping import oefos2hochschulfaechersystematik


class OerPortalUniGrazMapping(SearchIndexImportMapping):

    def __init__(self):

        lrt_mapping = {
            "https://uni-graz/resourcetype/report": Lrt.TEXT,
            "https://uni-graz/resourcetype/study": Lrt.TEXT,
        }
        lrt_mapping = {**hcrt2hcrt.mapping, **lrt_mapping}

        self.add_mapping("lrt", lrt_mapping, Lrt.OTHER)

        subject_mapping = {}
        for key in oefos2hochschulfaechersystematik.mapping:
            subject_mapping[key.replace("https://vocabs.acdh.oeaw.ac.at/oefosdisciplines/", "")] = oefos2hochschulfaechersystematik.mapping[key]
        self.add_mapping("subject", subject_mapping, None)
