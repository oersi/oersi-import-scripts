import re

from ComeInMapping import ComeInMapping
from EduSharingHelpers import EduSharingImport, get
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import SKOS
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess

class ComeInImport(EduSharingImport):
    collectionUrl = "https://redaktion.openeduhub.net/edu-sharing/components/collections?id=3bf248cc-fb53-4f81-a99a-a40d6d3d2e71"
    
    def __init__(self):
        super().__init__(
            "ComeIn",
            "redaktion.openeduhub.net",
            mapping=ComeInMapping(),
            edu_sharing_metadataset="mds_oeh",
            provider_id = self.collectionUrl,
            criteria=[
                {
                    "property": "ccm:oeh_publisher_combined",
                    "values": [
                        "ComeIn"
                    ]
                }
            ]
        )

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)

        if meta is None:
            return None

        # --------------------------------------------------
        # Subjects (about) 
        # WLO uses at least three fields for about (ccm:taxonid, ccm:oeh_taxonid_university, ccm:curriculum)
        # We do not handle ccm:curriculum but rely on taxonid or oeh_taxonid_university being present
        # --------------------------------------------------
        existing_subjects = set(map(lambda x: x["id"], meta["about"]))
    
        # ccm:oeh_taxonid_university (DeStatis)
        es_additional_subjects = get(record["properties"], "ccm:oeh_taxonid_university")
        additional_subjects = set(filter(lambda x: x is not None and x not in existing_subjects, map(lambda s: s.replace("http://w3id.org/openeduhub/vocabs/hochschulfaechersystematik/", "https://w3id.org/kim/hochschulfaechersystematik/"), es_additional_subjects))) if es_additional_subjects else []
        if additional_subjects:
            meta["about"].extend(list(map(lambda s: {"id": s}, additional_subjects)))
        
        # ccm:taxonid to keywords (transfers some unmappable subjects to keywords)
        es_subjects = get(record["properties"], "ccm:taxonid")
        additional_keywords = list(filter(None, map(lambda x: self.mapping.get("keywords", x), es_subjects))) if es_subjects else []
        meta["keywords"] = list(set(meta["keywords"]).union(set(additional_keywords))) if additional_keywords else meta["keywords"]

        # --------------------------------------------------
        # Conditions of access
        # --------------------------------------------------
        oeh_quality_login = get(record["properties"], "ccm:oeh_quality_login")
        if oeh_quality_login and "0" in oeh_quality_login:
            meta["conditionsOfAccess"] = {"id": ConditionsOfAccess.LOGIN}

        # --------------------------------------------------
        # learningResourceType (LRT)
        # WLO uses at least three fields for LRT (ccm:educationallearningresourcetype, ccm:oeh_lrt, ccm:oeh_lrt_aggregated)
        # Although the AMB profile allows OEHRT and HCRT value lists, they should be unified for better usability in OERSI related applications.
        # Primary goal is to avoid duplicate concepts with different labels. Therefore we replace known mapped OEHRT ids with the HCRT ids.
        # --------------------------------------------------
        existing_lrts = set(map(lambda x: x["id"], meta["learningResourceType"]))
        oeh_lrt = get(record["properties"], "ccm:oeh_lrt")
        oeh_lrt_aggregated = get(record["properties"], "ccm:oeh_lrt_aggregated")
        additional_oeh_lrts = oeh_lrt if oeh_lrt else []
        additional_oeh_lrts = list(set(set(additional_oeh_lrts).union(set(oeh_lrt_aggregated)))) if oeh_lrt_aggregated else additional_oeh_lrts
        additional_lrts = list(set(filter(lambda x: x is not None and x not in existing_lrts, map(lambda x: self.mapping.get("lrt", x), additional_oeh_lrts))))
        if additional_lrts:
            meta["learningResourceType"].extend(list(map(lambda s: {"id": s}, additional_lrts)))
 
        # --------------------------------------------------
        # educationalLevel
        # --------------------------------------------------
        existing_levels = set(map(lambda x: x["id"], meta["educationalLevel"]))
        
        additional_levels = set(filter(lambda x: x is not None and x not in existing_levels, map(lambda s: self.mapping.get("educational_level", s.strip()), meta["keywords"]))) if meta["keywords"] else []
        if additional_levels:
            meta["educationalLevel"].extend(list(map(lambda s: {"id": s}, additional_levels)))

        # --------------------------------------------------
        # teaches, assesses, competencyRequired
        # --------------------------------------------------
        meta["teaches"] = get_teaches(self, record)
        meta["assesses"] = get_assesses(record)
        meta["competencyRequired"] = get_competency_required(record)

        # --------------------------------------------------
        # interactivityType
        # Semantically there can only be one value for interactivityType
        # If there are multiple values, we set it to mixed
        # --------------------------------------------------
        interactivityTypes = list(filter(None, map(lambda s: self.mapping.get("interactivityType", s), meta["keywords"]))) if meta["keywords"] else []
        if len(interactivityTypes) == 1:
            meta["interactivityType"] = interactivityTypes[0]
        elif len(interactivityTypes) > 1:
            meta["interactivityType"] = self.mapping.get("interactivityType", "Lehr-/Lernform: gemischt")
        else:
            meta["interactivityType"] = None
            
        # --------------------------------------------------
        # encoding (override default EduSharingHelpers)
        # --------------------------------------------------
        meta["encoding"] = [build_encoding(record)] if get(record, "mediatype") != "link" and get(record["content"], "url") is not None else None
        
        # --------------------------------------------------
        # Default fields from EduSharingHelpers
        # --------------------------------------------------
        # audience
        # dateCreated, datePublished, dateModified
        
        # --------------------------------------------------
        # ADDITIONAL FIELDS (currently not in use)
        # --------------------------------------------------
        # RELATIONS
        # isPartOf 
        # isBasedOn 
        # hasPart 

        # MEDIA 
        # trailer
        # duration
        # caption

        # OTHER
        # cclom:typicallearningtime
        # ccm:author_freetext
        # ccm:educationaltypicalagerange_from/to
        # ccm:fskRating
        
        # --------------------------------------------------
        # Cleanup
        # --------------------------------------------------
        
        # Remove keywords that were mapped to other fields
        meta["keywords"] = list(filter(lambda x: keep_keyword(x), meta["keywords"])) if meta["keywords"] else []

        return meta

def get_labelled_concept_from_graph(concept_id, graph):
    pref_labels = graph.objects(URIRef(concept_id), SKOS.prefLabel)
    
    pref_label_de = None
    for label in pref_labels:
        if isinstance(label, Literal) and label.language == 'de':
            pref_label_de = label.value
            break

    if pref_label_de is None:
        return []
    
    return to_labelled_concepts([concept_id], [pref_label_de])
    
def keep_keyword(keyword):
    discard_regexes = [
        "Bildungsstufe.*",
        "Lehr-/Lernform.*",
        "MKR-NRW.*",
        "OR-NRW.*"
    ]
    for r in discard_regexes:
        if re.match(r, keyword):
            return False
    return True

def get_competency_required(record):
    competency_required = []
    competency_required.extend(to_labelled_concepts(get(record["properties"], "ccm:oeh_competence_requirements"), get(record["properties"], "ccm:oeh_competence_requirements_DISPLAYNAME")))
    competency_required.extend(to_labelled_concepts(get(record["properties"], "ccm:oeh_languageLevel"), get(record["properties"], "ccm:oeh_languageLevel_DISPLAYNAME")))
    return competency_required

def get_teaches(self, record):
    teaches = []
   
    # Competencies in the digital world (KMK)
    teaches.extend(to_labelled_concepts(get(record["properties"], "ccm:competence"), get(record["properties"], "ccm:competence_DISPLAYNAME")))
    
    # European Framework for the Digital Competence of Educators (DigCompEdu)
    teaches.extend(to_labelled_concepts(get(record["properties"], "ccm:oeh_digital_competency"), get(record["properties"], "ccm:oeh_digital_competency_DISPLAYNAME")))
    
    # Keywords to competencies: Competencies of NRW frameworks
    keywords = get(record["properties"], "cclom:general_keyword")
    additional_competencies = list(filter(None, map(lambda s: self.mapping.get("nrw_competencies", s), keywords))) if keywords else []

    orNRW = Graph().parse("./vocabulary/orientierungsrahmenNRW.ttl", format="turtle") 
    mkrNRW = Graph().parse("./vocabulary/medienkompetenzrahmenNRW.ttl", format="turtle") 
    
    for competency in additional_competencies:
        if('orientierungsrahmenNRW' in competency):
            teaches.extend(get_labelled_concept_from_graph(competency, orNRW))
        if('medienkompetenzrahmenNRW' in competency):
            teaches.extend(get_labelled_concept_from_graph(competency, mkrNRW))
    
    return teaches

def get_assesses(record):
    assesses = []
    assesses.extend(to_labelled_concepts(get(record["properties"], "ccm:oeh_competence_check"), get(record["properties"], "ccm:oeh_competence_check_DISPLAYNAME")))
    return assesses

def build_encoding(record):
    mimetype = get(record, "mimetype")
    downloadUrl = get(record, "downloadUrl")
    contentUrl = get(record["content"], "url")
    return {
        "type": "MediaObject",
        "contentUrl": downloadUrl,
        "embedUrl": contentUrl.replace("/components/render/","/eduservlet/render?node_id="),
        "encodingFormat": mimetype if mimetype and re.match("^[a-z]+/[a-zA-Z0-9\\-+_.]+$", mimetype) else None,
        "contentSize": get(record, "size")
    }

def to_labelled_concepts(value_list, label_list):
    if not value_list or not label_list or len(value_list) != len(label_list):
        return []
    return [
        {
            "id": x,
            "prefLabel": {
                "de": label_list[i]
            } if label_list[i] else None
        } for i, x in enumerate(value_list)
    ]


if __name__ == "__main__":
    ComeInImport().process()
