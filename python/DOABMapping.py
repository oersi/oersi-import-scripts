from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class DOABMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("language", {
            "deu": "de",
            "eng": "en",
            "fre": "fr",
            "ger": "de",
            "ita": "it",
            "spa": "es",
            "tur": "tr"
        })

        self.add_mapping("lrt", {
            "book": Lrt.TEXTBOOK,
            "chapter": Lrt.TEXTBOOK
        }, Lrt.OTHER)

        self.add_mapping("subject", {
            "bic Book Industry Communication::A The arts": Subject.N9_ART_ART_THEORY,
            "bic Book Industry Communication::D Literature & literary studies": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "bic Book Industry Communication::G Reference, information & interdisciplinary subjects": Subject.N0_INTERDISCIPLINARY,
            "bic Book Industry Communication::H Humanities::HB History": Subject.N05_HISTORY,
            "bic Book Industry Communication::H Humanities::HD Archaeology": Subject.N012_ARCHAEOLOGY,
            "bic Book Industry Communication::H Humanities::HP Philosophy": Subject.N04_PHILOSOPHY,
            "bic Book Industry Communication::J Society & social sciences::JM Psychology": Subject.N32_PSYCHOLOGY,
            "bic Book Industry Communication::J Society & social sciences::JN Education": Subject.N33_EDUCATIONAL_SCIENCES,
            "bic Book Industry Communication::J Society & social sciences::JP Politics & government": Subject.N25_POLITICAL_SCIENCE,
            "bic Book Industry Communication::K Economics, finance, business & management::KC Economics": Subject.N175_ECONOMICS,
            "bic Book Industry Communication::K Economics, finance, business & management::KJ Business & management": Subject.N30_BUSINESS_AND_ECONOMICS,
            "bic Book Industry Communication::L Law": Subject.N28_LAW,
            "bic Book Industry Communication::M Medicine": Subject.N49_HUMAN_MEDICINE_EXCL_DENTISTRY,
            "bic Book Industry Communication::P Mathematics & science::PB Mathematics": Subject.N37_MATHEMATICS,
            "bic Book Industry Communication::P Mathematics & science::PG Astronomy, space & time": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "bic Book Industry Communication::P Mathematics & science::PH Physics": Subject.N39_PHYSICS_ASTRONOMY,
            "bic Book Industry Communication::P Mathematics & science::PN Chemistry": Subject.N40_CHEMISTRY,
            "bic Book Industry Communication::P Mathematics & science::PS Biology, life sciences": Subject.N42_BIOLOGY,
            "bic Book Industry Communication::R Earth sciences, geography, environment, planning": Subject.N44_GEOGRAPHY,
            "bic Book Industry Communication::T Technology, engineering, agriculture::TG Mechanical engineering & materials": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "bic Book Industry Communication::T Technology, engineering, agriculture::TH Energy technology & engineering": Subject.N211_ENERGY_PROCESS_ENGINEERING,
            "bic Book Industry Communication::T Technology, engineering, agriculture::TJ Electronics & communications engineering": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "bic Book Industry Communication::T Technology, engineering, agriculture::TV Agriculture & farming": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "bic Book Industry Communication::U Computing & information technology": Subject.N71_COMPUTER_SCIENCE,
            "bic Book Industry Communication::U Computing & information technology::UY Computer science": Subject.N71_COMPUTER_SCIENCE,
        }, None)
