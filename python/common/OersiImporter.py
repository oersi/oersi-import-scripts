import abc
from search_index_import_commons.Helpers import filter_none_values
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter


def normalize_record(record):
    return filter_none_values(record, recursive=True)


class OersiImporter(SearchIndexImporter):

    def contains_record(self, record_id):
        # not implement per default; override in specific importers
        return False

    def load_single_record(self, record_id):
        # not implement per default; override in specific importers
        return None

    def transform(self, data: list) -> list:
        transformed_data = list(map(lambda record: self.to_search_index_metadata(record), data))
        size_with_none_values = len(transformed_data)
        transformed_data = list(filter(None, transformed_data))
        self.stats.skipped += size_with_none_values - len(transformed_data)
        return list(map(lambda record: normalize_record(record), transformed_data))

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    @staticmethod
    def to_date(datetime_string: str):
        if datetime_string is not None:
            return datetime_string[:10]
        return None
