from bs4 import BeautifulSoup
import html
import json
import logging
import requests
from HoouMapping import HoouMapping
from search_index_import_commons.Helpers import get
from search_index_import_commons.SitemapReader import SitemapReader
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess


# NOTE: requires installation of non-standard lib 'python3-bs4'


def to_date(datetime_string):
    if datetime_string is not None:
        return datetime_string[:10]
    return None


def get_languages_from_html(html_record):
    language_label = html_record.find("span", string="inLanguage")
    language_tag = language_label.find_next("td") if language_label else None
    languages = language_tag.string if language_tag else None
    return languages.strip().split() if languages else []


class HoouImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=HoouMapping())

    def get_name(self):
        return "HOOU"

    def load_next(self):
        headers = {"User-Agent": self.user_agent}
        if not hasattr(self, "urls"):
            sitemap = SitemapReader("https://www.hoou.de/sitemap.xml", user_agent=self.user_agent,
                                    url_pattern=".*/(materials|projects)/.*")
            self.urls = sitemap.get_url_locations()

        while self.urls:
            loc = self.urls.pop()
            loc_response = requests.get(loc, headers=headers)
            if loc_response.status_code != 200:
                self.stats.add_failure()
                logging.info("Could not fetch record %s: %s", loc, loc_response)
                continue
            html_record = BeautifulSoup(loc_response.text, "html.parser")
            data_tag = html_record.find(attrs={"data-test": "model-linked-data", "type": "application/ld+json"})
            record = json.loads(html.unescape(data_tag.string))

            # some workaround fixes, because of HOOU data quality
            if "id" not in record:
                record["id"] = loc
            if "inLanguage" not in record:
                record["inLanguage"] = get_languages_from_html(html_record)
            return [record]
        return None

    def determine_subjects_from_keywords(self, keywords):
        subjects = []
        for keyword in keywords:
            s = self.mapping.get("subject", keyword)
            if s != keyword and s is not None:
                subjects.append(s)
        return set(subjects)

    def to_search_index_metadata(self, record):
        lrts = set(filter(None, [self.mapping.get("lrt", record["learningResourceType"])] if "learningResourceType" in record else [None]))
        audiences = set(filter(None, map(lambda s: self.mapping.get("audience", get(s, "educationalRole")), record["audience"]))) if "audience" in record else None
        keywords = list(set(map(lambda s: s.strip(), record["keywords"].split(",")))) if "keywords" in record else []
        subjects = self.determine_subjects_from_keywords(keywords)
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "id": record["id"],
            "name": get(record, "name"),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": get(record, "thumbnailUrl"),
            "description": get(record, "description"),
            "inLanguage": list(filter(None, map(lambda s: self.mapping.get("language", s.strip()), get(record, "inLanguage")))),
            "license": {"id": record["license"]} if "license" in record else None,
            "creator": list(map(lambda a: {"type": a["@type"], "name": a["name"]}, record["author"])) if "author" in record else None,
            "datePublished": to_date(get(record, "datePublished")),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "audience": list(map(lambda s: {"id": s}, audiences)) if audiences else None,
            "keywords": keywords,
            "sourceOrganization": list(map(lambda s: {"type": "Organization", "name": s["name"], "id": s["url"]}, record["provider"])) if "provider" in record else None,
            "mainEntityOfPage": [
                {
                    "id": record["id"],
                    "dateCreated": to_date(get(record, "dateCreated")),
                    "dateModified": to_date(get(record, "dateModified")),
                    "provider": {
                        "id": "https://oerworldmap.org/resource/urn:uuid:ac5d5269-6449-43c0-b43b-2ed372763a0e",
                        "type": "Service",
                        "name": "HOOU"
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    HoouImport().process()
