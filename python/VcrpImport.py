from EduSharingHelpers import get, EduSharingImport


class VcrpImport(EduSharingImport):
    def __init__(self):
        super().__init__("VCRP", "openedu-rlp.de", use_freetext_authors=False)

    def to_search_index_metadata(self, record):
        return self.map_default(record)

    def has_sufficient_metadata(self, meta):
        if not meta.get("about"):
            return False
        return super().has_sufficient_metadata(meta)


if __name__ == "__main__":
    VcrpImport().process()
