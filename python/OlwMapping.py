from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class OlwMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("lrt", {
            "Audio": Lrt.AUDIO,
            "Audioreader": Lrt.OTHER,
            "Online-Tutorial": Lrt.OTHER,
            "Textdokument": Lrt.TEXT,
            "Video": Lrt.VIDEO
        }, Lrt.OTHER)

        self.add_mapping("license", {
            "1": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "2": None,
            "3": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
            "4": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "5": "https://creativecommons.org/licenses/by-sa/4.0/",
            "6": "https://creativecommons.org/licenses/by/4.0/",
        }, None)

        self.add_mapping("language", {
            "deutsch": "de",
            "english": "en",
            "Spanisch": "es",
        }, None)

        self.add_mapping("subject", {
            "Arbeitswissenschaften": Subject.N011_WORK_STUDIES_BUSINESS_STUDIES,
            "Architektur": Subject.N013_ARCHITECTURE,
            "Bauingenieurwesen": Subject.N68_CIVIL_ENGINEERING,
            "E-Learning": None,
            "Elektrotechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Geschichte": Subject.N05_HISTORY,
            "Informatik": Subject.N71_COMPUTER_SCIENCE,
            "Maschinenbau": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Mathematik": Subject.N37_MATHEMATICS,
            "Philosophie": Subject.N04_PHILOSOPHY,
            "Physik": Subject.N128_PHYSICS,
            "Politikwissenschaft": Subject.N25_POLITICAL_SCIENCE,
            "Pädagogik": Subject.N33_EDUCATIONAL_SCIENCES,
            "Rechts- und Wirtschaftswissenschaften": Subject.N3_LAW_ECONOMICS_AND_SOCIAL_SCIENCES,
            "Soziologie": Subject.N149_SOCIOLOGY,
            "Sportwissenschaft": Subject.N029_SPORTS_SCIENCE,
            "Universitätsbibliothek": Subject.N06_INFORMATION_AND_LIBRARY_SCIENCES,
        }, None)
