from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class MitOcwMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("lrt", {
            "Exams": Lrt.DRILL_AND_PRACTICE,
            "Lecture Audio": Lrt.AUDIO,
            "Lecture Videos": Lrt.VIDEO,
            "Online Textbooks": Lrt.TEXTBOOK,
            "Videos": Lrt.VIDEO
        }, Lrt.OTHER)

        self.add_mapping("subject", {
            "Aerodynamics": Subject.N128_PHYSICS,
            "Aerospace Engineering": Subject.N057_AERONAUTICAL_AND_AEROSPACE_ENGINEERING,
            "African History": Subject.N05_HISTORY,
            "Algebra and Number Theory": Subject.N37_MATHEMATICS,
            "American History": Subject.N05_HISTORY,
            "Analytical Chemistry": Subject.N40_CHEMISTRY,
            "Ancient History": Subject.N05_HISTORY,
            "Anthropology": Subject.N009_ANTHROPOLOGY_HUMAN_BIOLOGY,
            "Applied Mathematics": Subject.N37_MATHEMATICS,
            "Archaeology": Subject.N012_ARCHAEOLOGY,
            "Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "Astrophysics": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "Atomic, Molecular, Optical Physics": Subject.N128_PHYSICS,
            "Biochemistry": Subject.N025_BIOCHEMISTRY,
            "Biological Engineering": Subject.N282_BIOTECHNOLOGY,
            "Biology": Subject.N42_BIOLOGY,
            "Biomedicine": Subject.N300_BIOMEDICINE,
            "Biotechnology": Subject.N282_BIOTECHNOLOGY,
            "Business": Subject.N021_BUSINESS_ADMINISTRATION,
            "Chemistry": Subject.N40_CHEMISTRY,
            "Chemical Engineering": Subject.N033_CHEMICAL_ENGINEERING_CHEMICAL_PROCESS_ENGINEERING,
            "Cognitive Science": Subject.N32_PSYCHOLOGY,
            "Computer Science": Subject.N71_COMPUTER_SCIENCE,
            "Differential Equations": Subject.N37_MATHEMATICS,
            "Discrete Mathematics": Subject.N37_MATHEMATICS,
            "Earth Science": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Economics": Subject.N175_ECONOMICS,
            "Electrical Engineering": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Engineering": Subject.N8_ENGINEERING_SCIENCES,
            "Ethics": Subject.N169_ETHICS,
            "Fine Arts": Subject.N75_FINE_ARTS,
            "Geography": Subject.N44_GEOGRAPHY,
            "Geology": Subject.N065_GEOLOGY_PALAEONTOLOGY,
            "Geophysics": Subject.N066_GEOPHYSICS,
            "Health and Medicine": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "History": Subject.N05_HISTORY,
            "Humanities": Subject.N1_HUMANITIES,
            "Linear Algebra": Subject.N37_MATHEMATICS,
            "Mathematical Analysis": Subject.N37_MATHEMATICS,
            "Mathematics": Subject.N37_MATHEMATICS,
            "Mechanical Engineering": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Media Studies": Subject.N302_MEDIA_SCIENCE,
            "Music": Subject.N78_MUSIC_MUSICOLOGY,
            "Music History": Subject.N114_MUSICOLOGY_HISTORY_OF_MUSIC,
            "Neuroscience": Subject.N32_PSYCHOLOGY,
            "Operations Management": Subject.N021_BUSINESS_ADMINISTRATION,
            "Particle Physics": Subject.N128_PHYSICS,
            "Philosophy": Subject.N127_PHILOSOPHY,
            "Physics": Subject.N128_PHYSICS,
            "Probability and Statistics": Subject.N37_MATHEMATICS,
            "Public Administration": Subject.N29_ADMINISTRATIVE_SCIENCES,
            "Quantum Mechanics": Subject.N128_PHYSICS,
            "Religion": Subject.N136_RELIGIOUS_STUDIES,
            "Social Science": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Sociology": Subject.N149_SOCIOLOGY,
            "Software Design and Engineering": Subject.N71_COMPUTER_SCIENCE,
            "Teaching and Education": Subject.N33_EDUCATIONAL_SCIENCES,
            "Topology and Geometry": Subject.N37_MATHEMATICS,
            "Thermodynamics": Subject.N128_PHYSICS,
            "Urban Planning": Subject.N67_SPATIAL_PLANNING
        }, None)
