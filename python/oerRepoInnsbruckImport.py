from EduSharingHelpers import EduSharingImport
from oerRepoInnsbruckMapping import OerRepoInnsbruckMapping


class OerRepoInnsbruckImport(EduSharingImport):
    def __init__(self):
        super().__init__("Universität Innsbruck OER Repositorium", "oer-repo.uibk.ac.at", mapping=OerRepoInnsbruckMapping(), use_freetext_authors=False)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if meta is not None and not meta["sourceOrganization"]:
            meta["sourceOrganization"] = [
                {
                    "name": "Universität Innsbruck",
                    "type": "Organization",
                    "id": "https://ror.org/054pv6659"
                }
            ]
        return meta


if __name__ == "__main__":
    OerRepoInnsbruckImport().process()
