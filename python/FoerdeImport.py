from EduSharingHelpers import get, EduSharingImport
from FoerdeMapping import FoerdeMapping


class FoerdeImport(EduSharingImport):
    def __init__(self):
        super().__init__("fOERde", "oer.uni-kiel.de", mapping=FoerdeMapping())

    def filter_resource(self, record):
        edu_context = get(record["properties"], "ccm:educontextname_DISPLAYNAME")
        if not edu_context or "Hochschule" not in edu_context:
            return False
        return super().filter_resource(record)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if meta is not None and not meta["sourceOrganization"]:
            meta["sourceOrganization"] = [
                {
                    "name": "Christian-Albrechts-Universität zu Kiel",
                    "type": "Organization",
                    "id": "https://ror.org/04v76ef78"
                }
            ]
        return meta


if __name__ == "__main__":
    FoerdeImport().process()
