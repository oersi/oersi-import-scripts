from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class TUDelftOpenCourseWareMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "course": Lrt.COURSE,
        }, None)

        self.add_mapping("subject", {
            "Aerospace Engineering": Subject.N057_AERONAUTICAL_AND_AEROSPACE_ENGINEERING,
            "Applied Earth Sciences": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Applied Mathematics": Subject.N37_MATHEMATICS,
            "Applied Physics": Subject.N39_PHYSICS_ASTRONOMY,
            "Applied Science": None,
            "Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "Bachelor": None,
            "Biomechanical Design": Subject.N42_BIOLOGY,
            "Biomedical Engineering": Subject.N300_BIOMEDICINE,
            "Biotechnology": Subject.N282_BIOTECHNOLOGY,
            "Chemical Engineering": Subject.N40_CHEMISTRY,
            "Civil Engineering": Subject.N68_CIVIL_ENGINEERING,
            "Computer Science and Engineering": Subject.N8_ENGINEERING_SCIENCES,
            "Computer Science": Subject.N71_COMPUTER_SCIENCE,
            "De Delftse Leerlijn voor Scheikunde": Subject.N40_CHEMISTRY,
            "Electrical Engineering": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Engineering and Policy Analysis": Subject.N8_ENGINEERING_SCIENCES,
            "Geomatics": Subject.N69_SURVEYING,
            "Hydraulic Engineering": Subject.N094_HYDRAULIC_ENGINEERING,
            "Industrial Design Engineering": Subject.N203_INDUSTRIAL_DESIGN_PRODUCT_DESIGN,
            "Information Skills BSc": None,
            "Information Skills MSc": None,
            "Language Courses": Subject.N1_HUMANITIES,
            "Marine Technology": Subject.N142_NAVAL_ARCHITECTURE_SHIP_TECHNOLOGY,
            "Maritieme Techniek": Subject.N142_NAVAL_ARCHITECTURE_SHIP_TECHNOLOGY,
            "Mathematics": Subject.N37_MATHEMATICS,
            "Mechanical Engineering": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Media and Knowledge Engineering": Subject.N305_MEDIA_TECHNOLOGY,
            "Microelectronics": Subject.N157_MICROELECTRONICS,
            "Minor: Stedenbouw in de Delta": Subject.N67_SPATIAL_PLANNING,
            "Offshore Engineering": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Petroleum Engineering": Subject.N8_ENGINEERING_SCIENCES,
            "Secondary Education Modules": None,
            "Sustainable Development": Subject.N458_ENVIRONMENTAL_PROTECTION,
            "Sustainable Energy Technology": Subject.N310_RENEWABLE_ENERGY,
            "Sustainable Processes and Energy": Subject.N310_RENEWABLE_ENERGY,
            "Systems Engineering, Policy Analysis & Management": Subject.N140_APPLIED_SYSTEMS_SCIENCE,
            "TPM Minors and Electives": None,
            "Teacher Training": Subject.N33_EDUCATIONAL_SCIENCES,
            "Technology, Policy and Management": None, # title is much too general to be assigned to a single destatis subject
            "Transport, Infrastructure and Logistics": Subject.N268_TRANSPORTATION_SYSTEMS,
            "Various": Subject.N0_INTERDISCIPLINARY,
            "Watermanagement": Subject.N077_WATER_MANAGEMENT,
        }, None)
