from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class VhbMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("lrt", {
            "application": Lrt.APPLICATION,
            "audio": Lrt.AUDIO,
            "task": Lrt.DRILL_AND_PRACTICE,
            "image": Lrt.IMAGE,
            "experiment": Lrt.EXPERIMENT,
            "case_study": Lrt.CASE_STUDY,
            "map": Lrt.MAP,
            "assessment": Lrt.ASSESSMENT,
            "educational_game": Lrt.EDUCATIONAL_GAME,
            "reference_book": Lrt.INDEX,
            "presentation": Lrt.SLIDE,
            "simulation": Lrt.SIMULATION,
            "script": Lrt.SCRIPT,
            "text": Lrt.TEXT,
            "video": Lrt.VIDEO,
            "wiki": Lrt.WEB_PAGE,
        }, Lrt.OTHER)

        self.add_mapping("subject", {
            "https://w3id.org/kim/hochschulfaechersystematik/vhb_0": Subject.N0_INTERDISCIPLINARY,
            "https://w3id.org/kim/hochschulfaechersystematik/vhb_1": Subject.N0_INTERDISCIPLINARY,
            "https://w3id.org/kim/hochschulfaechersystematik/vhb_3": Subject.N0_INTERDISCIPLINARY,
            "https://w3id.org/kim/hochschulfaechersystematik/vhb_4": Subject.N0_INTERDISCIPLINARY
        }, None)

    def get(self, group, key):
        if group == "subject" and key.startswith("https://w3id.org/kim/hochschulfaechersystematik/n") and key not in self.mappings.get(group)["mapping"]:
            return key
        return super().get(group, key)
