from VhbMapping import VhbMapping
from EduSharingHelpers import EduSharingImport


class VhbImport(EduSharingImport):
    def __init__(self):
        super().__init__("vhb", "oer.vhb.org", VhbMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta


if __name__ == "__main__":
    VhbImport().process()
