from bs4 import BeautifulSoup
import re
import requests
from HessenHubMapping import HessenHubMapping
from search_index_import_commons.Helpers import get
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess


# NOTE: requires installation of non-standard lib 'python3-bs4'


class HessenHubImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=HessenHubMapping())
        self.loaded = False
        self.author_ids = {}
        self.base_url = "https://oer.hessenhub.de/"

    def get_name(self):
        return "HessenHub"

    def load_next(self):
        if self.loaded:
            return None
        headers = {"User-Agent": self.user_agent}
        html_response = requests.get(self.base_url, headers=headers)
        if html_response.status_code != 200:
            raise IOError("Could not fetch records from {}: {}".format(self.url, html_response))

        html_page = BeautifulSoup(html_response.text, "html.parser")
        html_records = html_page.find_all("li", attrs={"class": "media"})
        self.loaded = True
        return html_records if len(html_records) > 0 else None

    def to_search_index_metadata(self, html_record):
        record = self.parse_html_record(html_record)

        languages = list(set(filter(None, map(lambda s: self.mapping.get("language", s), get(record, "inLanguage"))))) if "inLanguage" in record else []
        lrts = set(filter(None, map(lambda s: self.mapping.get("lrt", s), get(record, "learningResourceType")))) if "learningResourceType" in record else []
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), get(record, "about")))) if "about" in record else []
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "id": get(record, "id"),
            "name": get(record, "name"),
            "conditionsOfAccess": {"id": get(record, "conditionsOfAccess")},
            "inLanguage": languages,
            "license": {"id": get(record, "license")} if "license" in record else None,
            "image": get(record, "image"),
            "creator": get(record, "creator"),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "keywords": get(record, "keywords"),
            "sourceOrganization": get(record, "sourceOrganization"),
            "mainEntityOfPage": [
                {
                    "id": self.base_url,
                    "provider": {
                        "id": self.base_url,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        return meta

    def parse_html_record(self, html_record):
        record = {}
        heading = html_record.find(attrs={"class": "media-heading"})
        if heading:
            a = heading.find("a")
            if a:
                record["id"] = a["href"]
                record["name"] = a.string

        authors_label = html_record.find(string=re.compile("^Autor[*]in.*")).parent
        single_author = authors_label.find("a")
        if single_author:
            record["creator"] = [self.parse_html_author(single_author)]
        else:
            authors_list = authors_label.findNext("ul")
            if authors_list:
                multiple_authors = authors_list.findAll("a")
                record["creator"] = list(map(lambda html_author: self.parse_html_author(html_author), multiple_authors))

        type_icon = html_record.find("img", attrs={"class": "type-icon"})
        if type_icon:
            record["image"] = type_icon["src"]

        subject_label = html_record.find(string=re.compile("^Fach.*")).parent
        record["about"] = [subject_label.find("a").string]

        language_label = html_record.find(string=re.compile("^Sprache.*")).parent
        record["inLanguage"] = [language_label.find("a").string]

        lrt_label = html_record.find(string=re.compile("^Typ.*")).parent
        record["learningResourceType"] = [lrt_label.find("a").string]

        keywords_label = html_record.find(string=re.compile("^Schlagwörter.*")).parent
        record["keywords"] = list(map(lambda html_keyword: html_keyword.string, keywords_label.findAll("a")))

        record["license"] = html_record.find("a", attrs={"rel": "license"})["href"]

        record["sourceOrganization"] = []
        record["conditionsOfAccess"] = ConditionsOfAccess.NO_LOGIN
        if re.match("^https://[.a-zA-Z]*uni-marburg[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Philipps-Universität Marburg", "type": "Organization", "id": "https://ror.org/01rdrb571"})
        if re.match("^https://[.a-zA-Z]*openlearnware[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Technische Universität Darmstadt", "type": "Organization", "id": "https://ror.org/05n911h24"})
        if re.match("^https://lernbar[.]uni-frankfurt[.]de.+", record["id"]) or re.match("^https://[.a-zA-Z]*geomedienlabor[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Goethe-Universität Frankfurt am Main", "type": "Organization", "id": "https://ror.org/04cvxnb49"})
        if re.match("^https://www[.]uni-kassel[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Universität Kassel", "type": "Organization", "id": "https://ror.org/04zc7p361"})
        if re.match("^https://[.a-zA-Z]*uni-giessen[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Justus-Liebig-Universität Gießen", "type": "Organization", "id": "https://ror.org/033eqas34"})
        if re.match("^https://moodle[.]frankfurt-university[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Frankfurt University of Applied Sciences", "type": "Organization", "id": "https://ror.org/02r625m11"})
            record["conditionsOfAccess"] = ConditionsOfAccess.LOGIN
        if re.match("^https://[.a-zA-Z]*hs-gm[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Hochschule Geisenheim", "type": "Organization", "id": "https://ror.org/05myv7q56"})
        if re.match("^https://[.a-zA-Z]*hs-fulda[.]de.+", record["id"]):
            record["sourceOrganization"].append({"name": "Hochschule Fulda", "type": "Organization", "id": "https://ror.org/041bz9r75"})
        if re.match("^https://moodle-ext[.]thm[.]de.+", record["id"]):
            record["conditionsOfAccess"] = ConditionsOfAccess.LOGIN
        if re.match("^https://linux[.]cards.*", record["id"]):
            record["conditionsOfAccess"] = ConditionsOfAccess.LOGIN

        return record

    def parse_html_author(self, html_author):
        author = {"name": html_author.string, "type": "Person"}
        details = self.get_author_details(html_author["href"])
        author["id"] = details["id"]
        return author

    def get_author_details(self, author_path):
        if author_path in self.author_ids:
            return self.author_ids[author_path]
        details = {}
        html_response = requests.get(self.base_url + author_path, headers={"User-Agent": self.user_agent})
        if html_response.status_code != 200:
            raise IOError("Could not fetch author details from {}: {}".format(self.base_url + author_path, html_response))
        html_page = BeautifulSoup(html_response.text, "html.parser")
        html_orcid = html_page.find("a", attrs={"href": re.compile("^https://orcid.org/.+")})
        if html_orcid:
            details["id"] = html_orcid["href"]
        else:
            details["id"] = None
        self.author_ids[author_path] = details
        return details


if __name__ == "__main__":
    HessenHubImport().process()
