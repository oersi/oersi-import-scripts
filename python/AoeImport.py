import urllib.parse as up
import re
import logging
from AoeMapping import AoeMapping
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from search_index_import_commons.OaiPmhReader import OaiPmhReader


def to_date(datetime_string):
    if datetime_string is not None:
        return datetime_string[:10]
    return None


def is_valid_url(url):
    if not url:
        return False
    u = up.urlparse(url.strip())
    return u.scheme and u.netloc and " " not in u.path


class AoeImport(OersiImporter):
    """
    NOTE: the aoe API is not open and standard requests are forbidden / rejected. Permission must be granted by aoe.
    """

    def __init__(self):
        super().__init__(mapping=AoeMapping())
        self.base_url_record = "https://aoe.fi/#/materiaali/"
        self.oai = OaiPmhReader("https://aoe.fi/meta/oaipmh", "oai_dc", None, self.user_agent)
        self.namespaces = {
            "oai": "http://www.openarchives.org/OAI/2.0/",
            "oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
            "dc": "http://purl.org/dc/elements/1.1/",
            "lrmi": "http://dublincore.org/dcx/lrmi-terms/1.1/",
            "xml": "http://www.w3.org/XML/1998/namespace"
        }

    def get_name(self):
        return "aoe"

    def load_next(self):
        data = self.oai.load_next()
        if not data:
            return None
        records = list(filter(lambda record: self.filter_resource(record), data))
        self.stats.skipped += len(data) - len(records)
        return records

    def filter_resource(self, record):
        dc_record = record.find("oai:metadata/oai_dc:dc", self.namespaces)
        if dc_record is not None:
            educational_levels = list(map(lambda s: s.text.strip(),
                                          dc_record.findall("lrmi:learningResource/lrmi:educationalLevel", self.namespaces)))
            has_higher_education_level = len({
                                                 "korkeakoulutus", "higher education",  # higher education
                                                 "alemman korkeakouluasteen tutkinto", "bachelor's degree",  # bachelor's degree
                                                 "ylemmän korkeakouluasteen tutkinto", "master's degree",  # master's degree
                                                 "tutkijakoulutusasteen tutkinto", "third cycle degree"  # third cycle degree
                                             }.intersection(set(educational_levels))) > 0
            if not has_higher_education_level:
                header_record = record.find("oai:header", self.namespaces)
                record_id = header_record.findtext("oai:identifier", None, self.namespaces).split(":")[-1]
                logging.debug("skip resource %s, because educational level is not 'higher education': %s",
                              record_id, educational_levels)
            return has_higher_education_level
        return False

    def to_search_index_metadata(self, record):
        namespaces = self.namespaces
        header_record = record.find("oai:header", namespaces)
        if "status" in header_record.attrib and header_record.get("status") == "deleted":
            return None
        record_id = header_record.findtext("oai:identifier", None, namespaces).split(":")[-1]
        dc_record = record.find("oai:metadata/oai_dc:dc", namespaces)

        subjects = list(filter(None, map(lambda s: self.mapping.get("subject", s.text.strip()), dc_record.findall("lrmi:learningResource/lrmi:educationalAlignment/lrmi:educationalSubject", namespaces))))
        subjects.extend(list(filter(None, map(lambda s: self.mapping.get("subject", s.text.strip()), dc_record.findall("lrmi:about/lrmi:thing/lrmi:identifier", namespaces)))))
        lrts = set(filter(None, map(lambda s: self.mapping.get("lrt", s.text.strip()), dc_record.findall("dc:type", namespaces))))
        audiences = set(filter(None, map(lambda s: self.mapping.get("audience", s.text.strip()), dc_record.findall("lrmi:educationalAudience/lrmi:educationalRole", namespaces))))
        license_url = self.mapping.get("license", dc_record.findtext("dc:rights", None, namespaces))
        organizations = set(map(lambda s: s.text.strip(), dc_record.findall("lrmi:author/lrmi:person/lrmi:affiliation", namespaces)))
        languages = list(map(lambda s: s.text.strip(), dc_record.findall("lrmi:inLanguage", namespaces)))
        languages.sort()

        encoding = list(filter(None, map(lambda s: self.get_download_encoding(s), dc_record.findall("lrmi:material", namespaces))))
        if len(encoding) > 1:
            encoding = [{
                "contentUrl": "https://aoe.fi/api/v1/material/file/" + record_id,
                "encodingFormat": "application/zip",
                "type": "MediaObject"
            }]
        if "embedUrl" not in {key for entry in encoding for key in entry.keys()}:
            encoding.append({
                "embedUrl": "https://aoe.fi/#/embed/" + record_id + "/en"
            })

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": languages[0] if len(languages) > 0 else "fi"}
            ],
            "id": self.base_url_record + record_id,
            "name": dc_record.findtext("dc:title[@xml:lang='en']", None, namespaces).strip() if dc_record.findtext("dc:title[@xml:lang='en']", None, namespaces) else dc_record.findtext("dc:title[@xml:lang='fi']".strip(), None, namespaces),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": dc_record.findtext("dc:description[@format='image/png']", None, namespaces),
            "description": dc_record.findtext("dc:description[@xml:lang='en']", None, namespaces) if dc_record.findtext("dc:description[@xml:lang='en']", None, namespaces) else dc_record.findtext("dc:description[@xml:lang='fi']", None, namespaces),
            "inLanguage": languages,
            "license": {"id": license_url} if license_url else None,
            "creator": self.get_creator(dc_record),
            "about": list(map(lambda s: {"id": s}, set(subjects))),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "audience": list(map(lambda s: {"id": s}, audiences)),
            "keywords": list(map(lambda s: s.text.strip(), dc_record.findall("lrmi:about/lrmi:thing/lrmi:name", namespaces))),
            "sourceOrganization": list(map(lambda s: {"type": "Organization", "name": s}, organizations)),
            "mainEntityOfPage": [
                {
                    "id": self.base_url_record + record_id,
                    "dateCreated": to_date(dc_record.findtext("lrmi:dateCreated", None, namespaces)),
                    "dateModified": to_date(dc_record.findtext("lrmi:dateModified", None, namespaces)),
                    "provider": {
                        "type": "Service",
                        "id": "https://aoe.fi",
                        "name": "Finnish Library of Open Educational Resources"
                    }
                }
            ],
            "encoding": encoding
        }
        if not meta["image"]:
            meta["image"] = "https://oersi.org/resources/aoe-logo.png"
        is_based_on_records = dc_record.findall("lrmi:isBasedOn", namespaces)
        if is_based_on_records:
            meta["isBasedOn"] = list(filter(None, map(lambda x: self.get_based_on_item(x), is_based_on_records)))
        return meta

    def get_based_on_item(self, is_based_on_record):
        url = is_based_on_record.findtext("lrmi:url", None, self.namespaces)
        if not is_valid_url(url):
            return None
        name = is_based_on_record.findtext("lrmi:name", None, self.namespaces)
        return {"id": url.strip(), "name": name}

    def get_creator(self, dc_record):
        creators = []
        for person_xml in dc_record.findall("lrmi:author/lrmi:person", self.namespaces):
            name = person_xml.find("lrmi:name", self.namespaces).text.strip()
            name = re.sub('\\([^)]+\\)', '', name).strip()
            name_match = re.match("^(.+), ?(.+)", name)
            if name_match:
                first = name_match.group(1).strip()
                last = name_match.group(2).strip()
                name = first + " " + last
            affiliation_xml = person_xml.find("lrmi:affiliation", self.namespaces)
            creators.append({
                "type": "Person",
                "name": name,
                "affiliation": {"type": "Organization", "name": affiliation_xml.text.strip()} if affiliation_xml is not None else None
            })
        for name_xml in dc_record.findall("lrmi:author/lrmi:organization/lrmi:legalName", self.namespaces):
            creators.append({"type": "Organization", "name": name_xml.text.strip()})
        return creators

    def get_download_encoding(self, material_xml):
        url_xml = material_xml.find("lrmi:url", self.namespaces)
        if url_xml is None:
            return None
        url = url_xml.text.strip()
        format_xml = material_xml.find("lrmi:format", self.namespaces)
        filesize_xml = material_xml.find("lrmi:filesize", self.namespaces)
        if filesize_xml is not None:  # is downloadable
            return {
                "contentUrl": url,
                "encodingFormat": format_xml.text if format_xml is not None else None,
                "contentSize": filesize_xml.text
            }
        elif url.startswith("https://www.youtube.com/watch"):
            yt_id = up.parse_qs(up.urlparse(url).query)["v"][0]
            return {
                "embedUrl": "https://www.youtube.com/embed/" + yt_id
            }
        elif url.startswith("https://youtu.be/"):
            yt_id = url.replace("https://youtu.be/", "")
            return {
                "embedUrl": "https://www.youtube.com/embed/" + yt_id
            }
        return None


if __name__ == "__main__":
    AoeImport().process()
