from EduSharingHelpers import EduSharingImport
from HoouEduSharingMapping import HoouEduSharingMapping


class HoouEduSharingImport(EduSharingImport):
    def __init__(self):
        super().__init__("HOOU_edusharing", "oer.hoou.de", mapping=HoouEduSharingMapping(), excerpt=True)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta


if __name__ == "__main__":
    HoouEduSharingImport().process()
