import logging
import re
import requests
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from DOABMapping import DOABMapping
from common.OersiImporter import OersiImporter


def find_all(metadata, key, key_field="key"):
    return [x for x in metadata if x[key_field].startswith(key)]


def find_all_values(metadata, key, value_field="value", key_field="key"):
    items = find_all(metadata, key, key_field=key_field)
    return list(filter(None, map(lambda x: x[value_field], items)))


def to_date(datetime_string):
    if datetime_string is not None:
        if len(datetime_string) == 4:
            return datetime_string + "-01-01"
        elif len(datetime_string) == 7:
            return datetime_string + "-01"
        return datetime_string[:10]
    return None


class DOABImport(OersiImporter):
    def __init__(self, config=None):
        super().__init__(config=config, mapping=DOABMapping())
        self.server_url = "https://directory.doabooks.org"
        self.headers = {'User-Agent': self.user_agent, 'Accept': 'application/json'}
        if self.get_name() in self.config and "included_subjects" in self.config[self.get_name()]:
            self.included_subjects = self.config[self.get_name()]["included_subjects"].copy()
        else:
            self.included_subjects = ["Analytical+chemistry", "Artificial+intelligence", "Astrophysics", "Biochemistry"]
        self.items_per_request = 25
        self.offset = 0

    def get_name(self):
        return "DOAB"

    def contains_record(self, record_id):
        return record_id.startswith(self.server_url + "/handle/")

    def load_single_record(self, record_id):
        url_match = re.match("^" + self.server_url + "/handle/([0-9./]+)", record_id)
        if url_match:
            handle = url_match.group(1)
            params = {"query": 'handle:"' + handle + '"', "expand": "metadata,bitstreams"}
            resp = requests.get(self.server_url + "/rest/search", headers=self.headers, params=params)
            json = resp.json()
            if len(json) == 0:
                return {"response_status_code": 404}
            return json[0]
        return None

    def load_next(self):
        while self.included_subjects:
            subject = self.included_subjects[0]
            params = {"query": 'dc.subject.classification:"' + subject + '"', "expand": "metadata,bitstreams", "limit": self.items_per_request, "offset": self.offset}
            resp = requests.get(self.server_url + "/rest/search", headers=self.headers, params=params)
            self.offset += self.items_per_request
            if resp.status_code != 200:
                self.included_subjects.pop(0)
                self.offset = 0
                self.stats.add_failure()
                logging.info("Could not fetch records %s: %s", subject, resp)
                continue
            records = resp.json()
            if len(records) == 0:
                self.included_subjects.pop(0)
                self.offset = 0
                continue
            return records
        return None

    def parse_person_name(self, name):
        parts = name.split(',')
        parts.reverse()
        return " ".join(parts).strip()

    def get_mapped_subject_for_classification_code(self, code, separator):
        if not code:
            return None
        value = self.mapping.get("subject", code)
        if value is None:
            idx = code.rfind(separator)
            return self.get_mapped_subject_for_classification_code(code[:idx], separator) if idx >= 0 else None
        return value

    def to_search_index_metadata(self, record):
        metadata = record["metadata"]
        identifier = self.server_url + "/handle/" + record["handle"]
        languages = list(filter(None, map(lambda x: self.mapping.get("language", x), find_all_values(metadata, "dc.language", "code"))))
        descriptions = list(filter(None, map(lambda x: x, find_all_values(metadata, "dc.description"))))
        lrts = list(set(filter(None, map(lambda x: self.mapping.get("lrt", x), find_all_values(metadata, "dc.type")))))
        subjects = list(set(filter(None, map(lambda c: self.get_mapped_subject_for_classification_code(c, "::"), find_all_values(metadata, "dc.subject.classification")))))
        contributor_entries = find_all(metadata, "dc.contributor")
        creators = list(map(lambda x: self.parse_person_name(x["value"]), filter(lambda x: x["key"] == "dc.contributor.author", contributor_entries)))
        contributors = list(map(lambda x: self.parse_person_name(x["value"]), filter(lambda x: x["key"] != "dc.contributor.author", contributor_entries)))
        thumbnail_bitstream = find_all(record["bitstreams"], "THUMBNAIL", key_field="bundleName")
        # From doabooks.org: If not noted otherwise all contents are available under Attribution 4.0 (CC BY 4.0)
        license_urls = find_all_values(thumbnail_bitstream[0]["metadata"], "dc.rights.uri") if len(thumbnail_bitstream) > 0 else None
        license_url = license_urls[0] if license_urls else "https://creativecommons.org/licenses/by/4.0/"
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": languages[0] if len(languages) > 0 else "en"}
            ],
            "type": ["LearningResource"],
            "id": identifier,
            "name": record["name"],
            "image": self.server_url + thumbnail_bitstream[0]["retrieveLink"] if len(thumbnail_bitstream) > 0 else None,
            "description": "\n".join(descriptions) if len(descriptions) > 0 else None,
            "inLanguage": languages,
            "license": {"id": license_url},
            "creator": list(map(lambda s: {"type": "Person", "name": s}, creators)),
            "contributor": list(map(lambda s: {"type": "Person", "name": s}, contributors)),
            "learningResourceType": list(map(lambda x: {"id": x}, lrts)),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": find_all_values(metadata, "dc.subject.other"),
            # "sourceOrganization": list(map(lambda x: {"type": "Organization", "name": x, "id": self.mapping.get("org_id", x)}, organizations)),
            # # "dateCreated": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='issued']/xoai:element/xoai:field", None, namespaces),
            "publisher": list(map(lambda x: {"type": "Organization", "name": x}, find_all_values(metadata, "publisher.name"))),
            "datePublished": to_date(min(find_all_values(metadata, "dc.date.issued"), default=None)),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "dateCreated": to_date(min(find_all_values(metadata, "dc.date.available"), default=None)),
                    "dateModified": to_date(record["lastModified"]),
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.server_url
                    }
                }
            ],
            # "encoding": list(map(lambda e: {
            #     "contentUrl": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='url']", None, namespaces),
            #     "encodingFormat": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='format']", None, namespaces),
            #     "contentSize": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='size']", None, namespaces)
            # }, bundles_xml)),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN}
        }
        return meta


if __name__ == "__main__":
    DOABImport().process()
