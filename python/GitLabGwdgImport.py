from GitLabHelpers import GitLabImportBase


class GitLabGwdgImport(GitLabImportBase):
    def __init__(self):
        super().__init__("GWDG GitLab", "gitlab.gwdg.de", default_gitlab_pages_domain="pages.gwdg.de")


if __name__ == "__main__":
    GitLabGwdgImport().process()
