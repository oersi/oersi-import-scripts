from GitLabHelpers import GitLabImportBase


class GitLabRwthAachenImport(GitLabImportBase):
    def __init__(self):
        super().__init__("RWTH Aachen GitLab", "git.rwth-aachen.de", default_gitlab_pages_domain="pages.rwth-aachen.de")


if __name__ == "__main__":
    GitLabRwthAachenImport().process()
