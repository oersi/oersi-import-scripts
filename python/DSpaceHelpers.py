from search_index_import_commons.Helpers import flatten


class XoaiRecordHelper:
    def __init__(self, record):
        self.namespaces = {
            "oai": "http://www.openarchives.org/OAI/2.0/",
            "xoai": "http://www.lyncode.com/xoai"
        }
        self.record = record
        self.dc_xml = None

    def __get_dc_xml(self):
        if self.dc_xml is None:
            self.dc_xml = self.record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", self.namespaces)
        return self.dc_xml

    @staticmethod
    def __parse_person_name(name):
        parts = name.split(',')
        return (parts[1] + " " + parts[0]).strip() if len(parts) >= 2 else name

    def has_status_deleted(self):
        record_status = self.record.find("oai:header", self.namespaces)
        return "status" in record_status.attrib and record_status.get("status") == "deleted"

    def get_bitstreams(self, bundle_name):
        bundles_xml = list(filter(lambda e: bundle_name == e.findtext("xoai:field", None, self.namespaces), self.record.findall("oai:metadata/xoai:metadata/xoai:element[@name='bundles']/xoai:element[@name='bundle']", self.namespaces)))
        bitstreams = flatten(list(map(lambda e: e.findall("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']", self.namespaces), bundles_xml)))
        return bitstreams

    def get_download_encodings(self):
        original_bitstreams = self.get_bitstreams("ORIGINAL")
        return list(map(lambda e: {
            "contentUrl": e.findtext("xoai:field[@name='url']", None, self.namespaces),
            "encodingFormat": e.findtext("xoai:field[@name='format']", None, self.namespaces),
            "contentSize": e.findtext("xoai:field[@name='size']", None, self.namespaces)
        }, original_bitstreams))

    def get_thumbnail(self):
        image_bitstreams = self.get_bitstreams("THUMBNAIL")
        image_urls = list(filter(None, map(lambda e: e.findtext("xoai:field[@name='url']", None, self.namespaces), image_bitstreams)))
        return image_urls[0] if image_urls else None

    def get_license_url(self):
        return self.__get_dc_xml().findtext("xoai:element[@name='rights']/xoai:element[@name='uri']/xoai:element/xoai:field", None, self.namespaces)

    def get_subjects(self):
        return list(set(map(lambda s: s.text.strip(), self.__get_dc_xml().findall("xoai:element[@name='subject']/xoai:element/xoai:field", self.namespaces))))

    def get_contributors(self, contributor_type):
        return list(map(lambda s: self.__parse_person_name(s.text.strip()), self.__get_dc_xml().findall("xoai:element[@name='contributor']/xoai:element[@name='" + contributor_type + "']/xoai:element/xoai:field", self.namespaces)))

