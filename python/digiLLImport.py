from bs4 import BeautifulSoup
import json
import logging
import re
import requests
import urllib.parse
from search_index_import_commons.SitemapReader import SitemapReader
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as Lrt


# NOTE: requires installation of non-standard lib 'python3-bs4'


def parse_description(record):
    desc_tag = record.find(attrs={"class": "entry-content"}).find(string=re.compile(".*Beschreibung.*")).find_parent("div")
    desc = desc_tag.get_text().replace("Beschreibung", "").strip()
    if len(desc) == 0:  # workaround, because of digiLL-pages with broken html-structure
        desc_tag = desc_tag.find_next("p")
        desc = desc_tag.get_text().strip()
    return desc


def parse_creator(record):
    fullname = record.find("img", attrs={"src": re.compile(".*digiLL_kursleiter.+png")}).parent
    if len(fullname.text.strip()) == 0:  # workaround, because of digiLL-pages with names in the next p-tag
        fullname = fullname.find_next(["p", "div"])
    fullname = fullname.text.strip().replace(u'\xa0', u' ')
    fullname = re.sub('“.+”', '', fullname)
    name_list = re.split(', ?| & | und ', fullname)
    name_list = map(lambda n: re.sub('\\(.+\\)', '', n), name_list)
    name_list = map(lambda n: re.sub('B. A.|M. Ed.|weitere|.*Lehrstuhl.*|Lehramts?studierender?|Deutsch als Zweitsprache|Fachdidaktik.+Unterrichtsfach.*', '', n), name_list)
    name_list = map(lambda n: re.sub('.*Zentrum.+Lehrer[*]?[Ii]nnenbildung.*', 'Zentrum für LehrerInnenbildung', n), name_list)
    name_list = map(lambda n: re.sub('Mercator-Institut.+Sprachförderung.*', 'Mercator-Institut für Sprachförderung und Deutsch als Zweitsprache', n), name_list)
    name_list = filter(None, map(lambda n: n.strip(), name_list))
    return list(map(lambda n: to_oersi_creator(n), name_list))


def to_oersi_creator(digill_entity_name):
    entity_name_match = re.match("^((Jun.-Prof. ?|Prof. ?|Dr. ?)+)? ?(.+)", digill_entity_name)
    entity_name = entity_name_match.group(3).strip()
    entity_type = "Person"
    if re.match(".*(Zentrum für LehrerInnenbildung|Gesellschaft|Mercator-Institut|school is open).*", entity_name):
        entity_type = "Organization"
    return {"type": entity_type, "name": entity_name, "honorificPrefix": entity_name_match.group(1).strip() if entity_name_match.group(1) else None}


def parse_institution(record):
    institution = record.find("img", attrs={"src": re.compile(".*university.png")}).nextSibling
    if institution is None or len(institution) == 0:
        institution = record.find("img", attrs={"src": re.compile(".*university.png")}).parent.find_next(["p", "div"]).text
    name_list = re.split(' / | und ', institution.strip())
    name_list = map(lambda n: re.sub('^Ruhr-Universität$', 'Ruhr-Universität Bochum', n), name_list)
    name_list = map(lambda n: re.sub('^Kooperationsprojekt der ', '', n), name_list)
    name_list = map(lambda n: re.sub('^der ', '', n), name_list)
    return list(map(lambda n: {"type": "Organization", "name": n.strip()}, name_list))


def determine_conditions_of_access(course_url):
    if re.match("^(https://moodle\\.uni-due\\.de|https://www\\.uni-muenster\\.de).+", course_url):
        return ConditionsOfAccess.LOGIN
    return ConditionsOfAccess.NO_LOGIN


class DigiLLImport(OersiImporter):
    def __init__(self):
        super().__init__()

    def get_name(self):
        return "digiLL"

    def load_next(self):
        if not hasattr(self, "urls"):
            sitemap = SitemapReader("https://digill.de/course-sitemap.xml", user_agent=self.user_agent,
                                    url_pattern=".+/course/.+")
            self.urls = sitemap.get_url_locations()

        while self.urls:
            loc = self.urls.pop()
            loc_response = requests.get(loc, headers={"User-Agent": self.user_agent})
            if loc_response.status_code != 200:
                self.stats.add_failure()
                logging.info("Could not fetch record %s: %s", loc, loc_response)
                continue
            html_record = BeautifulSoup(loc_response.text, "html.parser")
            return [html_record]
        return None

    def to_search_index_metadata(self, record):
        course_url = record.find("a", string=re.compile("Zum (Lernmodul|Online-Kurs)"))["href"]
        if re.match("^https://www\\.ilias\\.uni-koeln\\.de.+", course_url):     # only available for students of "Universität Köln"
            return None
        #upload_date = record.find("img", attrs={"src": re.compile(".*uploaddatum.png")}).nextSibling
        yoast_data = json.loads(record.find('script', class_=lambda c: c == 'yoast-schema-graph', attrs={"type": "application/ld+json"}).string)
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "id": course_url,
            "name": record.find("header", attrs={"class": "entry-header"}).find("h1").string,
            "conditionsOfAccess": {"id": determine_conditions_of_access(course_url)},
            "image": urllib.parse.quote(record.find("meta", attrs={"property": "og:image"})["content"], safe='/:'),
            "description": parse_description(record),
            "inLanguage": ["de"],
            "license": {"id": record.find("a", attrs={"rel": "license"})["href"]},
            "creator": parse_creator(record),
            "learningResourceType": [{"id": Lrt.COURSE}],
            "sourceOrganization": parse_institution(record),
            "mainEntityOfPage": [
                {
                    "id": record.find("link", attrs={"rel": "canonical"})["href"],
                    "dateCreated": self.to_date(yoast_data["@graph"][0].get("datePublished")),
                    "dateModified": self.to_date(yoast_data["@graph"][0].get("dateModified")),
                    "provider": {
                        "id": "https://digill.de",
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    DigiLLImport().process()
