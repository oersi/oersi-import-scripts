import YouTubeHelpers
from common.OersiImporter import normalize_record
from GitMetadataMapping import GitMetadataMapping
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from search_index_import_commons.GitMetadataConverter import GitMetadataConverter
from search_index_import_commons.Helpers import get, get_list


def get_controlled_vocab_entry(s, transform):
    if isinstance(s, str):
        identifier = transform(s)
        return None if identifier is None else {"id": identifier}
    return s


def init_controlled_vocab_list(record, field_name, transform=lambda x: x):
    return list(filter(None, map(lambda s: get_controlled_vocab_entry(s, transform), get_list(record, field_name))))


def get_creator_name(creator):
    if "name" in creator:
        return creator["name"]
    names = []
    if "givenName" in creator:
        names.append(creator["givenName"])
    if "familyName" in creator:
        names.append(creator["familyName"])
    return " ".join(names) if len(names) > 0 else None


class Converter(GitMetadataConverter):
    def __init__(self, config):
        self.mapping = GitMetadataMapping()
        self.config = config
        self.feature_youtube_extract_videos_from_playlist = config["git"]["features"]["youtube_extract_videos_from_playlist"] if "git" in config and "features" in config["git"] and "youtube_extract_videos_from_playlist" in config["git"]["features"] else False
        self.feature_versions = config["git"]["features"]["versions"] if "git" in config and "features" in config["git"] and "versions" in config["git"]["features"] else False
        self.feature_rating = config["git"]["features"]["rating"] if "git" in config and "features" in config["git"] and "rating" in config["git"]["features"] else False

    def transform(self, record, provider_name, provider_id):
        meta = self.__convert_record__(record, provider_name, provider_id)
        record_id = meta["id"]
        if self.feature_youtube_extract_videos_from_playlist and record_id.startswith("https://www.youtube.com/playlist"):
            return YouTubeHelpers.get_metadata_entries_for_playlist(record_id, self.config, meta)
        meta = normalize_record(meta)
        return meta

    def __convert_record__(self, record, provider_name, provider_id):
        lrts = init_controlled_vocab_list(record, "learningResourceType", lambda s: self.mapping.get("lrt", s))
        creator = list(map(lambda c: {"type": c["type"] if "type" in c else "Person", "name": get_creator_name(c), "id": get(c, "id"), "affiliation": get(c, "affiliation") if c.get("affiliation") else None}, get_list(record, "creator")))
        organizations = get_list(record, "sourceOrganization")
        organizations += list(filter(None, map(lambda c: get(c, "affiliation"), creator)))
        record_license = None
        if "license" in record:
            record_license = {"id": record["license"]} if isinstance(record["license"], str) else record["license"]
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "type": ["LearningResource"],
            "id": record["id"] if "id" in record else get(record["git_metadata"], "identifier"),
            "name": get(record, "name"),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": record["image"] if "image" in record else get(record, "thumbnailUrl"),
            "description": get(record, "description"),
            "inLanguage": get_list(record, "inLanguage"),
            "license": record_license,
            "creator": creator,
            "dateCreated": get(record, "dateCreated"),
            "datePublished": get(record, "datePublished"),
            "about": init_controlled_vocab_list(record, "about"),
            "learningResourceType": lrts,
            "audience": init_controlled_vocab_list(record, "audience"),
            "keywords": get(record, "keywords"),
            "sourceOrganization": organizations,
            "encoding": get_list(record, "encoding"),
            "mainEntityOfPage": [
                {
                    "id": get(record["git_metadata"], "source_url"),
                    "provider": {
                        "type": "Service",
                        "name": provider_name,
                        "id": provider_id
                    }
                }
            ]
        }
        if self.feature_versions:
            meta["hasVersion"] = get(record["git_metadata"], "hasVersion")
        if self.feature_rating:
            meta["aggregateRating"] = {
                "ratingCount": record["git_metadata"]["star_count"],
                "bestRating": 1,
                "ratingValue": 1
            } if "star_count" in record["git_metadata"] else None
        return meta
