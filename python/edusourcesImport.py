import re
import json
import requests
from common.OersiImporter import OersiImporter
from search_index_import_commons.Helpers import filter_none_values
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from edusourcesMapping import edusourcesMapping


def build_date(date):
    if len(date) <= 4:
        return date + "-01-01"
    return date


def get_encodings(record):
    encodings = []
    for file in record["files"]:
        if file["type"] == "website":
            continue
        encoding = {"type": "MediaObject"}
        if re.match("youtube.com", file["url"]) and not re.match("channel/|c/|playlist", file["url"]):
            encoding["embedUrl"] = "https://youtube.com/embed/" + file["url"].split("?v=")[1]
        else:
            encoding["contentUrl"] = file["url"]
        encodings.append(encoding)
    return encodings


def parse_orcid_from_creator(creator):
    creator_orcid = creator["orcid"] if creator["orcid"] and re.match(r"^https?://orcid.org/[0-9X-]+$", creator["orcid"]) else None
    if creator_orcid and creator_orcid.startswith("http:"):
        creator_orcid = creator_orcid.replace("http:", "https:")
    return creator_orcid


class edusourcesImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=edusourcesMapping())
        self.page = 1
        self.items_per_request = 10
        self.domain = "https://search.edusources.nl"

    def get_name(self):
        return "edusources"

    def load_next(self):
        data = {
            "search_text": "",
            "page_size": str(self.items_per_request),
            "page": str(self.page),
            "is_prefilter": 0,
            "filters": [
                {
                    "external_id": "lom_educational_levels",
                    "items": ["WO", "WO - Bachelor", "WO - Master"]
                },
                {
                    "external_id": "copyright.keyword",
                    "items": ["cc-by-nc-sa", "cc-by-nc-sa-40", "cc-by-nc-sa-30", "cc-by-nc-nd", "cc-by-nc-nd-40",
                              "cc-by-nc-nd-30", "cc-by", "cc-by-40", "cc-by-30", "cc-by-sa", "cc-by-sa-40",
                              "cc-by-sa-30", "cc-by-nc", "cc-by-nc-40", "cc-by-nd", "cc-by-nd-40", "cc-by-nd-30"]
                },
                {
                    "external_id": "aggregation_level",
                    "items": ["2", "4", "3"]
                }
            ]
        }
        headers = {
            "Accept": "application/json",
            "Accept-Language": "en",
            "Content-Type": "application/json",
            "Connection": "keep-alive",
            "User-Agent": self.user_agent
        }
        resp = requests.post(self.domain + "/api/v1/materials/search/", data=json.dumps(data), headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from edusources: {}".format(resp))
        records = resp.json()["records"]
        if len(records) == 0:
            return None
        self.page += 1
        return records

    def get_creators(self, record):
        creators = []
        for creator in record["authors"]:
            creator_name = creator["name"]
            creator_orcid = parse_orcid_from_creator(creator)
            if creator_name == "Various Authors" or creator_name == "Various Experts" or "no name" in creator_name:
                continue
            # Check for organization
            if self.mapping.get("organization", creator_name):
                name = self.mapping.get("organization", creator_name)
                organization = {"type": "Organization", "name": name}
                if self.mapping.get("organization_id", name):
                    organization["id"] = self.mapping.get("organization_id", name)
                creators.append(organization)
            elif creator_name.startswith("Dr"):
                if creator_name.startswith("Drs"):
                    # Drs is not a doctorate
                    name = creator_name.split("Drs")[1].strip()
                    creators.append(filter_none_values({"type": "Person", "name": name, "id": creator_orcid}))
                else:
                    # cases: Dr, Dr. ir., Dr.
                    point = creator_name.rfind(".")
                    if point != -1:
                        name = creator_name[point+1:].strip()
                    else:
                        name = creator_name.split("Dr")[1].strip()
                    creators.append(filter_none_values({"type": "Person", "name": name, "honorificPrefix": "Dr.", "id": creator_orcid}))
            else:
                creators.append(filter_none_values({"type": "Person", "name": creator_name, "id": creator_orcid}))
        return creators

    def to_search_index_metadata(self, record):
        subjects = [{"id": self.mapping.get("subject", subject)} for subject in record["disciplines"]
                    if self.mapping.get("subject", subject)]
        encodings = get_encodings(record)
        creators = self.get_creators(record)

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "en"}
            ],
            "type": ["LearningResource"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "about": subjects if len(subjects) > 0 else None,
            "learningResourceType": [{"id": self.mapping.get("lrt", record["technical_type"])}] if self.mapping.get("lrt", record["technical_type"]) else None,
            "id": "https://edusources.nl/materials/" + record["external_id"],
            "name": record["title"],
            "description": record["description"],
            "inLanguage": [record["language"]] if record["language"] and record["language"] not in ["none", "other"] else None,
            "license": {"id": self.mapping.get("license", str(record["copyright"]))},
            "creator": creators,
            "keywords": record["keywords"],
            "sourceOrganization": [{
                "type": "Organization",
                "name": self.mapping.get("organization", organization),
                "id": self.mapping.get("organization_id", organization)
            } for organization in record["publishers"] if self.mapping.get("organization", organization)],
            "publisher": [{
                "type": "Organization",
                "name": self.mapping.get("organization", organization),
                "id": self.mapping.get("organization_id", organization)
            } for organization in record["publishers"] if self.mapping.get("organization", organization)],
            "dateModified": build_date(record["modified_at"]),
            "datePublished": build_date(record["published_at"]),
            "hasPart": [{"id": "https://edusources.nl/materials/" + part} for part in record["has_parts"]],
            "isPartOf": [{"id": "https://edusources.nl/materials/" + part} for part in record["is_part_of"]],
            "encoding": encodings,
            "image": "https://oersi.org/resources/edusources-logo.png",
            "mainEntityOfPage": [
                {
                    "id": "https://edusources.nl/materials/" + record["external_id"],
                    "provider": {
                        "id": "https://edusources.nl",
                        "type": "Service",
                        "name": "edusources"
                    }
                }
            ]
        }

        return meta


if __name__ == "__main__":
    edusourcesImport().process()
