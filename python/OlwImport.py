from datetime import date
import requests
from OlwMapping import OlwMapping
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess


def build_name(user):
    return user["firstName"].strip() + " " + user["lastName"].strip()


def build_date(timestamp):
    return str(date.fromtimestamp(timestamp / 1000))


class OlwImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=OlwMapping())
        self.page = 0

    def get_name(self):
        return "OpenLearnWare"

    def filter_resource(self, record):
        if self.mapping.get("license", str(record["licenseType"])) is not None and len(record["users"]) == 0:
            return False
        return "name" in record and record["name"]

    def load_next(self):
        headers = {"User-Agent": self.user_agent}
        params = {"size": "100", "page": str(self.page)}
        resp = requests.get("https://openlearnware.tu-darmstadt.de/olw-rest-db/api/resource-detailview/",
                            params=params, headers=headers)
        self.page += 1  # starts with page=0
        if resp.status_code != 200:
            raise IOError("Could not fetch records from OLW: {}".format(resp))
        json = resp.json()
        if json["numberOfElements"] == 0:
            return None
        records = list(filter(lambda record: self.filter_resource(record), json["elements"]))
        self.stats.skipped += len(json["elements"]) - len(records)
        return records

    def to_search_index_metadata(self, record):
        record_id = str(record["id"])
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s["code"]), record["areas"])))
        license_url = self.mapping.get("license", str(record["licenseType"]))
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "en"}
            ],
            "type": ["LearningResource"],
            "id": "https://openlearnware.tu-darmstadt.de/resource/" + record_id,
            "name": record["name"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": "https://oersi.de/resources/olw-logo.svg",
            "description": record["description"],
            "inLanguage": list(map(lambda s: self.mapping.get("language", s["name"]), record["languages"])),
            "license": {"id": license_url} if license_url else None,
            "creator": list(map(lambda s: {"type": "Person", "name": build_name(s)}, record["users"])),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": [{"id": self.mapping.get("lrt", record["type"])}],
            # "keywords": list(map(lambda s: s, record["??"])),
            "sourceOrganization": [{"type": "Organization", "name": "Technische Universität Darmstadt", "id": "https://ror.org/05n911h24"}],
            "dateCreated": build_date(record["creationDate"]),
            "datePublished": build_date(record["publicationDate"]),
            "mainEntityOfPage": [
                {
                    "id": "https://openlearnware.tu-darmstadt.de/resource/" + record_id,
                    "dateCreated": build_date(record["uploadDate"]),
                    "provider": {
                        "id": "https://oerworldmap.org/resource/urn:uuid:a8b12952-aff1-4e2b-aca6-42c8395d98fc",
                        "type": "Service",
                        "name": "OpenLearnWare"
                    }
                }
            ]
        }
        if "Video" in [record["type"]]:
            meta["encoding"] = [
                {
                    "type": "MediaObject",
                    "embedUrl": "https://www.openlearnware.de/embed/#/" + record_id,
                }
            ]
        return meta


if __name__ == "__main__":
    OlwImport().process()
