from GitLabHelpers import GitLabImportBase


class GitLabHelmholtzImport(GitLabImportBase):
    def __init__(self):
        # pages 'pages.hzdr.de' is not public -> don't use "default_gitlab_pages_domain" for this instance
        super().__init__("Helmholtz Codebase", "codebase.helmholtz.cloud")


if __name__ == "__main__":
    GitLabHelmholtzImport().process()
