from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
from mapping import hcrt2hcrt


class GitMetadataMapping(SearchIndexImportMapping):
    def __init__(self):
        lrt_mapping = {
            "Kurs": Lrt.COURSE,
            "Course": Lrt.COURSE,
            "Modul": Lrt.OTHER,
        }
        lrt_mapping = {**hcrt2hcrt.mapping, **lrt_mapping}
        self.add_mapping("lrt", lrt_mapping, Lrt.OTHER)

