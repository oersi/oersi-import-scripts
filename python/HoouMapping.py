from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.DcxLrmiAudience as Audience
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class HoouMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "Bilder": Lrt.IMAGE,
            "Video": Lrt.VIDEO
        }, None)

        self.add_mapping("audience", {
            "Lehrer*in": Audience.TEACHER,
            "Schüler*innen": Audience.STUDENT,
            "Student*in": Audience.STUDENT,
        }, None)

        self.add_mapping("language", {
            "Arabisch": "ar",
            "Deutsch": "de",
            "Englisch": "en",
            "Französisch": "fr",
            "Griechisch": "el",
            "Lateinisch": "la",
            "Persisch": "fa",
            "Russisch": "ru",
            "Spanisch": "es",
            "Urdu": "ur",
            "Vietnamesisch": "vi"
        }, None)

        self.add_mapping("subject", {
            "angewandte Kunst": Subject.N007_APPLIED_ART,
            "Archäologie": Subject.N012_ARCHAEOLOGY,
            "Architektur": Subject.N013_ARCHITECTURE,
            "Bauingenieurwesen": Subject.N68_CIVIL_ENGINEERING,
            "Bibliotheks- und Informationswissenschaften": Subject.N06_INFORMATION_AND_LIBRARY_SCIENCES,
            "Bildhauerkunst": Subject.N205_SCULPTING_STATUARY_ART,
            "Bildung und Erziehung": Subject.N33_EDUCATIONAL_SCIENCES,
            "Biologie": Subject.N026_BIOLOGY,
            "Biotechnologie": Subject.N282_BIOTECHNOLOGY,
            "Biowissenschaften": Subject.N42_BIOLOGY,
            "Chemie": Subject.N40_CHEMISTRY,
            "Erziehungswissenschaften": Subject.N33_EDUCATIONAL_SCIENCES,
            "Ethik": Subject.N169_ETHICS,
            "Geografie": Subject.N44_GEOGRAPHY,
            "Geowissenschaften": Subject.N039_GEOSCIENCES_GENERAL,
            "Geschichte": Subject.N05_HISTORY,
            "Geschichte und Geografie": Subject.N05_HISTORY,
            "Informatik": Subject.N079_COMPUTER_SCIENCE,
            "Ingenieurwissenschaften": Subject.N8_ENGINEERING_SCIENCES,
            "Kulturwissenschaft": Subject.N14_CULTURAL_STUDIES_IN_THE_NARROWER_SENSE,
            "Kunst": Subject.N9_ART_ART_THEORY,
            "Landschaftsgestaltung": Subject.N093_LAND_MANAGEMENT_ENGINEERING_LANDSCAPE_DESIGN,
            "Landwirtschaft": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Mathematik": Subject.N37_MATHEMATICS,
            "Medizin": Subject.N107_MEDICINE_GENERAL_MEDICINE,
            "Medizin und Gesundheit": Subject.N107_MEDICINE_GENERAL_MEDICINE,
            "Mikrobiologie": Subject.N42_BIOLOGY,
            "Musik": Subject.N78_MUSIC_MUSICOLOGY,
            "Naturwissenschaften": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Naturwissenschaften und Mathematik": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Pädagogik": Subject.N052_EDUCATIONAL_SCIENCE_PEDAGOGY,
            "Philosophie": Subject.N127_PHILOSOPHY,
            "Physik": Subject.N128_PHYSICS,
            "Politikwissenschaft": Subject.N25_POLITICAL_SCIENCE,
            "Programmieren": Subject.N71_COMPUTER_SCIENCE,
            "Psychologie": Subject.N132_PSYCHOLOGY,
            "Raumplanung": Subject.N134_SPATIAL_PLANNING,
            "Religion": Subject.N136_RELIGIOUS_STUDIES,
            "Sozialwissenschaften": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Soziologie": Subject.N149_SOCIOLOGY,
            "Sport": Subject.N2_SPORTS,
        }, None)
