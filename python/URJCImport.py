import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt

from DSpaceHelpers import XoaiRecordHelper
from URJCMapping import URJCMapping
from common.OersiImporter import OersiImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


class URJCImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=URJCMapping())
        self.loaded = False
        self.oai = OaiPmhReader("https://burjcdigital.urjc.es/server/oai/request", "xoai", "col_10115_19268", self.user_agent)

    def get_name(self):
        return "URJC"

    def load_next(self):
        return self.oai.load_next()

    def to_search_index_metadata(self, record):
        record_helper = XoaiRecordHelper(record)
        namespaces = record_helper.namespaces
        if record_helper.has_status_deleted():
            return None

        subject_texts = record_helper.get_subjects()
        keywords = subject_texts
        license_url = record_helper.get_license_url()
        dc_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", namespaces)
        language = dc_xml.findtext("xoai:element[@name='language']/xoai:element/xoai:element/xoai:field", None, namespaces)
        language_code = self.mapping.get("language", language) if language else None
        description = dc_xml.findtext("xoai:element[@name='description']/xoai:element[@name='abstract']/xoai:element/xoai:field", None, namespaces)
        creator_names = record_helper.get_contributors("author")
        publisher_xml = dc_xml.findall("xoai:element[@name='publisher']/xoai:element/xoai:field", namespaces)
        lrts = set(filter(None, map(lambda t: self.mapping.get("lrt", t.text.strip()),dc_xml.findall("xoai:element[@name='type']/xoai:element/xoai:field", namespaces))))
        identifier = dc_xml.findtext("xoai:element[@name='identifier']/xoai:element[@name='uri']/xoai:element/xoai:field", None, namespaces)
        image = record_helper.get_thumbnail()
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": language_code if language_code else "es"}
            ],
            "type": ["Book", "LearningResource"] if search_index_import_commons.constants.KimHcrt.TEXTBOOK in lrts else ["LearningResource"],
            "id": identifier,
            "name": dc_xml.findtext("xoai:element[@name='title']/xoai:element/xoai:field", None, namespaces),
            "image": image if image else "https://burjcdigital.urjc.es/assets/images/logo-header.png",
            "description": description,
            "inLanguage": [language_code] if language_code else ["es"],
            "license": {"id": license_url} if license_url else None,
            "creator": list(map(lambda s: {"type": "Person", "name": s}, creator_names)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "Universidad Rey Juan Carlos", "id": "https://ror.org/01v5cv687"}],
            "datePublished": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='accessioned']/xoai:element/xoai:field", None, namespaces),
            "publisher": list(map(lambda s: {"type": "Organization", "name": s.text.strip()}, publisher_xml)),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://burjcdigital.urjc.es"
                    }
                }
            ],
            "encoding": record_helper.get_download_encodings(),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN}
        }
        return meta


if __name__ == "__main__":
    URJCImport().process()
