from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class OEPMSMapping(SearchIndexImportMapping):

    OST = "Ostschweizer Fachhochschule"
    FHNW = "Fachhochschule Nordwestschweiz"

    def __init__(self):

        self.add_mapping("lang", {
            "ger": "de",
            "eng": "en"
        }, None)

        self.add_mapping("license", {
            "CC0": "https://creativecommons.org/publicdomain/zero/1.0/",
            "CC BY": "https://creativecommons.org/licenses/by/4.0/",
            "CC BY-NC": "https://creativecommons.org/licenses/by-nc/4.0/",
        }, None)

        self.add_mapping("lrt", {
            "Case study": Lrt.CASE_STUDY,
            "Teaching resource": Lrt.LESSON_PLAN,
            "Conference proceeding": Lrt.TEXT,
            "Script": Lrt.SCRIPT,
            "Other type of teaching resources": Lrt.OTHER,
            "Simulation game": Lrt.EDUCATIONAL_GAME,
            "Research report": Lrt.TEXT,
        }, None)

        self.add_mapping("subject", {
            "Business administration": Subject.N021_BUSINESS_ADMINISTRATION,
            "Communication": Subject.N303_COMMUNICATION_SCIENCE_JOURNALISM,
            "Communication and marketing": Subject.N303_COMMUNICATION_SCIENCE_JOURNALISM,
            "Economics": Subject.N175_ECONOMICS,
            "Informatics and business informatics": Subject.N277_BUSINESS_INFORMATICS,
            "Marketing": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Tourism": Subject.N274_TOURISM_MANAGEMENT,
            "Tourism, Law society and culture": Subject.N274_TOURISM_MANAGEMENT,
            "Mathematics and statistics": Subject.N312_STATISTICS,
        }, None)

        self.add_mapping("org", {
            "FH OST - Ostschweizer Fachhochschule": self.OST,
            "OST - Ostschweizer Fachhochschule": self.OST,
            "OST – Ostschweizer Fachhochschule": self.OST,
            "OST Ostschweizer Fachhochschule": self.OST,
            "Ostschweizer Fachhochschule": self.OST,
            "FH OST": self.OST,
            "OST": self.OST,
            "Ost Ostschweizer Fachhochschule": self.OST,
            "Aduno Gruppe": "Aduno Gruppe",
            "BFH": "Berner Fachhochschule",  # TODO check
            "HSLU": "Hochschule Luzern",
            "HSLU W": "Hochschule Luzern",
            "Hochschule Heilbronn": "Hochschule Heilbronn",
            "FHNW": self.FHNW,
            "Fachhochschule Nordwestschweiz": self.FHNW,
            "Fachhochschule Nordwestschweiz FHNW": self.FHNW,
            "Fachhochschule Nordwestschweiz FHNW, Hochschule für Wirtschaft": self.FHNW,
            "Fachhochschule Nordwestschweiz FHNW, Institute for Competitiveness and Communication und Institut für Nonprofit- und Public Management": self.FHNW,
            "Fachhochschule Nordwestschweiz, Hochschule für Wirtschaft FHNW-HSW": self.FHNW,
            "Hochschule für Wirtschaft FHNW": self.FHNW,
            "Hochschule für Wirtschaft, FHNW": self.FHNW,
            "Hochschule für Wirtschaft": self.FHNW,
            "FHS St. Gallen": self.OST,
            "FHSG": self.OST,
            "IU Internationale Hochschule": "IU Internationale Hochschule",
            "IUBH": "IU Internationale Hochschule",
            "University of Pretoria": "Universität Pretoria",
            "VZ VermögensZentrum AG": "VZ VermögensZentrum AG",
            "ZHAW, School of Management and Law": "ZHAW",
            "Energieagentur St.Gallen": "Energieagentur St.Gallen",
            "Stadtverwaltung Bremgarten": "Stadtverwaltung Bremgarten"
        })

        self.add_mapping("org_id", {
            "Ostschweizer Fachhochschule": "https://ror.org/038mj2660",
            "Berner Fachhochschule": "https://ror.org/02bnkt322",
            "Hochschule Heilbronn": "https://ror.org/04g5gcg95",
            "Fachhochschule Nordwestschweiz": "https://ror.org/04mq2g308",
            "IU Internationale Hochschule": "https://ror.org/04fdat027",
            "Hochschule Luzern": "https://ror.org/04nd0xd48",
            "Universität Pretoria": "https://ror.org/00g0p6g84",
            "ZHAW": "https://ror.org/05pmsvm27",
        }, None)
