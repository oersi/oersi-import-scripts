import requests
from datetime import datetime
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from PhetMapping import PhetMapping


class PhetImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=PhetMapping())
        self.page = 0
        self.items_per_request = 10
        self.domain = "https://phet.colorado.edu"
        self.simulations = []

    def get_name(self):
        return "PhET"

    def load_next(self):
        params = {
            "format": "json",
            "summary": "",
            "levels": "university",
        }
        headers = {
            "Accept": "application/json",
            "Accept-Language": "en",
            "Content-Type": "application/json",
            "User-Agent": self.user_agent
        }

        if len(self.simulations) == 0:
            resp = requests.post(self.domain + "/services/metadata/1.3/simulations",
                                 headers=headers, params=params)
            if resp.status_code != 200:
                raise IOError("Could not fetch records from Phet: {}".format(resp))
            projects = resp.json()["projects"]
            self.simulations = sum([project["simulations"] for project in projects], [])

        responses = [requests.post(self.domain + "/services/metadata/1.3/simulations", headers=headers,
                                   params={"format": "json", "simulation": record["name"]}).json()
                     for record in self.simulations[self.page:self.page + self.items_per_request]]

        records = []
        for response in responses:
            for simulation in response["projects"][0]["simulations"]:
                record = simulation
                if record["legacyType"] == "flash":
                    continue
                record["keywords"] = response["keywords"]
                record["categories"] = response["categories"]
                record["version"] = response["projects"][0]["version"]
                records.append(record)

        if self.page >= len(self.simulations):
            return None
        self.page += self.items_per_request
        return records

    def to_search_index_metadata(self, record):
        record["keywords"] = {int(k): v for k, v in record["keywords"].items()}
        keywords = []
        for keyword_id in record["keywordIds"]:
            keywords.append(record["keywords"][keyword_id]["strings"]["en"])

        categories = {int(k): v for k, v in record["categories"].items()}
        subjects = []
        for subject in record["subjects"]:
            subj_str = {k: v for d in categories[subject]["strings"] for k, v in d.items()}
            subjects.append(subj_str["en"])

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "en"}
            ],
            "type": ["LearningResource"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "about": [{"id": self.mapping.get("subject", subject)} for subject in subjects],
            "learningResourceType": [{"id": "https://w3id.org/kim/hcrt/simulation"}],
            "id": "https://phet.colorado.edu/en/simulations/" + record["name"],
            "name": record["localizedSimulations"]["en"]["title"],
            "description": record["localizedSimulations"]["en"]["description"],
            "inLanguage": ["en", "de"] if "de" in list(record["localizedSimulations"].keys()) else ["en"],
            "image": self.domain + record["localizedSimulations"]["en"]["downloadUrl"].split("_")[0] + "-600.png",
            "license": {"id": "https://creativecommons.org/licenses/by/4.0/"},
            "creator": [{"type": "Person", "name": name.split("(")[0].strip()} for name in
                        record["credits"]["designTeam"].split("<br/>")],
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "University of Colorado Boulder", "id": "https://ror.org/02ttsq026"}],
            "dateModified": datetime.fromtimestamp(record["version"]["timestamp"]).strftime("%Y-%m-%d"),
            "mainEntityOfPage": [
                {
                    "id": "https://phet.colorado.edu/en/simulations/" + record["name"],
                    "provider": {
                        "id": "https://phet.colorado.edu",
                        "type": "Service",
                        "name": "PhET Interactive Simulations"
                    }
                }
            ]
        }

        return meta


if __name__ == "__main__":
    PhetImport().process()
