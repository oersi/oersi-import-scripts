from bs4 import BeautifulSoup
import html
import json
import logging
import re
import requests
from TUDelftOpenCourseWareMapping import TUDelftOpenCourseWareMapping
from search_index_import_commons.Helpers import get
from search_index_import_commons.SitemapReader import SitemapReader
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess


# NOTE: requires installation of non-standard lib 'python3-bs4'


def to_date(datetime_string):
    if datetime_string is not None:
        return datetime_string[:10]
    return None


def convert_entity(entity):
    entity_type = entity["@type"]
    if entity_type == "Organization":
        orga_id = None
        if entity["legalName"] == "Technische Universiteit Delft":
            orga_id = "https://ror.org/02e2c7k09"
        return {"type": entity_type, "name": entity["legalName"], "id": orga_id}
    return {
        "type": entity_type,
        "name": entity["givenName"] + " " + entity["familyName"],
        "honorificPrefix": get(entity, "honorificPrefix")
    }


def map_to_source_organization(entity):
    return {"type": entity["type"], "name": "TU Delft" if entity["name"] == "Technische Universiteit Delft" else entity["name"], "id": entity["id"]}


def load_subjects(user_agent):
    course_pattern = re.compile(".*/courses/[^/]+/?$")
    subjects_response = requests.get("https://ocw.tudelft.nl/programs/", headers={"User-Agent": user_agent})
    if subjects_response.status_code != 200:
        logging.warning("Could not fetch subjects: %s", subjects_response)
        return[]
    html_record = BeautifulSoup(subjects_response.text, "html.parser")
    subject_list = html_record.find("ul", class_="listing")
    subjects_by_course_id = {}
    for subject_entry in subject_list.findAll("li", recursive=False):
        subject = subject_entry.find(class_="course-title").find(string=True, recursive=False).strip()
        all_links = list(map(lambda a: a.attrs["href"], subject_entry.findAll("a")))
        courses = list(filter(lambda href: course_pattern.search(href), all_links))
        for course in courses:
            subjects = subjects_by_course_id[course] if course in subjects_by_course_id else []
            subjects.append(subject)
            subjects_by_course_id[course] = subjects
    return subjects_by_course_id


class TUDelftOpenCourseWareImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=TUDelftOpenCourseWareMapping())

    def get_name(self):
        return "TU Delft OpenCourseWare"

    def load_next(self):
        if not hasattr(self, "subjects"):
            self.subjects = load_subjects(self.user_agent)

        if not hasattr(self, "urls"):
            sitemap = SitemapReader("https://ocw.tudelft.nl/sitemap_index.xml", user_agent=self.user_agent, url_pattern=".*/courses/[^/]+/?$", sitemap_pattern=".*/course-sitemap[0-9]+[.]xml")
            self.urls = sitemap.get_url_locations()

        while self.urls:
            loc = self.urls.pop()
            loc_response = requests.get(loc, headers={"User-Agent": self.user_agent})
            if loc_response.status_code != 200:
                self.stats.add_failure()
                logging.info("Could not fetch record %s: %s", loc, loc_response)
                continue
            html_record = BeautifulSoup(loc_response.text, "html.parser")
            data_tag = html_record.find('script', class_=lambda c: c != 'yoast-schema-graph', attrs={"type": "application/ld+json"})
            record = json.loads(html.unescape(data_tag.string))
            yoast_data_tag = html_record.find('script', class_=lambda c: c == 'yoast-schema-graph', attrs={"type": "application/ld+json"})
            yoast_record = json.loads(html.unescape(yoast_data_tag.string))
            yoast_webpage_record = list(filter(lambda e, url=record["url"]: e["@type"] == "WebPage" and e["url"] == url, yoast_record["@graph"]))[0]
            # some workaround fixes, because some data is not included in the json-metadata, but can be found somewhere else
            if "image" not in record:
                image_node = html_record.find('meta', attrs={"property": "og:image"})
                record["image"] = image_node.attrs["content"] if image_node is not None else None
            if "datePublished" not in record:
                record["datePublished"] = get(yoast_webpage_record, "datePublished")
            if "dateModified" not in record:
                record["dateModified"] = get(yoast_webpage_record, "dateModified")
            return [record]

        return None

    def to_search_index_metadata(self, record):
        lrts = set(filter(None, [self.mapping.get("lrt", record["learningResourceType"])] if "learningResourceType" in record else []))
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), self.subjects[record["url"]]))) if record["url"] in self.subjects else []
        language = get(record, "inLanguage")
        license_exclusions = ["https://ocw.tudelft.nl/courses/engineering-building-nature/"]
        license_url = record["license"] if "license" in record and record["license"] not in license_exclusions else None
        if license_url is None:
            license_url = "https://creativecommons.org/licenses/by-nc-sa/4.0/" # Except where otherwise noted, contents on https://ocw.tudelft.nl are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
        publisher = convert_entity(record["publisher"]) if "publisher" in record else None
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": language if language else "en"}
            ],
            "id": record["url"],
            "name": get(record, "name"),
            "image": get(record, "image"),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "description": get(record, "about"),
            "dateCreated": to_date(get(record, "dateCreated")),
            "datePublished": to_date(get(record, "datePublished")),
            "inLanguage": [language] if language else None,
            "license": {"id": license_url} if license_url is not None else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "publisher": [publisher] if publisher else None,
            "sourceOrganization": [map_to_source_organization(publisher)] if publisher else None,
            "creator": [{"type": "Organization", "name": "TU Delft", "id": "https://ror.org/02e2c7k09"}] + (list(map(lambda a: convert_entity(a), record["author"])) if "author" in record else []),
            "mainEntityOfPage": [
                 {
                     "id": record["url"],
                     "dateCreated": to_date(get(record, "dateCreated")),
                     "dateModified": to_date(get(record, "dateModified")),
                     "provider": {
                         "id": "https://ocw.tudelft.nl",
                         "type": "Service",
                         "name": self.get_name()
                     }
                 }
            ]
        }
        return meta


if __name__ == "__main__":
    TUDelftOpenCourseWareImport().process()
