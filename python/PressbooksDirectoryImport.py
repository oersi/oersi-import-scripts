import logging

from datetime import date
import requests
import html2text
import re
import urllib.parse
from PressbooksDirectoryMapping import PressbooksDirectoryMapping
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as LearningResourceType
from search_index_import_commons.Helpers import parse_entity_name, get


def build_date(timestamp):
    return str(date.fromtimestamp(timestamp))


def remove_suffixes(entity_name, separator=","):
    removable_suffixes = ["Pressbooks Consultant", "contributing editor", "Project Editor", "[Ee]ditors?", "ABD", "A\\.B\\.", "ATC", "B\\.?A\\.?", "BCBA", "BN", "BSc?N?", "CCAA", "CDA", "CHE\\.", "CMfgE/CQE/LSME", "CNE", "CPA \\(inactive\\)", "CRC", "CSCS", "CSFS", "DVM", "[Ee]d\\.? ?[Ss]\\.?", "Ed\\.?D\\.?", "FCSRT", "FRCPC", "FSM\\.", "ID", "J\\.?D\\.?", "Jr.", "LAT", "LBA", "LCPC", "LCSW", "LD", "LICSW", "LMSW", "Lt Col", "M Ed", "MAS", "MAT", "M\\.?A\\.?", "MA\\.ED\\.", "M\\.?B\\.?A\\.?", "MD", "M\\.?Ed\\.?", "M\\.?F\\.?A\\.?", "M\\.?L\\.?I\\.?S\\.?", "MN", "MPA", "MPH", "M\\.S\\.Ed\\.", "MSIT", "MSN/MPH\\(c\\)", "MSN", "MSpVM", "M\\.?Sc?\\.?", "NRP", "O\\.D\\.", "O\\.S\\.B\\.", "OTR/L", "Paramedic", "PharmD", "Ph\\.?D\\.?", "RDN?", "RECE; ECE\\.C", "RN", "RPN", "RRT", "USAF \\(ret\\)"]
    return re.sub("(" + separator + " ?(" + "|".join(removable_suffixes) + ")(?=" + separator + "|$))", "", entity_name.strip())


def post_filter_resource(record):
    has_license = "license" in record and record["license"] and "id" in record["license"] and bool(record["license"]["id"])
    has_author = "creator" in record and record["creator"] and len(record["creator"]) > 0
    return has_license or has_author


class PressbooksDirectoryImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=PressbooksDirectoryMapping())
        self.resources_loaded = False
        self.cursor = None
        self.html2text = html2text.HTML2Text(bodywidth=0)
        self.html2text.ignore_links = True
        self.html2text.ignore_emphasis = True
        self.records_by_network_host = {}
        self.isbn_pattern = re.compile(r".*(97[89]-?(\d-?){10}).*")

    def get_name(self):
        return "Pressbooks Directory"

    def load_next(self):
        if self.resources_loaded:
            return None
        params = {"cursor": self.cursor} if self.cursor is not None else None
        json = self.process_browse_records_request(params=params)
        if "cursor" in json:
            self.cursor = json["cursor"]
        else:
            self.cursor = "LOADED"
            self.resources_loaded = True
        records = json["hits"]
        for record in records:
            if get(record, "isBasedOn"):
                logging.debug("record " + str(record["id"]) + " has isBasedOn " + record["isBasedOn"])
                record["isBasedOnDetails"] = self.load_metadata_for_url(get(record, "isBasedOn"))
        return records

    def process_browse_records_request(self, params=None):
        headers = {"User-Agent": self.user_agent}
        default_params = {"x-algolia-api-key": "587d230fe991746c9a30f6007fb3e06c", "x-algolia-application-id": "K0SNCQLM4A"}
        resp = requests.get("https://k0sncqlm4a-dsn.algolia.net/1/indexes/prod_pressbooks_directory/browse",
                            params={**default_params, **({} if params is None else params)}, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from Pressbooks Directory: {}".format(resp))
        return resp.json()

    def load_metadata_for_url(self, url):
        network_host = urllib.parse.urlparse(url).netloc
        network_host_records = self.load_metadata_for_host(network_host)
        for record in network_host_records:
            if record["url"].rstrip("/") == url.rstrip("/"):
                return record
        return None

    def load_metadata_for_host(self, network_host):
        if network_host in self.records_by_network_host:
            return self.records_by_network_host[network_host]
        records = []
        cursor = None
        while True:
            params = {"filters": "networkHost:" + network_host} if cursor is None else {"cursor": cursor}
            network_host_data = self.process_browse_records_request(params=params)
            records.extend(network_host_data["hits"])
            if "cursor" in network_host_data:
                cursor = network_host_data["cursor"]
            else:
                self.records_by_network_host[network_host] = records
                return records

    def transform(self, data: list) -> list:
        records = super().transform(data)
        filtered_records = list(filter(post_filter_resource, records))
        self.stats.skipped += len(records) - len(filtered_records)
        return filtered_records

    def to_search_index_metadata(self, record):
        license_url = self.mapping.get("license", record["licenseCode"])
        language = record["languageCode"].split("-")[0] if "languageCode" in record and record["languageCode"] else None
        keywords = list(set(filter(None, map(lambda s: s.strip() if s else None, record["about"] if "about" in record else []))))
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), keywords)))
        image = record["thumbnailUrl"] if "thumbnailUrl" in record and record["thumbnailUrl"] else record["image"]
        isbn = self.isbn_pattern.match(record.get("isbn")).group(1).replace("-", "") if record.get("isbn") and self.isbn_pattern.match(record.get("isbn")) else None
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": language if language else "en"}
            ],
            "type": ["Book", "LearningResource"] if isbn else ["LearningResource"],
            "id": record["url"],
            "name": record["name"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "image": image if image else None,
            "description": self._get_description(record),
            "inLanguage": [language] if language else [],
            "isbn": [isbn] if isbn else None,
            "license": {"id": license_url} if license_url else None,
            "creator": list(map(lambda n: self.build_entity_by_name(n), record["author"])),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": [{"id": LearningResourceType.TEXTBOOK}],
            "keywords": keywords,
            "publisher": [{"type": "Organization", "name": record["publisherName"]}] if record["publisherName"] else None,
            "sourceOrganization": list(map(lambda n: {"type": "Organization", "name": n}, record["institutions"])),
            "dateModified": build_date(record["lastUpdated"]),
            "datePublished": record["datePublished"] if "datePublished" in record and record["datePublished"] else None,
            "mainEntityOfPage": [
                {
                    "id": "https://pressbooks.directory/?" + urllib.parse.urlencode({"net": record["networkName"], "q": record["name"]}),
                    "provider": {
                        "id": "https://pressbooks.directory",
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        if get(record, "isBasedOn"):
            is_based_on_details = get(record, "isBasedOnDetails")
            is_based_on_name = get(is_based_on_details, "name")
            if is_based_on_name:
                is_based_on_license_url = self.mapping.get("license", is_based_on_details["licenseCode"])
                meta["isBasedOn"] = [
                    {
                        "id": get(record, "isBasedOn"),
                        "name": is_based_on_name,
                        "creator": list(map(lambda n: self.build_entity_by_name(n), is_based_on_details["author"])),
                        "license": {"id": is_based_on_license_url} if is_based_on_license_url else None
                    }
                ]
        return meta

    def _get_description(self, record):
        description = self.html2text.handle(record["description"]).strip()
        return description if description else None

    def get_entity_type(self, entity_name):
        organization_keywords = ["bureau of", "college", "department of", "design lab", "education", "faculty", "institute", "office", "university", "school of", "support center"]
        if re.match("(?i)^[^,]*(" + "|".join(organization_keywords) + ").*", entity_name):
            return "Organization"
        return "Organization" if entity_name in self.mapping.organization_names else "Person"

    def build_entity_by_name(self, entity_name):
        name = remove_suffixes(entity_name)
        affiliation_name = None
        entity_type = self.get_entity_type(name)
        if entity_type == "Person":
            affiliation_keywords = ["university", "college", "institute"]
            affiliation_match = re.match("(?i)^([^,]+),([^,]*(" + "|".join(affiliation_keywords) + ")[^,]*)$", name)
            if affiliation_match:
                name = affiliation_match.group(1).strip()
                affiliation_name = affiliation_match.group(2).strip()
        entity = parse_entity_name(name)
        entity["type"] = entity_type
        if affiliation_name:
            entity["affiliation"] = {"type": "Organization", "name": affiliation_name}
        return entity


if __name__ == "__main__":
    PressbooksDirectoryImport().process()
