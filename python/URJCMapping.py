from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
from search_index_import_commons.mapping import iso639_2_to_iso639_1


class URJCMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "Book": Lrt.TEXTBOOK,
            "Video": Lrt.VIDEO
        }, None)

        self.add_mapping("language", {
            **iso639_2_to_iso639_1.mapping,
            **{"en": "en", "es": "es"}
        }, None)
