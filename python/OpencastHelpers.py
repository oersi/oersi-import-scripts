import abc
import logging
import re
import requests

from search_index_import_commons.Helpers import get, get_list, parse_entity_name
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as LearningResourceType



def append_string_if_not_contained(base_string, additional_string, separator):
    if not additional_string:
        return base_string
    elif not base_string:
        return additional_string
    elif additional_string in base_string:
        return base_string
    return base_string + separator + additional_string


def get_first(record, field_name):
    l = get_list(record, field_name)
    return l[0] if l else None


def get_image_url(record):
    images = get(get(get(record, "mediapackage"), "attachments"), "attachment")
    if not images:
        return None
    images = [images] if not isinstance(images, list) else images
    image_url = next((x["url"] for x in images if x["type"] == "presenter/player+preview"), None)
    image_url = next((x["url"] for x in images if x["type"] == "presenter/search+preview"), None) if not image_url else image_url
    return image_url


class OpencastConnector:
    def __init__(self, opencast_base_url, user_agent):
        self.opencast_base_url = opencast_base_url
        self.user_agent = user_agent

    @abc.abstractmethod
    def __process_json_search__(self, api_search_path, params):
        pass
    @abc.abstractmethod
    def __process_sorted_search__(self, api_search_path, limit, offset):
        pass
    @abc.abstractmethod
    def find_series_for_episode(self, all_series, episode):
        pass
    @abc.abstractmethod
    def map_default(self, record, mapping, append_description_from_series):
        pass
    @abc.abstractmethod
    def get_subjects(self, record, mapping):
        pass
    @abc.abstractmethod
    def get_description(self, record, append_description_from_series):
        pass

    def retrieve_series(self, limit, offset):
        return self.__process_sorted_search__("/search/series.json", limit, offset)
    def retrieve_series_by_id(self, series_id):
        return self.__process_json_search__("/search/series.json", {"id": series_id})
    def retrieve_episodes(self, limit, offset):
        return self.__process_sorted_search__("/search/episode.json", limit, offset)
    def retrieve_episodes_by_id(self, episode_id):
        return self.__process_json_search__("/search/episode.json", {"id": episode_id})

class OpencastConnectorV15AndBelow(OpencastConnector):
    def __process_json_search__(self, api_search_path, params):
        resp = requests.get(self.opencast_base_url + api_search_path, params=params, headers={"User-Agent": self.user_agent})
        return resp.json()["search-results"]
    def __process_sorted_search__(self, api_search_path, limit, offset):
        return self.__process_json_search__(api_search_path, {"limit": limit, "offset": offset, "sort": "DATE_CREATED"})
    def find_series_for_episode(self, all_series, episode):
        return next((x for x in all_series if x["id"] == get(get(episode, "mediapackage"), "series")), None)
    def map_default(self, record, mapping, append_description_from_series):
        mediapackage = get(record, "mediapackage")
        license_url = mapping.get("license", get(record, "dcLicense")) if mapping else get(record, "dcLicense")
        language = mapping.get("language", get(record, "dcLanguage")) if mapping else get(record, "dcLanguage")
        subject = self.get_subjects(record, mapping)
        meta = {
            "name": record["dcTitle"] if "dcTitle" in record else get(mediapackage, "title"),
            "description": self.get_description(record, append_description_from_series),
            "dateCreated": get(record, "dcCreated"),
            "inLanguage": [language] if language else None,
            "license": {"id": license_url} if license_url else None,
            "about": [{"id": subject}] if subject else None,
        }
        return meta
    def get_subjects(self, record, mapping):
        return mapping.get("subject", get(record, "dcSubject")) if mapping else get(record, "dcSubject")
    def get_description(self, record, append_description_from_series):
        return append_string_if_not_contained(get(record, "dcDescription"), get(record["series"], "dcDescription"), "\n\n") if "series" in record and append_description_from_series else get(record, "dcDescription")

class OpencastConnectorV16(OpencastConnector):
    def __process_json_search__(self, api_search_path, params):
        resp = requests.get(self.opencast_base_url + api_search_path, params=params, headers={"User-Agent": self.user_agent})
        return resp.json()
    def __process_sorted_search__(self, api_search_path, limit, offset):
        return self.__process_json_search__(api_search_path, {"limit": limit, "offset": offset, "sort": "title"})
    def find_series_for_episode(self, all_series, episode):
        return next((x for x in all_series if get_first(x["dc"], "identifier") == get(get(episode, "mediapackage"), "series")), None)
    def map_default(self, record, mapping, append_description_from_series):
        mediapackage = get(record, "mediapackage")
        dc_record = get(record, "dc")
        license_url = mapping.get("license", get_first(dc_record, "license")) if mapping else get_first(dc_record, "license")
        languages = list(filter(None, map(lambda x: mapping.get("language", x) if mapping else x, get_list(dc_record, "language"))))
        subjects = self.get_subjects(record, mapping)
        meta = {
            "name": get_first(dc_record, "title") if get_first(dc_record, "title") else get(mediapackage, "title"),
            "description": self.get_description(record, append_description_from_series),
            "dateCreated": get_first(dc_record, "created"),
            "inLanguage": languages,
            "license": {"id": license_url} if license_url else None,
            "about": list(map(lambda x: {"id": x}, subjects)),
        }
        return meta
    def get_subjects(self, record, mapping):
        dc_record = get(record, "dc")
        return list(filter(None, map(lambda x: mapping.get("subject", x) if mapping else x, get_list(dc_record, "subject"))))
    def get_description(self, record, append_description_from_series):
        episode_description = get_first(get(record, "dc"), "description")
        series_description = get_first(get(record["series"], "dc"), "description") if record.get("series") else None
        return append_string_if_not_contained(episode_description, series_description, "\n\n") if series_description and append_description_from_series else episode_description


class OpencastImportBase(OersiImporter):

    items_per_request = 50

    def __init__(self, name, opencast_domain, mapping=None, append_description_from_series=False):
        super().__init__(mapping=mapping)
        self.name = name
        self.opencast_domain = opencast_domain
        self.opencast_base_url = "https://" + opencast_domain
        self.append_description_from_series = append_description_from_series
        self.offset = 0
        self.oc_series = None
        self.connector = None

    def get_name(self):
        return self.name

    def __init_connector__(self):
        version_resp = requests.get(self.opencast_base_url + "/info/health", headers={"User-Agent": self.user_agent})
        if version_resp.status_code != 200:
            raise IOError("Could not fetch api version from {}: {}".format(self.opencast_base_url, version_resp))
        oc_major_api_version = int(version_resp.json()["releaseId"].split(".")[0])
        logging.debug("OC major API version: %s", oc_major_api_version)
        if oc_major_api_version >= 16:
            self.connector = OpencastConnectorV16(self.opencast_base_url, self.user_agent)
        else:
            self.connector = OpencastConnectorV15AndBelow(self.opencast_base_url, self.user_agent)

    def contains_record(self, record_id):
        return record_id.startswith(self.opencast_base_url + "/")

    def load_single_record(self, record_id):
        if self.connector is None:
            self.__init_connector__()
        url_match = re.match("^" + self.opencast_base_url + "/play/(.+)", record_id)
        if url_match:
            oc_id = url_match.group(1)
            episode_json = self.connector.retrieve_episodes_by_id(oc_id)
            if "result" not in episode_json or len(episode_json["result"]) == 0:
                return {"response_status_code": 404}
            record = episode_json["result"]
            if self.filter_resource(record):
                series_id = record["mediapackage"]["series"]
                series_json = self.connector.retrieve_series_by_id(series_id)
                if "result" in series_json:
                    record["series"] = series_json["result"]
                return record
            else:
                return {"response_status_code": 404}
        return None

    def load_series(self):
        series_offset = 0
        loaded = False
        self.oc_series = []
        while not loaded:
            series_json = self.connector.retrieve_series(self.items_per_request, series_offset)
            series_offset += self.items_per_request
            if "result" not in series_json or len(get_list(series_json, "result")) == 0:
                loaded = True
            else:
                self.oc_series.extend(get_list(series_json, "result"))

    def load_next(self):
        if self.connector is None:
            self.__init_connector__()
        if not self.oc_series:
            self.load_series()
        episodes_json = self.connector.retrieve_episodes(self.items_per_request, self.offset)
        self.offset += self.items_per_request
        if "result" not in episodes_json or len(get_list(episodes_json, "result")) == 0:
            return None
        records = get_list(episodes_json, "result")
        for record in records:
            record["series"] = self.connector.find_series_for_episode(self.oc_series, record)
        return list(filter(lambda r: self.filter_resource(r), records))

    @abc.abstractmethod
    def filter_resource(self, record):
        """instance specific filter for record"""

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    def map_default(self, record):
        meta = self.connector.map_default(record, self.mapping, self.append_description_from_series)
        mediapackage = get(record, "mediapackage")
        url = self.opencast_base_url + "/play/" + str(mediapackage["id"])
        creators = get(get(mediapackage, "creators"), "creator")
        creators = [creators] if creators and not isinstance(creators, list) else creators
        meta["@context"] = [
            "https://w3id.org/kim/amb/context.jsonld", {"@language": meta["inLanguage"][0] if meta.get("inLanguage") else "en"}
        ]
        meta["id"] = url
        meta["image"] = get_image_url(record)
        meta["conditionsOfAccess"] = {"id": ConditionsOfAccess.NO_LOGIN}
        meta["learningResourceType"] = [{"id": LearningResourceType.VIDEO}]
        meta["creator"] = list(map(lambda a: parse_entity_name(a), creators)) if creators else None
        meta["mainEntityOfPage"] = [
            {
                "id": url,
                "dateCreated": meta.get("dateCreated"),
                "dateModified": get(record, "modified"),
                "provider": {
                    "id": self.opencast_base_url,
                    "type": "Service",
                    "name": self.get_name()
                }
            }
        ]
        return meta
    def get_subjects(self, record):
        return self.connector.get_subjects(record, self.mapping)
    def get_description(self, record):
        return self.connector.get_description(record, self.append_description_from_series)
