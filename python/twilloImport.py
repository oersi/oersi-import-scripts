from EduSharingHelpers import get, EduSharingImport


class TwilloImport(EduSharingImport):
    def __init__(self,config=None):
        super().__init__("twillo", "www.twillo.de", use_freetext_authors=False,config=config)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta


if __name__ == "__main__":
    TwilloImport().process()
