from EduSharingHelpers import EduSharingImport
from HessenHubEduSharingMapping import HessenHubEduSharingMapping


class HessenHubEduSharingImport(EduSharingImport):
    def __init__(self):
        super().__init__("HessenHub EduSharing", "edu-sharing.uni-kassel.de", use_freetext_authors=False, mapping=HessenHubEduSharingMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta

    def has_sufficient_metadata(self, meta):
        if not meta["about"]:
            return False
        return super().has_sufficient_metadata(meta)


if __name__ == "__main__":
    HessenHubEduSharingImport().process()
