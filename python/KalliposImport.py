import logging
import re
import time

import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess

from DSpaceHelpers import XoaiRecordHelper
from KalliposMapping import KalliposMapping
from common.OersiImporter import OersiImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


class KalliposImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=KalliposMapping())
        self.server_url = "https://repository.kallipos.gr"
        # col_11419_2 - books, col_11419_3 - chapters, col_11419_4 - learning objects
        self.oai = OaiPmhReader(self.server_url + "/oai/request", "xoai", "col_11419_2", self.user_agent)
        self.namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "xoai": "http://www.lyncode.com/xoai"}
        self.fallback_language = "en"
        self.isbn_pattern = re.compile(r".*(97[89][- ]?(\d[- ]?){10}).*")

    def get_name(self):
        return "KALLIPOS"

    def contains_record(self, record_id):
        return record_id.startswith(self.server_url + "/handle/")

    def load_single_record(self, record_id):
        # TODO implement
        return None

    def load_next(self):
        for i in range(5):
            try:
                records = self.oai.load_next()
                return records
            except IOError:
                logging.debug("Cannot load records -> %s retry...", i)
                time.sleep(1)
        raise IOError("Could not fetch records from {}".format(self.get_name()))

    def get_language_typed_element_field_values_for_all(self, elements, preferred_language):
        values = []
        if elements:
            for element in elements:
                values.extend(self.get_language_typed_element_field_values(element, preferred_language))
        return values

    def get_language_typed_element_field_values(self, element, preferred_language):
        if element is None:
            return []
        values = element.findall("xoai:element[@name='" + preferred_language + "']/xoai:field", self.namespaces)
        if not values and preferred_language != self.fallback_language:
            values = element.findall("xoai:element[@name='" + self.fallback_language + "']/xoai:field", self.namespaces)
        if not values:
            first = element.find("xoai:element", self.namespaces)
            values = element.findall("xoai:element[@name='" + first.get("name") + "']/xoai:field", self.namespaces) if first is not None else []
        return list(map(lambda x: x.text.strip(), values))

    def parse_person_name(self, name):
        parts = name.split(',')
        return (parts[1] + " " + parts[0]).strip()

    def get_orga_name(self, affiliation):
        value = self.mapping.get("org_name", affiliation.split(',')[-1].strip()) if ',' in affiliation else None
        if value is None:
            value = self.mapping.get("org_name", affiliation)
        return value

    def get_mapped_subject_for_classification_code(self, code, separator):
        if not code:
            return None
        value = self.mapping.get("subject", code)
        if value is None:
            idx = code.rfind(separator)
            return self.get_mapped_subject_for_classification_code(code[:idx], separator) if idx >= 0 else None
        return value

    def get_mapped_subjects(self, record):
        heal_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='heal']", self.namespaces)
        subject_codes = heal_xml.findall("xoai:element[@name='classificationcode']/xoai:element/xoai:field", self.namespaces)
        if subject_codes:
            subjects = set(filter(None, map(lambda c: self.get_mapped_subject_for_classification_code(c.text.strip(), "."), subject_codes)))
        else:
            subject_values = self.get_language_typed_element_field_values(heal_xml.find("xoai:element[@name='intermediate']/xoai:element[@name='classificationnameid']", self.namespaces), "en")
            subjects = set(filter(None, map(lambda s: self.get_mapped_subject_for_classification_code(s, "::"), subject_values)))
        return subjects

    def parse_isbn(self, dc_xml, namespaces):
        isbn = dc_xml.findtext("xoai:element[@name='identifier']/xoai:element[@name='isbn']/xoai:element/xoai:field", None, namespaces)
        return self.isbn_pattern.match(isbn).group(1).replace("-", "").replace(" ", "") if isbn and self.isbn_pattern.match(isbn) else None

    def to_search_index_metadata(self, record):
        record_helper = XoaiRecordHelper(record)
        namespaces = record_helper.namespaces
        if record_helper.has_status_deleted():
            return None
        handle_identifier = record.findtext("oai:metadata/xoai:metadata/xoai:element[@name='others']/xoai:field[@name='handle']", None, namespaces)
        identifier = self.server_url + "/handle/" + handle_identifier
        dc_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", namespaces)
        heal_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='heal']", namespaces)
        language = dc_xml.findtext("xoai:element[@name='language']/xoai:element/xoai:element/xoai:field", None, namespaces)
        preferred_language = "en"
        subjects = self.get_mapped_subjects(record)
        license_id = heal_xml.findtext("xoai:element[@name='license']/xoai:element/xoai:field", None, namespaces)
        if license_id is not None and not license_id.startswith("http"):
            license_id = self.mapping.get("license", license_id)
        affiliation = self.get_language_typed_element_field_values(heal_xml.find("xoai:element[@name='authoraffiliation']", namespaces), "el")
        organizations = list(set(filter(None, map(lambda x: self.get_orga_name(x), affiliation))))
        keywords = self.get_language_typed_element_field_values(dc_xml.find("xoai:element[@name='subject']", namespaces), preferred_language)
        titles = self.get_language_typed_element_field_values(dc_xml.find("xoai:element[@name='title']", namespaces), preferred_language)
        if len(titles) == 0:
            return None
        title = titles[0]
        description = self.get_language_typed_element_field_values(dc_xml.find("xoai:element[@name='description']/xoai:element[@name='abstract']", namespaces), preferred_language)
        creators = self.get_language_typed_element_field_values(dc_xml.find("xoai:element[@name='contributor']/xoai:element[@name='author']", namespaces), "en")
        contributors = self.get_language_typed_element_field_values_for_all(heal_xml.findall("xoai:element[@name='contributorname']/xoai:element", namespaces), "en")
        lrts = set(map(lambda t: self.mapping.get("lrt", t.text.strip()), dc_xml.findall("xoai:element[@name='type']/xoai:element/xoai:field", namespaces)))
        isbn = self.parse_isbn(dc_xml, namespaces)
        image = record_helper.get_thumbnail()
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": language if language else "en"}
            ],
            "type": ["Book", "LearningResource"] if isbn else ["LearningResource"],
            "id": identifier,
            "name": title,
            "image": image if image else "https://repository.kallipos.gr/image/logo_en.png",
            "description": "\n".join(description) if len(description) > 0 else None,
            "inLanguage": [language] if language else None,
            "isbn": [isbn.replace("-", "")] if isbn else None,
            "license": {"id": license_id} if license_id else None,
            "creator": list(map(lambda s: {"type": "Person", "name": self.parse_person_name(s)}, creators)),
            "contributor": list(map(lambda s: {"type": "Person", "name": self.parse_person_name(s)}, contributors)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords,
            "sourceOrganization": list(map(lambda x: {"type": "Organization", "name": x, "id": self.mapping.get("org_id", x)}, organizations)),
            # "dateCreated": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='issued']/xoai:element/xoai:field", None, namespaces),
            "datePublished": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='accessioned']/xoai:element/xoai:field", None, namespaces),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.server_url
                    }
                }
            ],
            "encoding": record_helper.get_download_encodings(),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN}
        }
        return meta


if __name__ == "__main__":
    KalliposImport().process()
