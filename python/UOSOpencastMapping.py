from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class UOSOpencastMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("language", {
            "de": "de",
            "deu": "de",
            "en": "en",
            "eng": "en",
            "De": "de",
            "German": "de"
        }, None)

        self.add_mapping("license", {
            "CC0": "https://creativecommons.org/publicdomain/zero/1.0/",
            "CC-BY": "https://creativecommons.org/licenses/by/4.0/",
            "CC-BY-SA": "https://creativecommons.org/licenses/by-sa/4.0/",
            "PD": "https://creativecommons.org/publicdomain/mark/1.0/",
            "PDM": "https://creativecommons.org/publicdomain/mark/1.0/",
            "Creative Commons 3.0: Attribution-NonCommercial-NoDerivs": "https://creativecommons.org/licenses/by-nc-nd/3.0/"
        }, None)

        self.add_mapping("subject", {
            'Geschichte': Subject.N05_HISTORY,
            'Informatik': Subject.N71_COMPUTER_SCIENCE,
            'Internationales Recht': Subject.N28_LAW,
            'Mathe': Subject.N37_MATHEMATICS,
            'Mathematik': Subject.N37_MATHEMATICS
        }, None)
