import unittest
from unittest import mock
import YouTubeHelpers
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://youtube.googleapis.com/youtube/v3/playlistItems':
        with open('tests/resources/youtube_playlistItems_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://youtube.googleapis.com/youtube/v3/videos':
        with open('tests/resources/youtube_videos_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class HoouImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_get_metadata_entries_for_playlist(self, mock_post, mock_get):
        config = {
            "http": {"useragent": "UnitTestBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
            "youtube": {"api_key": "xxxxx"}
        }
        result_metadata = YouTubeHelpers.get_metadata_entries_for_playlist("https://www.youtube.com/playlist?list=unit-test-example", config)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/youtube_expected_001.json')


if __name__ == '__main__':
    unittest.main()
