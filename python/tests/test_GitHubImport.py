import json
import unittest
from unittest import mock
from GitHubImport import GitHubImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://api.github.com/search/repositories':
        if kwargs["params"]["page"] != "1":
            return MockResponse(content=json.dumps({"items": []}), status_code=200)
        with open('tests/resources/github_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://api.github.com/repos/test/GitHubTestOER':
        with open('tests/resources/github_single_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://raw.githubusercontent.com/test/GitHubTestOER/master/metadata.yml':
        with open('tests/resources/github_001_metadata.yml') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://api.github.com/repos/test/notFound':
        return MockResponse(status_code=404)
    elif args[0] == 'https://api.github.com/repos/test/noMetadata':
        with open('tests/resources/github_single_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://raw.githubusercontent.com/test/noMetadata/master/metadata.yml':
        return MockResponse(status_code=404, content="404: Not Found")

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class GitHubImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        GitHubImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/github_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_single_import(self, mock_post, mock_get):
        GitHubImport().process_single_update("https://github.com/test/GitHubTestOER")
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/github_single_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import(self, mock_delete, mock_get):
        GitHubImport().process_single_update("https://github.com/test/notFound")
        self.assertEqual(mock_delete.call_count, 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import_for_missing_metadata(self, mock_delete, mock_get):
        GitHubImport().process_single_update("https://github.com/test/noMetadata")
        self.assertEqual(mock_delete.call_count, 1)


if __name__ == '__main__':
    unittest.main()
