import unittest
from unittest import mock
from eCampusOntarioImport import ECampusOntarioImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://platform.ecampusontario.ca/unified-search-service/ItemSearch/Search':
        with open('tests/resources/eCampusOntario_search_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://platform.ecampusontario.ca/search-service/ItemSearch':
        with open('tests/resources/eCampusOntario_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class ECampusOntarioImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        ECampusOntarioImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/eCampusOntario_expected_001.json')


if __name__ == '__main__':
    unittest.main()
