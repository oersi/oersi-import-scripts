import unittest
from unittest import mock
from twilloImport import TwilloImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "8.0.1"}}', status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/twillo_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_post_invalid_data(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/twillo_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_get_single_update(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def999/metadata':
        with open('tests/resources/twillo_single_003.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/doi-record-67-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_006.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/avtibeu1-without-doi99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_007.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-http401-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=401)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-http403-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=403)
    return MockResponse(status_code=404)


def mocked_requests_get_single_update_contributors(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    return MockResponse(status_code=404)


def mocked_requests_get_embed_url_version_8_or_more(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "8.0.1"}}', status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    return MockResponse(status_code=404)

def mocked_requests_get_content_url_without_ispublic(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "8.0.1"}}', status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_008.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    return MockResponse(status_code=404)

def mocked_requests_get_embed_url_version_less_than_8(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "7.0.1"}}', status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    return MockResponse(status_code=404)


def get_test_config_embed_url_enabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'edu_sharing': {'features': {'generate_embed_url': True}}
    }


def get_test_config_embed_url_disabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'edu_sharing': {'features': {'generate_embed_url': False}}
    }


def get_test_config_embed_url_disabled_generate_embed_url_missing():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'edu_sharing': {'features': {}}
    }


def get_test_config_embed_url_disabled_features_missing():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'edu_sharing': {}
    }

def mocked_requests_aspects_and_child_in_the_node(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_004.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    return MockResponse(status_code=404)

def mocked_requests_aspects_in_the_node_no_child(*args, **kwargs):
    if args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/12abcde3-4567-8fgh-i99e-a1b2c3def123/metadata':
        with open('tests/resources/twillo_single_005.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://www.twillo.de/edu-sharing/rest/node/v1/nodes/-home-/deletion-4567-8fgh-i99e-a1b2c3def123/metadata':
        return MockResponse(status_code=404)
    return MockResponse(status_code=404)


def get_test_config_edu_source_name_embed_url_enabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {'features': {'generate_embed_url': True}}
    }


def get_test_config_edu_source_name_embed_url_disabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {'features': {'generate_embed_url': False}}
    }


def get_test_config_edu_source_name_embed_url_missing():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {'features': {}}
    }


def get_test_config_edu_source_name_embed_url_and_features_missing():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {}
    }


def get_test_config_edu_source_name_enabled_edu_sharing_disabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {'features': {'generate_embed_url': True}},
        'edu_sharing': {'features': {'generate_embed_url': False}}
    }


def get_test_config_edu_source_name_disabled_edu_sharing_enabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'twillo': {'features': {'generate_embed_url': False}},
        'edu_sharing': {'features': {'generate_embed_url': True}}
    }


class TwilloImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        TwilloImport().process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[1][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/twillo_expected_001.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_single_import(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/twillo_single_expected_001.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_single_import_doi(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/doi-record-67-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/twillo_single_expected_006.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_single_import_avtibeu_without_doi(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/avtibeu1-without-doi99e-a1b2c3def123"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 0)

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update_contributors)
    def test_single_import_contributors(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/twillo_single_expected_002.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_series_object_with_main_external_reference_import(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def999"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/twillo_single_expected_003.json')

    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_deletion_via_single_import(self, mock_get, mock_delete):
        record_id = "https://www.twillo.de/edu-sharing/components/render/deletion-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_delete.call_count, 1)

    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_deletion_via_single_import_for_http401(self, mock_get, mock_delete):
        record_id = "https://www.twillo.de/edu-sharing/components/render/deletion-http401-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_delete.call_count, 1)

    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    def test_deletion_via_single_import_for_http403(self, mock_get, mock_delete):
        record_id = "https://www.twillo.de/edu-sharing/components/render/deletion-http403-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_delete.call_count, 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post_invalid_data)
    def test_invalid_data_import(self, mock_post, mock_get):
        TwilloImport().process()
        self.assertEqual(mock_post.call_count, 2)

    def test_embed_url_enabled(self):
        importer=TwilloImport(config=get_test_config_embed_url_enabled())
        self.assertTrue(importer.feature_embed_url)

    def test_embed_url_disabled(self):
        importer = TwilloImport(config=get_test_config_embed_url_disabled())
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_disabled_generate_embed_url_missing(self):
        importer = TwilloImport(config=get_test_config_embed_url_disabled_generate_embed_url_missing())
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_disabled_features_missing(self):
        importer = TwilloImport(config=get_test_config_embed_url_disabled_features_missing())
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_disabled_empty_config(self):
        importer = TwilloImport(config=None)
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_edu_source_name_enable(self):
        importer=TwilloImport(config=get_test_config_edu_source_name_embed_url_enabled())
        self.assertTrue(importer.feature_embed_url)

    def test_embed_url_edu_source_name_disabled(self):
        importer=TwilloImport(config=get_test_config_edu_source_name_embed_url_disabled())
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_edu_source_generate_embed_url_missing(self):
        importer = TwilloImport(config=get_test_config_edu_source_name_embed_url_missing())
        self.assertFalse(importer.feature_embed_url)

    def test_embed_url_edu_source_generate_embed_url_and_features_missing(self):
        importer = TwilloImport(config=get_test_config_edu_source_name_embed_url_and_features_missing())
        self.assertFalse(importer.feature_embed_url)

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_embed_url_version_8_or_more)
    def test_create_embed_url_success_version_8_or_more(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport(config=get_test_config_embed_url_enabled())
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0],
                                                      'tests/resources/twillo_single_expected_001_2.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_content_url_without_ispublic)
    def test_create_no_content_url_for_resources_without_is_public(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assertNotIn("encoding", result_metadata[0], "encoding should not be in the metadata for record with isPublic=false")

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_embed_url_version_less_than_8)
    def test_create_embed_url_fail_version_less_than_8(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport(config=get_test_config_embed_url_enabled())
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0],
                                                      'tests/resources/twillo_single_expected_001_3.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_embed_url_version_8_or_more)
    def test_create_embed_url_edu_source_name_enabled_edu_sharing_disabled(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport(config=get_test_config_edu_source_name_enabled_edu_sharing_disabled())
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0],
                                                          'tests/resources/twillo_single_expected_001_2.json')

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_get_embed_url_version_8_or_more)
    def test_create_embed_url_edu_source_name_disabled_edu_sharing_enabled(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport(config=get_test_config_edu_source_name_disabled_edu_sharing_enabled())
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0],
                                                          'tests/resources/twillo_single_expected_001_3.json')


    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_aspects_and_child_in_the_node)
    def test_aspects_in_the_node_and_child_in_the_node(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 0)

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    @mock.patch('requests.get', side_effect=mocked_requests_aspects_in_the_node_no_child)
    def test_aspects_in_the_node_and_no_child(self, mock_get, mock_post):
        record_id = "https://www.twillo.de/edu-sharing/components/render/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = TwilloImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)

if __name__ == '__main__':
    unittest.main()
