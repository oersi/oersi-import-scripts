import unittest
from unittest import mock
from HessenHubEduSharingImport import HessenHubEduSharingImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://edu-sharing.uni-kassel.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "9.0"}}', status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://edu-sharing.uni-kassel.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/HessenHubEduSharing_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_post_invalid_data(*args, **kwargs):
    if args[0] == 'https://edu-sharing.uni-kassel.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/HessenHubEduSharing_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


class HessenHubEduSharingImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        HessenHubEduSharingImport().process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[1][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/HessenHubEduSharing_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post_invalid_data)
    def test_invalid_data_import(self, mock_post, mock_get):
        HessenHubEduSharingImport().process()
        self.assertEqual(mock_post.call_count, 2)


if __name__ == '__main__':
    unittest.main()
