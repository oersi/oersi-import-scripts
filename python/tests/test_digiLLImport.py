import unittest
from unittest import mock
from digiLLImport import DigiLLImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://digill.de/course-sitemap.xml':
        with open('tests/resources/digill_sitemap_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://digill.de/course/course-01':
        with open('tests/resources/digill_001.html') as f:
            return MockResponse(content=f.read(), status_code=200)
    return MockResponse(status_code=404)


def missing_date_mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://digill.de/course-sitemap.xml':
        with open('tests/resources/digill_sitemap_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://digill.de/course/course-01':
        with open('tests/resources/digill_002.html') as f:
            return MockResponse(content=f.read(), status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class DigiLLImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        DigiLLImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/digill_expected_001.json')

    @mock.patch('requests.get', side_effect=missing_date_mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import_for_record_with_missing_date(self, mock_post, mock_get):
        DigiLLImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/digill_expected_002.json')


if __name__ == '__main__':
    unittest.main()
