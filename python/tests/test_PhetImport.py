import ast
import unittest
from unittest import mock
from PhetImport import PhetImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://phet.colorado.edu/services/metadata/1.3/simulations':
        with open('tests/resources/Phet_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


class PhetImportTest(BaseImportTest):
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post):
        PhetImport().process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[2][1]['json']
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/Phet_expected_001.json')


if __name__ == '__main__':
    unittest.main()
