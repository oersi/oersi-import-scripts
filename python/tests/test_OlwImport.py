import unittest
from unittest import mock
from OlwImport import OlwImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://openlearnware.tu-darmstadt.de/olw-rest-db/api/resource-detailview/':
        if kwargs["params"]["page"] != "0":
            return MockResponse(content='{"numberOfElements": 0}', status_code=200)
        with open('tests/resources/olw_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_get_not_available(*args, **kwargs):
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class OlwImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        OlwImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/olw_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get_not_available)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import_not_available(self, mock_post, mock_get):
        OlwImport().process()
        self.assertEqual(mock_post.call_count, 0)


if __name__ == '__main__':
    unittest.main()
