import json
import unittest
from unittest import mock
from DOABImport import DOABImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://directory.doabooks.org/rest/search':
        if "offset" in kwargs["params"] and kwargs["params"]["offset"] == 0:
            with open('tests/resources/doab_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        if "query" in kwargs["params"] and "20.500.12345/12345" in kwargs["params"]["query"]:
            with open('tests/resources/doab_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        return MockResponse(status_code=200, content=json.dumps([]))

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


def get_test_config():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'DOAB': {'included_subjects': ["Astrophysics"]}
    }


class DOABImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        DOABImport(config=get_test_config()).process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/doab_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_single_import(self, mock_post, mock_get):
        record_id = "https://directory.doabooks.org/handle/20.500.12345/12345"
        importer = DOABImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/doab_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import(self, mock_delete, mock_get):
        record_id = "https://directory.doabooks.org/handle/20.500.12345/98765"
        importer = DOABImport()
        importer.process_single_update(record_id)
        self.assertEqual(mock_delete.call_count, 1)


if __name__ == '__main__':
    unittest.main()
