import unittest
from unittest import mock
from TIBAVPortalImport import TIBAVPortalImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://getinfo.tib.eu/oai/intern/repository/tib':
        if "resumptionToken" in kwargs["params"]:
            return MockResponse(content='<?xml version="1.0" encoding="UTF-8"?><OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd" />', status_code=200)
        with open('tests/resources/tibav_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class TIBAVPortalImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        TIBAVPortalImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assertEqual(len(result_metadata), 2)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/tibav_expected_001.json')
        self.assertNotIn("license", result_metadata[1])


if __name__ == '__main__':
    unittest.main()
