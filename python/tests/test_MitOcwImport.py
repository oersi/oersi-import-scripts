import unittest
from unittest import mock
from MitOcwImport import MitOcwImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://open.mit.edu/api/v0/search/':
        if kwargs["json"]["from"] != 0:
            return MockResponse(content='{"hits": {"hits": []}}', status_code=200)
        elif kwargs["json"]["query"]["bool"]["filter"][0]["term"]["object_type"] == "course":
            with open('tests/resources/mit_ocw_course_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        elif kwargs["json"]["query"]["bool"]["filter"][0]["term"]["object_type"] == "resourcefile":
            with open('tests/resources/mit_ocw_resource_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        else:
            return MockResponse(content='{"hits": {"hits": []}}', status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


class MitOcwImportTest(BaseImportTest):

    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post):
        MitOcwImport().process()
        self.assertEqual(mock_post.call_count, 6)
        result_metadata = mock_post.call_args_list[3][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/mit_ocw_resource_expected_001.json')
        self.assert_result_contains_expected_metadata(result_metadata[1], 'tests/resources/mit_ocw_resource_expected_002.json')
        result_metadata = mock_post.call_args_list[5][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/mit_ocw_course_expected_001.json')


if __name__ == '__main__':
    unittest.main()
