import unittest
from unittest import mock
from PressbooksDirectoryImport import PressbooksDirectoryImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://k0sncqlm4a-dsn.algolia.net/1/indexes/prod_pressbooks_directory/browse':
        if "filters" in kwargs["params"] and "networkHost:example2.pressbooks.pub" == kwargs["params"]["filters"]:
            with open('tests/resources/pressbooksDirectory_networkHost_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        with open('tests/resources/pressbooksDirectory_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class PressbooksDirectoryImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        PressbooksDirectoryImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assertEqual(len(result_metadata), 2)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/pressbooksDirectory_expected_001.json')
        self.assertIsNone(result_metadata[1].get("description"))


if __name__ == '__main__':
    unittest.main()
