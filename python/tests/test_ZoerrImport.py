import unittest
from unittest import mock
from ZoerrImport import ZoerrImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "8.0"}}', status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_get_is_based_on(*args, **kwargs):
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/_about':
        return MockResponse(content='{"version": {"repository": "8.0"}}', status_code=200)
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/relation/v1/relation/-home-/otherid':
        with open('tests/resources/Zoerr_003_relations.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/Zoerr_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_post_invalid_data(*args, **kwargs):
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/Zoerr_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_post_is_based_on(*args, **kwargs):
    if args[0] == 'https://www.zoerr.de/edu-sharing/rest/search/v1/queries/-home-/-default-/ngsearch':
        if kwargs["params"]["skipCount"] != "0":
            return MockResponse(content='{"pagination": {"count": 0}}', status_code=200)
        with open('tests/resources/Zoerr_003.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


def get_test_config_is_based_on_enabled():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'ZOERR': {'features': {'get_is_based_on': True}},
    }


class ZoerrImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        ZoerrImport().process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[1][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/Zoerr_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post_invalid_data)
    def test_invalid_data_import(self, mock_post, mock_get):
        ZoerrImport().process()
        self.assertEqual(mock_post.call_count, 2)

    @mock.patch('requests.get', side_effect=mocked_requests_get_is_based_on)
    @mock.patch('requests.post', side_effect=mocked_requests_post_is_based_on)
    def test_get_is_based_on_enabled(self, mock_post, mock_get):
        importer = ZoerrImport(config=get_test_config_is_based_on_enabled())
        self.assertTrue(importer.feature_is_based_on)
        importer.process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[1][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/Zoerr_expected_003.json')


if __name__ == '__main__':
    unittest.main()
