import unittest

from common.OersiImporter import normalize_record


class OersiImportTest(unittest.TestCase):

    def test_remove_null_values(self):
        data = {
            "description": None,
            "children": [
                {"test": None}
            ],
            "single_child": {
                "test": None
            }
        }
        result = normalize_record(data)
        self.assertNotIn("description", result)
        self.assertNotIn("test", result.get("single_child", {}))
        children = result.get("children", [])
        self.assertEqual(len(children), 1)
        self.assertNotIn("test", children[0])


if __name__ == '__main__':
    unittest.main()