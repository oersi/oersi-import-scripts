import unittest
from unittest import mock
from VlcImport import VlcImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://oer-vlc.de/webservice/rest/server.php':
        with open('tests/resources/vlc_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_get_not_available(*args, **kwargs):
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


def get_test_config():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "OERSIDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) OERSI-Import/0.1"},
        'Virtual Linguistics Campus': {'wstoken': 'tokentokentoken'}
    }


class VlcImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        VlcImport(config=get_test_config()).process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/vlc_expected_001.json')
        self.assert_result_contains_expected_metadata(result_metadata[1], 'tests/resources/vlc_expected_002.json')


if __name__ == '__main__':
    unittest.main()
