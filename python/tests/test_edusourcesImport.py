import ast
import unittest
from unittest import mock
from edusourcesImport import edusourcesImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_post(*args, **kwargs):
    # The kwargs are somehow saved as string instead of dict
    if 'data' in kwargs:
        data = ast.literal_eval(kwargs['data'])
        kwargs['data'] = data
    if args[0] == 'https://search.edusources.nl/api/v1/materials/search/':
        if kwargs['data']['page'] == "1":
            with open('tests/resources/edusources_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        else:
            return MockResponse(content='{"records": []}', status_code=200)

    return default_backend_mock_requests_post(args, kwargs)


class edusourcesImportTest(BaseImportTest):
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post):
        edusourcesImport().process()
        self.assertEqual(mock_post.call_count, 3)
        result_metadata = mock_post.call_args_list[1][1]['json']
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/edusources_expected_001.json')


if __name__ == '__main__':
    unittest.main()
