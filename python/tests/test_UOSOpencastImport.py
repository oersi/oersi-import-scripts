import json
import unittest
from unittest import mock
from UOSOpencastImport import UOSOpencastImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/episode.json':
        if kwargs["params"]["offset"] != 0:
            return MockResponse(content='{"result": []}', status_code=200)
        with open('tests/resources/oc_uos_episodes_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/series.json':
        if kwargs["params"]["offset"] != 0:
            return MockResponse(content='{"result": []}', status_code=200)
        with open('tests/resources/oc_uos_series_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/info/health':
        return MockResponse(content='{"releaseId":"16.4.0","description":"Opencast node\u0027s health status","serviceId":"https://video4.virtuos.uni-osnabrueck.de","version":"1","status":"pass"}', status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_get_single_update(*args, **kwargs):
    if args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/episode.json':
        if kwargs["params"]["id"] == "12abcde3-4567-8fgh-i99e-a1b2c3def123":
            with open('tests/resources/oc_uos_single_episode_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        elif kwargs["params"]["id"] == "deletion-4567-8fgh-i99e-a1b2c3def123":
            return MockResponse(status_code=200, content=json.dumps({"total": 0}))
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/series.json':
        with open('tests/resources/oc_uos_single_series_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/info/health':
        return MockResponse(content='{"releaseId":"16.4.0","description":"Opencast node\u0027s health status","serviceId":"https://video4.virtuos.uni-osnabrueck.de","version":"1","status":"pass"}', status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_get_v14_api(*args, **kwargs):
    if args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/episode.json':
        if kwargs["params"]["offset"] != 0:
            return MockResponse(content='{"search-results": {}}', status_code=200)
        with open('tests/resources/oc_uos_episodes_v14_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/search/series.json':
        if kwargs["params"]["offset"] != 0:
            return MockResponse(content='{"search-results": {}}', status_code=200)
        with open('tests/resources/oc_uos_series_v14_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://video4.virtuos.uni-osnabrueck.de/info/health':
        return MockResponse(content='{"releaseId":"14.0.0"}', status_code=200)
    return MockResponse(status_code=404)


class UOSOpencastImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        UOSOpencastImport().process()
        self.assertEqual(mock_get.call_count, 5)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assertEqual(len(result_metadata), 1)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/oc_uos_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get_v14_api)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import_api_v14(self, mock_post, mock_get):
        UOSOpencastImport().process()
        self.assertEqual(mock_get.call_count, 5)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assertEqual(len(result_metadata), 1)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/oc_uos_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_single_import(self, mock_post, mock_get):
        record_id = "https://video4.virtuos.uni-osnabrueck.de/play/12abcde3-4567-8fgh-i99e-a1b2c3def123"
        importer = UOSOpencastImport()
        self.assertTrue(importer.contains_record(record_id))
        importer.process_single_update(record_id)
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args_list[0][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/oc_uos_single_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get_single_update)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import(self, mock_delete, mock_get):
        record_id = "https://video4.virtuos.uni-osnabrueck.de/play/deletion-4567-8fgh-i99e-a1b2c3def123"
        UOSOpencastImport().process_single_update(record_id)
        self.assertEqual(mock_delete.call_count, 1)
