import unittest
from unittest import mock
from OEPMSImport import OEPMSImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://oepms.org/sitemap_01.xml.gz':
        with open('tests/resources/oepms_sitemap.xml') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://oepms.org/record/1/export/xm':
        with open('tests/resources/oepms_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class OEPMSImportTest(BaseImportTest):
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        OEPMSImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/oepms_expected_001.json')

    # @mock.patch('requests.get', side_effect=mocked_requests_get)
    # @mock.patch('requests.post', side_effect=mocked_requests_post)
    # def test_contains_record(self, mock_post, mock_get):
    #     self.assertTrue(OEPMSImport().contains_record("1"))

    # DEACTIVATED, because unused and in conflict with single list/value handling
    # @mock.patch('requests.get', side_effect=mocked_requests_get)
    # @mock.patch('requests.post', side_effect=mocked_requests_post)
    # def test_single_import(self, mock_post, mock_get):
    #     OEPMSImport().process_single_update("1")
    #     self.assertEqual(mock_post.call_count, 1)
    #     result_metadata = mock_post.call_args[1]["json"]
    #     self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/oepms_expected_001.json')


if __name__ == '__main__':
    unittest.main()
