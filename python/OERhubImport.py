import re
import json
import requests
from OERhubMapping import OERhubMapping
from common.OersiImporter import OersiImporter
from search_index_import_commons.Helpers import fill_if_field_exists, build_user_from_vcard
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as Lrt



def build_date(record_date):
    if not record_date:
        return None
    return record_date[:10]


class OERhubImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=OERhubMapping())
        self.page = 0
        self.items_per_request = 2000
        self.domain = "https://oerhub.at"

    def get_name(self):
        return "OERHub"

    def contains_record(self, record_id):
        return record_id.startswith(self.domain+"/document/")

    def load_single_record(self, record_id):
        headers = {"User-Agent": self.user_agent}
        url_match = re.match("^" + self.domain + "/document/" + "([0-9a-z-]+)", record_id)
        if url_match:
            resp = requests.get(self.domain + "/search/" + url_match.group(1), headers=headers)
            json = resp.json()["data"]
            return json

    def load_next(self):
        params = {"query": "*", "size": str(self.items_per_request), "page": str(self.page)}
        headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent,
                   "sort": json.dumps({"type": "date", " order": "asc"})}
        resp = requests.post(self.domain + "/search/", json=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from OERHub: {}".format(resp))
        record_json = resp.json()
        records = list(fill_if_field_exists(record_json, ["data", "hits", "hits"]))
        if len(records) == 0:
            return None
        self.page += len(records)
        return records

    def prepare_author_name(self, name):
        prepared_author = {"name": None}
        prepared_name = str(name).split("(")[0].split("|")[0].strip()
        match_affiliation = re.search("\([a-zäöüßA-ZÄÖÜ\s]+\)", name)
        if match_affiliation:
            affiliation = match_affiliation.group(0).split("(")[1].split(")")[0].strip()
            mapped_affiliation = self.mapping.get("org", affiliation)
            if mapped_affiliation:
                affiliation = {"affiliation": {
                    "name": mapped_affiliation,
                    "id": self.mapping.get("org_id", mapped_affiliation),
                    "type": "Organization"
                    }
                }
                prepared_author.update(affiliation)
        # This single case just doesn't make sense to generalize
        if prepared_name.lower().find("lawbusters") != -1:
            prepared_name = "Die LawBusters"
        # "Institution:" in name
        match = re.search("Institution:", prepared_name)
        if match:
            author_name = self.mapping.get("creator", prepared_name[match.end():].strip().split(",")[0].strip())
            if author_name:
                prepared_affiliaton = prepared_name[match.end():].strip().split(",")[1].strip()
                affiliation = {
                    "affiliation": {
                        "type": "Organization",
                        "name": self.mapping.get("org", prepared_affiliaton),
                        "id": self.mapping.get("org_id", prepared_affiliaton)
                    }
                }
                prepared_author.update(affiliation)
                prepared_author["name"] = author_name
        # Multiple names in name divided by comma
        match = re.search("\\w+ \\w+,", prepared_name)
        if match:
            prepared_name = [p.strip() for p in prepared_name.split(',') if p and "Universität" not in p]
            prepared_name = [self.mapping.get("creator", p) for p in prepared_name if self.mapping.get("creator", p)]
            return prepared_name
        # Single name but wrong order with comma
        divided_names = prepared_name.split(',')
        if len(divided_names) > 1:
            prepared_name = divided_names[1] + " " + divided_names[0]
        prepared_name = prepared_name.strip()
        if self.mapping.get("creator", prepared_name):
            prepared_name = self.mapping.get("creator", prepared_name)
        prepared_author["name"] = prepared_name.strip()
        return prepared_author

    def append_creator(self, creator, creator_list):
        if type(creator) == list:
            for c in creator:
                if c not in [cl["name"] for cl in creator_list]:
                    if self.mapping.get("creator", c):
                        creator_list.append({"name": self.mapping.get("creator", c),
                                             "type": self.mapping.get("creator_type", c)})
        elif creator["name"] == "Unknown Unknown":
            return
        else:
            if creator["name"] and creator["name"] not in [c["name"] for c in creator_list]:
                creator_list.append({"name": creator["name"],
                                     "type": self.mapping.get("creator_type", creator["name"])})
                if "affiliation" in creator:
                    creator_list[-1].update(creator["affiliation"])

    def to_search_index_metadata(self, record):
        rs = record["_source"]
        rid = fill_if_field_exists(rs, ["technical", "location"]) or rs["oea_object_direct_link"]
        image = fill_if_field_exists(record, ["_source", "technical", "thumbnail", "url"])

        lang = fill_if_field_exists(rs, ["general", "language"])
        dc_lang = fill_if_field_exists(rs, ["dc_language"])
        if not lang and dc_lang:
            for entry in dc_lang:
                lang = self.mapping.get("language", entry)

        language = []
        if lang:
            language = re.findall("([a-z]{2})", lang)

        subjects = []
        for subject in rs["oea_classification_01"]:
            mapped_subject = self.mapping.get("subject", subject["id"])
            subjects_entry = {"id": mapped_subject}
            if mapped_subject and subjects_entry not in subjects:
                subjects.append(subjects_entry)

        lrt = self.mapping.get("lrt", rs["oea_classification_00"].lower())
        if lrt == Lrt.OTHER:
            # Sometimes, for whatever reason, oea_classification_00 is Miscellaneous and the real LRT is set elsewhere:
            other_types = fill_if_field_exists(rs, ["educational", "learningResourceType", "entry"])
            for other_type in other_types:
                if lang in other_type and other_type[lang] and self.mapping.get("lrt", other_type[lang].lower()):
                    lrt = self.mapping.get("lrt", other_type[lang].lower())

        creators_record = []
        creators = fill_if_field_exists(rs, ["lifecycle", "contributes"])
        if creators:
            for creator in creators:
                if creator["role"]["value"] == "Creator" or creator["role"]["value"] == "Author":
                    entities = fill_if_field_exists(creator, ["centities"])
                    if not entities:
                        entities = fill_if_field_exists(creator, ["entities"])
                        for entity in entities:
                            if entity:
                                name = self.prepare_author_name(entity)
                                self.append_creator(name, creators_record)
                    else:
                        for entity in entities:
                            user = build_user_from_vcard(entity["vcard"])
                            user["name"] = self.prepare_author_name(user["name"])
                            self.append_creator(user["name"], creators_record)
        if fill_if_field_exists(rs, ["oea_authors"]):
            for author in fill_if_field_exists(rs, ["oea_authors"]):
                name = self.prepare_author_name(author)
                self.append_creator(name, creators_record)

        record_license = fill_if_field_exists(record, ["_source", "rights", "description"])
        if not record_license:
            record_license = self.mapping.get("license", fill_if_field_exists(rs, ["oea_classification_02"]))

        keywords = []
        for kw in fill_if_field_exists(rs, ["general", "keywords"]) or []:
            if lang and lang in kw:
                keywords.append(kw[lang])

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "en"}
            ],
            "type": ["LearningResource"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "about":  subjects if subjects else None,
            "learningResourceType": [{"id": lrt}] if lrt else None,
            "id": rid,
            "name": rs["oea_title"],
            "description": rs["oea_abstract"],
            "image": image if image else None,
            "inLanguage": language if language else None,
            "license": {"id": record_license} if record_license else None,
            "creator": creators_record if creators_record else None,
            "keywords": keywords if keywords else None,
            "sourceOrganization": [{"type": "Organization", "name": self.mapping.get("org", record["_index"]), "id": self.mapping.get("org_id", record["_index"])}],
            "dateCreated": build_date(rs["oea_classification_03"]),
            "datePublished": build_date(rs["oea_ingest"]),
            "mainEntityOfPage": [
                {
                    "id": "https://oerhub.at/document/" + record["_id"],

                    "provider": {
                        "id": "https://oerhub.at",
                        "type": "Service",
                        "name": "OERhub"
                    }
                }
            ]
        }

        return meta


if __name__ == "__main__":
    OERhubImport().process()
