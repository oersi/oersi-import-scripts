import abc
import html2text
import logging
import re
import requests
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from search_index_import_commons.Helpers import build_user_from_vcard, get_list



def get(record, field_name):
    return filter_empty(record[field_name]) if field_name in record else None


def filter_empty(value):
    if isinstance(value, list):
        return list(filter(None, value))
    return value if value else None


def should_use_download_url(record):
    if record.get("isPublic") is False: # these resources have a problem with inheritance - the detail page is accessible, but the download file are not accessible
        return False
    return get(record, "mediatype") != "link" and get(record, "downloadUrl") is not None


def build_download_encoding(record):
    mimetype = get(record, "mimetype")
    return {
        "type": "MediaObject",
        "contentUrl": get(record, "downloadUrl"),
        "encodingFormat": mimetype if mimetype and re.match("^[a-z]+/[a-zA-Z0-9\\-+_.]+$", mimetype) else None,
        "contentSize": get(record, "size")
        }


def build_embed_url_encoding(record, embed_url_prefix):
    return {
        "type": "MediaObject",
        "embedUrl": embed_url_prefix + record["ref"]["id"] if embed_url_prefix is not None else None
    }


def build_encoding(download_encoding, embed_url_encoding):
    encoding_builder = []
    if download_encoding is not None:
        encoding_builder.extend(download_encoding)
    if embed_url_encoding is not None:
        encoding_builder.extend(embed_url_encoding)
    return encoding_builder


def get_license(record):
    license_url = get(record["license"], "url").replace("/deed.de", "").replace("/deed.en", "") if get(record["license"], "url") else None
    common_license_key = get(record["properties"], "ccm:commonlicense_key")
    license_url = "https://www.gnu.org/licenses/gpl-3.0" if not license_url and common_license_key and common_license_key[0] == "GPL3" else license_url
    license_url = "https://opensource.org/licenses/MIT" if not license_url and common_license_key and common_license_key[0] == "MIT" else license_url
    license_url = "https://www.apache.org/licenses/LICENSE-2.0" if not license_url and common_license_key and common_license_key[0] == "APACHE" else license_url
    return {"id": license_url} if license_url else None


def get_languages(record):
    es_languages = get(record["properties"], "cclom:general_language")
    lng_mapping = {
        "unknown": None,
        "ancient_greek": None,
        "ancient_indian": None,
        "latin": "la",
        "sanskrit": "sa"
    }
    map_lng = lambda x: lng_mapping[x] if x in lng_mapping else x
    languages = list(filter(None, map(map_lng, map(lambda x: re.sub("_.*", "", x), es_languages)))) if es_languages else None
    return languages


def build_date(datetime):
    if datetime is not None:
        return re.sub(r"T.+Z", "", datetime)
    return None


def is_series_record(record):
    child_count = get_list(record["properties"], "virtual:childobjectcount")
    return len(child_count) > 0 and any(child_count)


def get_entity_from_vcard_field(record, field):
    entities = []
    if field in record["properties"] and record["properties"][field]:
        for entry in record["properties"][field]:
            vcard = build_user_from_vcard(entry)
            if vcard is not None:
                entities.append({"name": vcard["name"], "type": vcard["type"], "id": vcard["id"]})
    return entities


class IdMapping:
    def get(self, group, key):
        return key

    def log_missing_keys(self):
        return


class EduSharingConnector:
    def __init__(self, name, edu_sharing_domain, user_agent, metadataset="-default-", criteria=None):
        self.name = name
        self.edu_sharing_base_url = "https://" + edu_sharing_domain
        self.count = 0
        self.user_agent = user_agent
        self.edu_sharing_major_version = None
        self.metadataset = metadataset
        self.criteria = criteria if criteria else []

    def get_headers(self):
        return {"Accept": "application/json", "User-Agent": self.user_agent}

    def get_major_version(self):
        if self.edu_sharing_major_version is None:
            resp = requests.get(self.edu_sharing_base_url + "/edu-sharing/rest/_about", headers=self.get_headers())
            self.edu_sharing_major_version = int(resp.json()["version"]["repository"].split(".")[0])
        return self.edu_sharing_major_version

    def load_next(self):
        items_per_request = 50
        params = {"propertyFilter": "-all-", "maxItems": str(items_per_request), "skipCount": str(self.count), "sortProperties": ["score", "cm:modified"], "sortAscending": "false"}
        if self.get_major_version() < 7:
            url = self.edu_sharing_base_url + "/edu-sharing/rest/search/v1/queriesV2/-home-/" + self.metadataset + "/ngsearch"
            resp = requests.post(url, params=params, json={"criterias": self.criteria, "facettes": []}, headers=self.get_headers())
        else:
            url = self.edu_sharing_base_url + "/edu-sharing/rest/search/v1/queries/-home-/" + self.metadataset + "/ngsearch"
            resp = requests.post(url, params=params, json={"criteria": self.criteria, "facets": []}, headers=self.get_headers())
        self.count += items_per_request
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.name + ": {}".format(resp))
        json = resp.json()
        if json["pagination"]["count"] == 0:
            return None
        return list(json["nodes"])

    def load_single_record(self, node_id):
        if not re.match("^[a-z0-9-]+$", node_id):
            raise ValueError("Invalid node id: " + node_id)
        params = {"propertyFilter": "-all-"}
        url = self.edu_sharing_base_url + "/edu-sharing/rest/node/v1/nodes/-home-/" + node_id + "/metadata"
        resp = requests.get(url, params=params, headers=self.get_headers())
        if resp.status_code == 200:
            record = resp.json()["node"]
            if "aspects" in resp.json()["node"] and 'ccm:io_childobject' in resp.json()["node"]['aspects']:
                return None
        elif resp.status_code in [401, 403, 404]:
            record = {}
        else:
            raise IOError("Could not fetch record from " + self.name + ": {}".format(resp))
        record["response_status_code"] = resp.status_code
        return record

    def get_relations(self, record):
        is_based_on = []
        request_url = self.edu_sharing_base_url + "/edu-sharing/rest/relation/v1/relation/-home-/" + record["ref"]["id"]
        params = {
            "Accept": "application/json",
            "User-Agent": self.user_agent
        }
        response = requests.get(request_url, params=params)
        if response.status_code == 200:
            relations = response.json()["relations"]
            for relation in relations:
                if relation["type"] == "isBasedOn":
                    is_based_on.append({
                        "name": relation["node"]["title"],
                        "type": ["LearningResource"],
                        "id": relation["node"]["content"]["url"],
                        "license": {"id": relation["node"]["license"]["url"]}
                    })
        return is_based_on

class MetadataReporter:
    def __init__(self, provider_name):
        self.provider_name = provider_name
        self.insufficient_data = []
        self.counter_records_with_non_public_mark = 0
        self.counter_records_with_non_public_mark_skipped = 0
        self.counter_records_with_invalid_download_url = 0
        self.counter_records_with_invalid_download_url_skipped = 0
        self.counter_records_with_valid_download_url = 0
        self.counter_records_with_valid_download_url_skipped = 0

    def add_insufficient_metadata_record(self, insufficient_metadata_record):
        self.insufficient_data.append(insufficient_metadata_record)

    def increase_counter_records_with_non_public_mark(self, is_skipped):
        self.counter_records_with_non_public_mark += 1
        if is_skipped:
            self.counter_records_with_non_public_mark_skipped += 1

    def increase_counter_records_with_invalid_download_url(self, is_skipped):
        self.counter_records_with_invalid_download_url += 1
        if is_skipped:
            self.counter_records_with_invalid_download_url_skipped += 1

    def increase_counter_records_with_valid_download_url(self, is_skipped):
        self.counter_records_with_valid_download_url += 1
        if is_skipped:
            self.counter_records_with_valid_download_url_skipped += 1

    def generate_report(self):
        for meta in self.insufficient_data:
            logging.info("%s: reject insufficient metadata %s (%s)", self.provider_name, meta["name"], meta["mainEntityOfPage"][0]["id"])
        logging.info("%s: Records with isPublic=false: %s (%s of these skipped)", self.provider_name, self.counter_records_with_non_public_mark, self.counter_records_with_non_public_mark_skipped)
        logging.info("%s: Records with valid download url: %s (%s of these skipped)", self.provider_name, self.counter_records_with_valid_download_url, self.counter_records_with_valid_download_url_skipped)
        logging.info("%s: Records with invalid download url: %s (%s of these skipped)", self.provider_name, self.counter_records_with_invalid_download_url, self.counter_records_with_invalid_download_url_skipped)


class EduSharingImport(OersiImporter):
    """Supported configurations:

     - freetext_author_blacklist: don't consider the freetext-author-field for records with edu-sharing-url (id) in this list
     - freetext_author_organizations: freetext-author-field values that should be set as "Organization" have to be configured here. Object with values "regex" (the value have to match this regex) and optionally "name" (a name that should be set as name instead of the original name from the freetext-field)
    """

    def __init__(self, name, edu_sharing_domain, mapping=IdMapping(), use_freetext_authors=True,
                 freetext_author_organizations=None, freetext_author_blacklist=None, provider_id=None,
                 excerpt=False, edu_sharing_metadataset="-default-", criteria=None, config=None):
        super().__init__(mapping=mapping, config=config)
        self.name = name
        self.edu_sharing_domain = edu_sharing_domain
        self.render_base_url = "https://" + self.edu_sharing_domain + "/edu-sharing/components/render/"
        self.connector = EduSharingConnector(name, edu_sharing_domain, self.user_agent, metadataset=edu_sharing_metadataset, criteria=criteria)
        self.use_freetext_authors = use_freetext_authors
        self.freetext_author_blacklist = freetext_author_blacklist if freetext_author_blacklist is not None else []
        if self.get_name() in self.config and "freetext_author_blacklist" in self.config[self.get_name()]:
            self.freetext_author_blacklist = self.config[self.get_name()]["freetext_author_blacklist"]
        self.freetext_author_organizations = freetext_author_organizations if freetext_author_organizations is not None else []
        if self.get_name() in self.config and "freetext_author_organizations" in self.config[self.get_name()]:
            self.freetext_author_organizations = self.config[self.get_name()]["freetext_author_organizations"]
        self.html2text = html2text.HTML2Text(bodywidth=0)
        self.html2text.ignore_links = True
        self.html2text.ignore_emphasis = True
        self.provider_id = "https://" + self.edu_sharing_domain if provider_id is None else provider_id
        self.excerpt = excerpt
        self.number_of_processed_batches = 0
        self.metadata_reporter = MetadataReporter(self.name)
        self.feature_embed_url = self.config["edu_sharing"]["features"]["generate_embed_url"] if "edu_sharing" in self.config and "features" in self.config["edu_sharing"] and "generate_embed_url" in self.config["edu_sharing"]["features"] else False
        if self.get_name() in self.config and "features" in self.config[self.get_name()] and "generate_embed_url" in self.config[self.get_name()]["features"]:
            self.feature_embed_url = self.config[self.get_name()]["features"]["generate_embed_url"]
        self.feature_is_based_on = self.config["edu_sharing"]["features"]["get_is_based_on"] if "edu_sharing" in self.config and "features" in self.config["edu_sharing"] and "get_is_based_on" in self.config["edu_sharing"]["features"] else False
        if self.get_name() in self.config and "features" in self.config[self.get_name()] and "get_is_based_on" in self.config[self.get_name()]["features"]:
            self.feature_is_based_on = self.config[self.get_name()]["features"]["get_is_based_on"]

    def get_name(self):
        return self.name

    def contains_record(self, record_id):
        return record_id.startswith(self.render_base_url)

    @staticmethod
    def filter_resource(record):
        if "originalRestrictedAccess" in record and record["originalRestrictedAccess"]:
            return False
        if get(record, "remote") is not None:
            return False
        return True

    def load_next(self):
        if self.excerpt:
            self.number_of_processed_batches += 1
            if self.number_of_processed_batches > 5:
                self.__finalize_import__()
                return None
        records = self.connector.load_next()
        if records is not None:
            size = len(records)
            records = list(filter(self.filter_resource, records))
            self.stats.skipped += size - len(records)
        else:
            self.__finalize_import__()
        return records

    def __finalize_import__(self):
        self.metadata_reporter.generate_report()

    def check_and_report_record(self, record, is_skipped=False):
        non_public = (record.get("isPublic") is False)
        has_download_url = get(record, "mediatype") != "link" and get(record, "downloadUrl") is not None
        if non_public:
            self.metadata_reporter.increase_counter_records_with_non_public_mark(is_skipped)
        if has_download_url:
            if non_public:
                # these resources have a problem with inheritance - the detail page is accessible, but the download file are not accessible
                self.metadata_reporter.increase_counter_records_with_invalid_download_url(is_skipped)
            else:
                self.metadata_reporter.increase_counter_records_with_valid_download_url(is_skipped)

    def load_single_record(self, record_id):
        node_id = record_id.replace(self.render_base_url, "")
        return self.connector.load_single_record(node_id)

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    def map_default(self, record):
        es_subjects = get(record["properties"], "ccm:taxonid")
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), es_subjects))) if es_subjects else []
        es_lrts = get(record["properties"], "ccm:educationallearningresourcetype")
        lrts = set(filter(None, map(lambda s: self.mapping.get("lrt", s), es_lrts))) if es_lrts else []
        es_level = get(record["properties"], "ccm:educationalcontext")
        educational_level = set(filter(lambda x: x is not None and x.startswith("https://w3id.org/kim/educationalLevel/"), map(lambda s: self.mapping.get("educational_level", s), es_level))) if es_level else []
        es_audience = get(record["properties"], "ccm:educationalintendedenduserrole")
        audience = set(filter(lambda x: x is not None and x.startswith("http://purl.org/dcx/lrmi-vocabs/educationalAudienceRole/"), map(lambda s: self.mapping.get("audience", s), es_audience))) if es_audience else []
        keywords = get(record["properties"], "cclom:general_keyword") if get(record["properties"], "cclom:general_keyword") else []
        classification_keywords = get(record["properties"], "ccm:classification_keyword_display") if get(record["properties"], "ccm:classification_keyword_display") else get(record["properties"], "ccm:classification_keyword")
        keywords = list(set(keywords).union(set(classification_keywords))) if classification_keywords else keywords
        organizations = self.get_organizations(record)
        embed_url_prefix = self.get_embed_url_prefix()
        embed_url = [build_embed_url_encoding(record, embed_url_prefix)] if embed_url_prefix is not None else None
        download_url = [build_download_encoding(record)] if should_use_download_url(record) else None
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "type": ["LearningResource"],
            "id": self.get_record_id(record),
            "name": record["title"] if record["title"] is not None else record["name"],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},  # overwrite, if different for a specific es-instance
            "image": get(record["preview"], "url"),
            "description": self.html2text.handle(get(record["properties"], "cclom:general_description")[0]) if get(record["properties"], "cclom:general_description") else None,
            "inLanguage": get_languages(record),
            "license": get_license(record),
            "creator": self.get_authors(record),
            "contributor": self.get_contributors(record),
            "publisher": get_entity_from_vcard_field(record, "ccm:lifecyclecontributer_publisher"),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "educationalLevel": list(map(lambda s: {"id": s}, educational_level)),
            "audience": list(map(lambda s: {"id": s}, audience)) if audience else None,
            "keywords": keywords,
            "sourceOrganization": organizations if organizations else None,
            "dateCreated": build_date(record["createdAt"]),
            "dateModified": build_date(record["modifiedAt"]),
            "isBasedOn": self.connector.get_relations(record) if self.feature_is_based_on else None,
            "mainEntityOfPage": [
                {
                    "id": self.get_edu_sharing_url(record),
                    "dateCreated": build_date(record["createdAt"]),
                    "dateModified": build_date(record["modifiedAt"]),
                    "provider": {
                        "id": self.provider_id,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ],
            "encoding": build_encoding(download_url,embed_url)
        }
        if not self.has_sufficient_metadata(meta):
            self.metadata_reporter.add_insufficient_metadata_record(meta)
            self.check_and_report_record(record, True)
            return None
        self.check_and_report_record(record, False)
        if get(record["properties"], "cm:document_createdISO8601"):
            meta["dateCreated"] = get(record["properties"], "cm:document_createdISO8601")[0]
        if get(record["properties"], "ccm:published_dateISO8601"):
            meta["datePublished"] = get(record["properties"], "ccm:published_dateISO8601")[0]
        return meta

    def get_record_id(self, record):
        if not is_series_record(record):
            if "ccm:published_doi_id" in record["properties"]:
                doi = record["properties"]["ccm:published_doi_id"][0].strip()
                doi_match = re.match(r"(https?://doi.org/)?(10.\d+/[^;/?:@&=+$,!]+)", doi)
                if doi_match:
                    return "https://doi.org/" + doi_match.group(2)
            if "ccm:wwwurl" in record["properties"]:
                return record["properties"]["ccm:wwwurl"][0].strip()
        return self.get_edu_sharing_url(record)

    def get_edu_sharing_url(self, record):
        return self.render_base_url + record["ref"]["id"]

    def has_sufficient_metadata(self, meta):
        if meta["id"].startswith("https://av.tib.eu/"):
            logging.debug("Missing DOI for AV-Portal resource " + meta["id"])
            return False
        if len(meta["about"]) == 0 and len(meta["learningResourceType"]) == 0 and len(meta["creator"]) == 0 and not meta["inLanguage"]:
            return False
        if len(meta["about"]) == 0 and len(meta["creator"]) == 0 and not meta["inLanguage"] and not meta["description"]:
            return False
        return True

    def get_organizations(self, record):
        institution_ids = get(record["properties"], "ccm:university")
        institution_names = get(record["properties"], "ccm:university_DISPLAYNAME")
        institution_names = list(filter(lambda x: x not in ["- Alle -"], institution_names)) if institution_names else []
        matching_institution_arrays = institution_ids is not None and len(institution_ids) == len(institution_names)
        organizations = [
            {"type": "Organization",
             "name": x,
             "id": institution_ids[i] if matching_institution_arrays and institution_ids[i] and institution_ids[i].startswith("https://ror.org/") else None
             } for i, x in enumerate(institution_names)
        ] if institution_names else []
        publishers = list(filter(lambda x: x["type"] == "Organization" and x["name"] not in institution_names, get_entity_from_vcard_field(record, "ccm:lifecyclecontributer_publisher")))
        organizations.extend(publishers)
        initiators = list(filter(lambda x: x["type"] == "Organization" and x["name"] not in institution_names, get_entity_from_vcard_field(record, "ccm:lifecyclecontributer_initiator")))
        organizations.extend(initiators)
        return organizations

    def get_authors(self, record):
        vcard_authors = list(filter(None, map(lambda v: build_user_from_vcard(v), record["properties"]["ccm:lifecyclecontributer_author"]))) if get(record["properties"], "ccm:lifecyclecontributer_author") else []
        edu_sharing_url = self.render_base_url + record["ref"]["id"]
        if self.use_freetext_authors and edu_sharing_url not in self.freetext_author_blacklist and get(record["properties"], "ccm:author_freetext"):
            freetext_authors = self.build_user_from_freetext(get(record["properties"], "ccm:author_freetext"))
        else:
            freetext_authors = []
        authors = []
        for author_name in list(dict.fromkeys(map(lambda a: a["name"], vcard_authors + freetext_authors))):
            author = {"name": author_name}
            vcard_author = next((a for a in vcard_authors if a["name"] == author_name), None)
            freetext_author = next((a for a in freetext_authors if a["name"] == author_name), None)
            if freetext_author is not None:
                author["type"] = freetext_author["type"]
                if "honorificPrefix" in freetext_author:
                    author["honorificPrefix"] = freetext_author["honorificPrefix"]
            if vcard_author is not None:
                author["type"] = vcard_author["type"]
                author["id"] = vcard_author["id"]
            authors.append(author)
        return authors

    def get_contributors(self, record):
        """
        Checks if any "ccm:lifecyclecontributer_*" field is set in the record and if yes, extracts the contributor.

        :param record: Current record
        :return: Full list all contributors
        """
        contributors = []
        contributor_field_names = [
            "ccm:lifecyclecontributer_initiator",
            "ccm:lifecyclecontributer_terminator",
            "ccm:lifecyclecontributer_validator",
            "ccm:lifecyclecontributer_editor",
            "ccm:lifecyclecontributer_graphical_designer",
            "ccm:lifecyclecontributer_technical_implementer",
            "ccm:lifecyclecontributer_content_provider",
            "ccm:lifecyclecontributer_technical_validator",
            "ccm:lifecyclecontributer_educational_validator",
            "ccm:lifecyclecontributer_script_writer",
            "ccm:lifecyclecontributer_instructional_designer",
            "ccm:lifecyclecontributer_subject_matter_expert",
            "ccm:lifecyclecontributer_animation",
            "ccm:lifecyclecontributer_archiv",
            "ccm:lifecyclecontributer_aufnahmeleitung",
            "ccm:lifecyclecontributer_aufnahmeteam",
            "ccm:lifecyclecontributer_ausstattung",
            "ccm:lifecyclecontributer_autor",
            "ccm:lifecyclecontributer_ballett",
            "ccm:lifecyclecontributer_bearbeitete_fassung",
            "ccm:lifecyclecontributer_bildende_kunst",
            "ccm:lifecyclecontributer_bildschnitt",
            "ccm:lifecyclecontributer_buch",
            "ccm:lifecyclecontributer_chor",
            "ccm:lifecyclecontributer_choreographie",
            "ccm:lifecyclecontributer_darsteller",
            "ccm:lifecyclecontributer_design",
            "ccm:lifecyclecontributer_dirigent",
            "ccm:lifecyclecontributer_dvd-grafik_und_design",
            "ccm:lifecyclecontributer_dvd-premastering",
            "ccm:lifecyclecontributer_ensemble",
            "ccm:lifecyclecontributer_fachberatung",
            "ccm:lifecyclecontributer_foto",
            "ccm:lifecyclecontributer_grafik",
            "ccm:lifecyclecontributer_idee",
            "ccm:lifecyclecontributer_interpret",
            "ccm:lifecyclecontributer_interview",
            "ccm:lifecyclecontributer_kamera",
            "ccm:lifecyclecontributer_kommentar",
            "ccm:lifecyclecontributer_komponist",
            "ccm:lifecyclecontributer_konzeption",
            "ccm:lifecyclecontributer_libretto",
            "ccm:lifecyclecontributer_literarische_vorlage",
            "ccm:lifecyclecontributer_maz-bearbeitung",
            "ccm:lifecyclecontributer_mitwirkende",
            "ccm:lifecyclecontributer_moderation",
            "ccm:lifecyclecontributer_musik",
            "ccm:lifecyclecontributer_musikalische_leitung",
            "ccm:lifecyclecontributer_musikalische_vorlage",
            "ccm:lifecyclecontributer_musikgruppe",
            "ccm:lifecyclecontributer_orchester",
            "ccm:lifecyclecontributer_paedagogischer_sachbearbeiter_extern",
            "ccm:lifecyclecontributer_produktionsleitung",
            "ccm:lifecyclecontributer_projektgruppe",
            "ccm:lifecyclecontributer_projektleitung",
            "ccm:lifecyclecontributer_realisation",
            "ccm:lifecyclecontributer_redaktion",
            "ccm:lifecyclecontributer_regie",
            "ccm:lifecyclecontributer_schnitt",
            "ccm:lifecyclecontributer_screen-design",
            "ccm:lifecyclecontributer_spezialeffekte",
            "ccm:lifecyclecontributer_sprecher",
            "ccm:lifecyclecontributer_studio",
            "ccm:lifecyclecontributer_synchronisation",
            "ccm:lifecyclecontributer_synchronregie",
            "ccm:lifecyclecontributer_synchronsprecher",
            "ccm:lifecyclecontributer_tanz",
            "ccm:lifecyclecontributer_text",
            "ccm:lifecyclecontributer_ton",
            "ccm:lifecyclecontributer_trick",
            "ccm:lifecyclecontributer_videotechnik",
            "ccm:lifecyclecontributer_uebersetzung",
            "cm:lifecyclecontributer_uebertragung",
        ]

        for field in contributor_field_names:
            contributors.extend(get_entity_from_vcard_field(record, field))
        return contributors

    def get_embed_url_prefix(self):
        if self.feature_embed_url is True:
            try:
                if self.connector.get_major_version() >= 8:
                    embed_url_prefix = self.connector.edu_sharing_base_url + "/edu-sharing/eduservlet/render?node_id="
                    return embed_url_prefix
                else:
                    return None
            except TypeError:
                logging.warning('the JSON object must be str, bytes or bytearray, not NoneType')
        else:
            return None

    def build_user_from_freetext(self, freetext_users_nested_list):
        name_list = [e for split_users in map(lambda x: re.split(', ?|\n|;| & ', x), freetext_users_nested_list) for e in split_users]
        name_list = map(lambda n: re.sub('\\([^)]+\\)', '', n).strip(), name_list)
        name_list = map(lambda n: re.sub('.+:', '', n).strip(), name_list)
        name_list = list(filter(None, name_list))
        return list(filter(None, map(lambda n: self.convert_freetext_entity(n), name_list)))

    def convert_freetext_entity(self, entity_name):
        name = entity_name
        entity_type = "Person"
        honorific_prefix = None
        for freetext_author_organization in self.freetext_author_organizations:
            if re.match(freetext_author_organization["regex"], entity_name):
                result = {
                    "type": "Organization",
                    "name": freetext_author_organization["name"] if "name" in freetext_author_organization else entity_name,
                }
                if "id" in freetext_author_organization:
                    result["id"] = freetext_author_organization["id"]
                return result

        name_match = re.match("^((?:Dr\\. ?|Prof\\. ?)+)(.+)", entity_name)
        if name_match:
            honorific_prefix = name_match.group(1).strip()
            name = name_match.group(2).strip()
        if re.match(r"[a-zA-Z]{2}.* [a-zA-Z]{2}.*", name):
            return {
                "type": entity_type,
                "name": name,
                "honorificPrefix": honorific_prefix
            }
        return None
