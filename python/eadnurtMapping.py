from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt


class EadnurtMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "Book": Lrt.TEXTBOOK,
        }, Lrt.OTHER)
