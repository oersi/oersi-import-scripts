import re

import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt

from DSpaceHelpers import XoaiRecordHelper
from eadnurtMapping import EadnurtMapping
from common.OersiImporter import OersiImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


class EadnurtImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=EadnurtMapping())
        self.loaded = False
        self.oai = OaiPmhReader("https://crust.ust.edu.ua/server/oai/request", "xoai", "com_123456789_15312", self.user_agent)
        self.isbn_pattern = re.compile(r".*(97[89]-?(\d-?){10}).*")

    def get_name(self):
        return "CRUST"

    def load_next(self):
        return self.oai.load_next()

    def parse_isbn(self, dc_xml, namespaces):
        isbn = dc_xml.findtext("xoai:element[@name='identifier']/xoai:element[@name='isbn']/xoai:element/xoai:field", None, namespaces)
        return self.isbn_pattern.match(isbn).group(1).replace("-", "") if isbn and self.isbn_pattern.match(isbn) else None

    def to_search_index_metadata(self, record):
        record_helper = XoaiRecordHelper(record)
        namespaces = record_helper.namespaces
        if record_helper.has_status_deleted():
            return None

        subject_texts = record_helper.get_subjects()
        keyword_exclusions = ["license (CC", "CC BY", "ліцензія CC", "Creative Commons License", "ліцензії Creative Commons", "license CC", "ліцензія Creative Commons", "license Creative Commons", "відкриті освітні ресурси", "open educational resources", "КЛ", "OER"]
        keywords = list(set(filter(lambda s: not any(map(lambda t: s.startswith(t), keyword_exclusions)), subject_texts)))
        license_url = record_helper.get_license_url()
        dc_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", namespaces)
        language = dc_xml.findtext("xoai:element[@name='language']/xoai:element/xoai:element/xoai:field", None, namespaces)
        language_code = re.sub("_.*", "", language) if language else None
        description = dc_xml.findtext("xoai:element[@name='description']/xoai:element[@name='abstract']/xoai:element[@name='uk_UA']/xoai:field", None, namespaces)
        creator_names = record_helper.get_contributors("author")
        contributor_names = record_helper.get_contributors("advisor")
        publisher_xml = dc_xml.findall("xoai:element[@name='publisher']/xoai:element/xoai:field", namespaces)
        lrts = set(map(lambda t: self.mapping.get("lrt", t.text.strip()),dc_xml.findall("xoai:element[@name='type']/xoai:element/xoai:field", namespaces)))
        identifier = dc_xml.findtext("xoai:element[@name='identifier']/xoai:element[@name='uri']/xoai:element/xoai:field", None, namespaces)
        if identifier.startswith("http://eadnurt.diit.edu.ua/jspui"):
            identifier = identifier.replace("http://eadnurt.diit.edu.ua/jspui", "https://crust.ust.edu.ua")
        isbn = self.parse_isbn(dc_xml, namespaces)
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "uk"}
            ],
            "type": ["Book", "LearningResource"] if search_index_import_commons.constants.KimHcrt.TEXTBOOK in lrts else ["LearningResource"],
            "id": identifier,
            "name": dc_xml.findtext("xoai:element[@name='title']/xoai:element/xoai:field", None, namespaces),
            "image": "https://diit.edu.ua/diit/img/home/logo_208.png",
            "description": re.sub("^UKR: ", "", description.strip()) if description else None,
            "inLanguage": [language_code] if language_code else None,
            "isbn": [isbn.replace("-", "")] if isbn else None,
            "license": {"id": license_url} if license_url else None,
            "creator": list(map(lambda s: {"type": "Person", "name": s}, creator_names)),
            "contributor": list(map(lambda s: {"type": "Person", "name": s}, contributor_names)),
            "learningResourceType": list(map(lambda s: {"id": s}, lrts)),
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "Ukrainian State University of Science and Technologies", "id": "https://ror.org/052pe2w94"}],
            # "dateCreated": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='issued']/xoai:element/xoai:field", None, namespaces),
            "datePublished": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='accessioned']/xoai:element/xoai:field", None, namespaces),
            "publisher": list(map(lambda s: {"type": "Organization", "name": s.text.strip()}, publisher_xml)),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://crust.ust.edu.ua"
                    }
                }
            ],
            "encoding": record_helper.get_download_encodings(),
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN}
        }
        return meta


if __name__ == "__main__":
    EadnurtImport().process()
