import xml.etree.ElementTree as Xml
import logging
import requests
from search_index_import_commons.SitemapReader import SitemapReader
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
from search_index_import_commons.Helpers import get
from OEPMSMapping import OEPMSMapping


class OEPMSImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=OEPMSMapping())
        self.baseurl = "https://oepms.org"

    def get_name(self):
        return "OEPMS"

    def contains_record(self, record_id):
        # this method should only check whether this importer is responsible - without executing external requests
        # deactivating for now - this importer is not used atm anyway
        # if not hasattr(self, "urls"):
        #     sitemap = SitemapReader(url=self.baseurl + "/sitemap_01.xml.gz", user_agent=self.user_agent,
        #                             url_pattern=".*/record/[^/]+/?$", sitemap_pattern=".*/sitemap-[0-9]+[.]xml.gz")
        #     self.urls = sitemap.get_url_locations()
        # return any(["/record/" + record_id in url for url in self.urls])
        return False

    def parse_record(self, xml):
        record = []
        xml_record = Xml.fromstring(xml)[0]

        for node in xml_record.iter():
            if "subfield" in node.tag:
                record[-1].append(node.attrib)
                record[-1].append(node.text)
            elif "controlfield" in node.tag or "datafield" in node.tag:
                record.append([node.attrib["tag"],
                               node.attrib["ind1"] if "ind1" in node.attrib else None,
                               node.attrib["ind2"] if "ind2" in node.attrib else None,
                               node.text])
        return record

    def load_single_record(self, record_id):
        loc_response = requests.get(self.baseurl+"/record/"+record_id+"/export/xm", headers={"User-Agent": self.user_agent})
        if loc_response.status_code != 200:
            self.stats.add_failure()
            logging.info("Could not fetch record %s: %s", record_id, loc_response)
            return

        return self.parse_record(loc_response.text.encode('latin-1'))

    def load_next(self):
        if not hasattr(self, "urls"):
            sitemap = SitemapReader(url=self.baseurl + "/sitemap_01.xml.gz", user_agent=self.user_agent,
                                    url_pattern=".*/record/[^/]+/?$", sitemap_pattern=".*/sitemap-[0-9]+[.]xml.gz")
            self.urls = sitemap.get_url_locations()

        while self.urls:
            loc = self.urls.pop()
            loc_response = requests.get(loc+"/export/xm", headers={"User-Agent": self.user_agent})
            if loc_response.status_code != 200:
                self.stats.add_failure()
                logging.info("Could not fetch record %s: %s", loc, loc_response)
                continue

            return [self.parse_record(loc_response.text.encode('latin-1'))]
        return None

    def clean_entry(self, entry):
        new_entry = [e for e in entry if e and e != " " and "\n" not in e]
        if len(new_entry) == 1:
            return new_entry[0]
        else:
            return new_entry

    def clean_record(self, record):
        cleaned_record = {}
        for entry in record:
            if entry[0] in cleaned_record:
                cleaned_record[entry[0]].extend(self.clean_entry(entry[1:]))
            else:
                cleaned_record.update({entry[0]: self.clean_entry(entry[1:])})
        return cleaned_record

    def prepare_name(self, name):
        split = name.split(',')
        return (split[1] + " " + split[0]).strip()

    def prepare_creators(self, creators):
        prepared_creators = []
        names = [self.prepare_name(c) for c in creators[2::5]]
        affiliations = [self.mapping.get("org", a.strip()) for a in creators[4::5]]
        ids = [self.mapping.get("org_id", a) for a in affiliations]

        for i in range(len(names)):
            prepared_creators.append({"name": names[i],
                                      "type": "Person"
                                      })
            if affiliations[i]:
                prepared_creators[-1]["affiliation"] = {
                    "name": affiliations[i],
                    "type": "Organization"
                }
            if ids[i]:
                prepared_creators[-1]["affiliation"]["id"] = ids[i]
        return prepared_creators

    def get_all_links(self, entries):
        links = []
        for i in range(len(entries)):
            if type(entries[i]) == str and entries[i].startswith("http"):
                links.append([entries[i], entries[i-2]])
        return links

    def get_all_keywords(self, record):
        keywords = []
        if "653" in record:
            keywords.extend(record["653"][2::3])
        if "690" in record:
            keywords.extend(record["690"][1::2])
        return keywords

    def to_search_index_metadata(self, record):
        record = self.clean_record(record)
        lang = self.mapping.get("lang", record["041"][-1])
        keywords = self.get_all_keywords(record)
        lrt = self.mapping.get("lrt", record["336"][-1])
        creators = self.prepare_creators(record["700"])
        if "710" in record:
            for creator in record["710"][2::3]:
                creator_name = self.mapping.get("org", creator.strip())
                creator_id = self.mapping.get("org_id", creator_name)
                if creator_name:
                    creators.append({"name": creator_name, "type": "Organization"})
                    if creator_id:
                        creators[-1]["id"] = creator_id
        subjects = [{"id": self.mapping.get("subject", s)} for s in record["650"][1::2]if self.mapping.get("subject", s)] if "650" in record else None
        download_links = self.get_all_links(record["856"])
        doi = (get(record, "024") or get(record, "924"))[-1]

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": lang if lang else "en"}
            ],
            "id": "https://doi.org/" + doi,
            "name": record["245"][-1],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "description": record["520"][-1],
            "datePublished": record["269"][-1],
            "inLanguage": [lang],
            "license": {"id": self.mapping.get("license", record["540"][-1])},
            "about": subjects if subjects else None,
            "learningResourceType": [{"id": lrt}] if lrt else None,
            "encoding": [{"contentUrl": url, "type": "MediaObject", "contentSize": contentsize} for url, contentsize in download_links],
            "keywords": keywords,
            "creator": creators,
            "mainEntityOfPage": [
                 {
                     "id": self.baseurl + "/record/" + record["001"],
                     "datePublished": record["269"][-1],
                     "provider": {
                         "id": "https://oepms.org/",
                         "type": "Service",
                         "name": self.get_name()
                     }
                 }
            ]
        }

        return meta


if __name__ == "__main__":
    OEPMSImport().process()
