from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt


class HoouEduSharingMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "diagram": Lrt.DIAGRAM,
            "figure": Lrt.IMAGE,
            "lecture": Lrt.COURSE,
            "other": Lrt.OTHER,
            "selfstudy": Lrt.OTHER,
            "simulation": Lrt.SIMULATION,
            "slide": Lrt.SLIDE,
            "table": Lrt.OTHER
        }, Lrt.OTHER)

        self.add_mapping("subject", {
        }, None)
