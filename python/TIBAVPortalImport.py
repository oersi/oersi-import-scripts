import logging
import re
import requests
import time
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as Lrt
from TIBAVPortalMapping import TIBAVPortalMapping
from common.OersiImporter import OersiImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


class TIBAVPortalImport(OersiImporter):
    def __init__(self):
        super().__init__(mapping=TIBAVPortalMapping())
        search_query = self.config[self.get_name()]["search_query"] if self.get_name() in self.config and "search_query" in self.config[self.get_name()] else 'documentFormat:el'
        set_spec = "collection~kmo-av_solr"
        if search_query:
            set_spec = set_spec + "~" + search_query
        self.oai = OaiPmhReader("https://getinfo.tib.eu/oai/intern/repository/tib", "oai_dc", set_spec, self.user_agent, timeout=30)
        self.namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/", "dc": "http://purl.org/dc/elements/1.1/"}
        self.id_pattern = re.compile(r"^sid:(\d+)$")
        self.doi_pattern = re.compile(r"^doi:(10.\d+/[^;?:@&=+$,!]+)$")
        self.license_pattern = re.compile(r"^https?://(?!rightsstatements\.org).*$")

    def get_name(self):
        return "TIB AV-Portal"

    def load_next(self):
        for i in range(5):
            try:
                records = self.oai.load_next()
                return records
            except requests.exceptions.ReadTimeout:
                logging.debug("ReadTimeout while loading records -> %s retry...", i + 1)
                time.sleep(1)
        raise IOError("Read timed out while loading records from {}".format(self.get_name()))

    def __build_entity_from_string(self, entity):
        entity_pattern = re.compile(r"(.+), ([^()]+) ?(?:\((.+)\))?")
        name = re.sub(entity_pattern, r"\2 \1", entity).strip()
        name = re.sub(" {2,}", " ", name)
        result = {"name": name, "type": "Person"}
        attributes = re.sub(entity_pattern, r"\3", entity)
        if attributes:
            for attribute in attributes.split(","):
                if attribute.strip().startswith("https://orcid.org/"):
                    result["id"] = attribute.strip()
        return result

    def __get_raw_about_value(self, subject_string):
        pattern = re.compile(r"^\(classificationName=[^)]+\)(.+)$")
        match = pattern.match(subject_string)
        return match.group(1).strip() if match else None

    def __get_description(self, dc_xml):
        description_xml = next((x for x in dc_xml.findall("dc:description", self.namespaces) if x.text and x.text.strip() not in ["Stummfilm", "Silent movie"]), None)
        if description_xml is None:
            return None, None
        description = description_xml.text.strip()
        lng = description_xml.attrib["{http://www.w3.org/XML/1998/namespace}lang"] if "{http://www.w3.org/XML/1998/namespace}lang" in description_xml.attrib else None
        return description, lng

    def to_search_index_metadata(self, record):
        record_status = record.find("oai:header", self.namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None

        dc_xml = record.find("oai:metadata/oai_dc:dc", self.namespaces)
        identifier = next((self.id_pattern.match(x).group(1) for x in map(lambda x: x.text.strip(), dc_xml.findall("dc:identifier", self.namespaces)) if self.id_pattern.match(x)), None)
        if not identifier:
            return None
        doi = next((self.doi_pattern.match(x).group(1) for x in map(lambda x: x.text.strip(), dc_xml.findall("dc:identifier", self.namespaces)) if self.doi_pattern.match(x)), None)
        license_url = next((x.text.strip() for x in dc_xml.findall("dc:rights", self.namespaces) if x.text and self.license_pattern.match(x.text.strip())), None)
        duration = None
        duration_pattern = re.compile("^(.*), (.*):(.*):(.*):(.*)")
        duration_match = next((x for x in map(lambda x: duration_pattern.match(x.text.strip()), dc_xml.findall("dc:format", self.namespaces)) if x), None)
        if duration_match:
            duration = "PT" + duration_match.group(2) + "H" + duration_match.group(3) + "M" + duration_match.group(4)+ "S"
        publishers = list(map(lambda x: x.text.strip(), dc_xml.findall("dc:publisher", self.namespaces)))
        creators = list(filter(None, map(lambda x: self.__build_entity_from_string(x.text.strip()), dc_xml.findall("dc:creator", self.namespaces))))
        contributors = list(filter(None, map(lambda x: self.__build_entity_from_string(x.text.strip()), dc_xml.findall("dc:contributor", self.namespaces))))
        languages = list(map(lambda x: x.text.strip(), dc_xml.findall("dc:language", self.namespaces)))
        all_subjects = list(filter(None, map(lambda x: x.text.strip(), dc_xml.findall("dc:subject", self.namespaces))))
        raw_subjects = list(filter(None, map(lambda x: self.__get_raw_about_value(x), all_subjects)))
        subjects = list(set(filter(None, map(lambda x: self.mapping.get("subject", x), raw_subjects))))
        description, desc_lng = self.__get_description(dc_xml)
        if desc_lng:
            context_lng = desc_lng
        elif len(languages) == 0:
            context_lng = "de"
        else:
            context_lng = "de" if "de" in languages else languages[0]

        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": context_lng}
            ],
            "type": ["LearningResource"],
            "id": ("https://doi.org/" + doi) if doi else ("https://av.tib.eu/media/" + identifier),
            "name": dc_xml.findtext("dc:title[.!='']", None, self.namespaces).strip(),
            "image": "https://av.tib.eu/thumbnail/" + identifier,
            "description": description,
            "inLanguage": languages,
            "license": {"id": license_url} if license_url else None,
            "duration": duration,
            "creator": creators,
            "contributor": contributors,
            "learningResourceType": [{"id": Lrt.VIDEO}],
            "keywords": list(filter(lambda x: not x.startswith("(classificationName="), all_subjects)),
            "about": list(map(lambda x: {"id": x}, subjects)),
            "datePublished": dc_xml.findtext("dc:date", None, self.namespaces),
            "publisher": list(map(lambda s: {"type": "Organization", "name": s}, publishers)),
            "mainEntityOfPage": [
                {
                    "id": "https://av.tib.eu/media/" + identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://av.tib.eu/"
                    }
                }
            ],
            "conditionsOfAccess": {"id": ConditionsOfAccess.NO_LOGIN},
            "encoding": [{"embedUrl": "https://av.tib.eu/player/" + identifier}]
        }
        return meta


if __name__ == "__main__":
    TIBAVPortalImport().process()
