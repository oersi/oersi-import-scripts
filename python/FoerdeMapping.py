from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class FoerdeMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "background_information": Lrt.OTHER,
            "course": Lrt.COURSE,
            "educational_game": Lrt.EDUCATIONAL_GAME,
            "experiment": Lrt.EXPERIMENT,
            "glossary": Lrt.INDEX,
            "h5p": Lrt.OTHER,
            "lesson plan": Lrt.LESSON_PLAN,
            "overview": Lrt.OTHER,
            "presentation": Lrt.SLIDE,
            "reference": Lrt.OTHER,
            "teaching_aids": Lrt.WORKSHEET,
            "video": Lrt.VIDEO,
            "worksheet": Lrt.WORKSHEET,
        }, Lrt.OTHER)

        self.add_mapping("subject", {
            "11": Subject.N40_CHEMISTRY,
            "15": None,
            "57": Subject.N37_MATHEMATICS,
            "92": Subject.N22_SPORTS_SPORTS_SCIENCE,
            "-1": Subject.N0_INTERDISCIPLINARY,
        }, None)
