import re
import requests
import html2text
import xml.etree.ElementTree as ET
from common.OersiImporter import OersiImporter
import search_index_import_commons.constants.KimConditionsOfAccess as ConditionsOfAccess
import search_index_import_commons.constants.KimHcrt as Lrt
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


def set_course_specific_values(record, meta):
    shortname = record.find("KEY[@name='shortname']").findtext("VALUE")
    if re.match("(ASTRO ?\\d+)", shortname):
        meta["creator"] = [
            {"type": "Person", "name": "Jürgen Handke", "honorificPrefix": "Prof. Dr."},
            {"type": "Person", "name": "Ulrich Lotzmann", "honorificPrefix": "Prof. Dr."},
            {"type": "Organization", "name": "Linguistic Engineering Team", "id": "https://oer-vlc.de/pluginfile.php/2/course/section/31/let.pdf"}
        ]
        meta["about"] = [{"id": Subject.N39_PHYSICS_ASTRONOMY}]
    else:
        meta["creator"] = [
            {"type": "Person", "name": "Jürgen Handke", "honorificPrefix": "Prof. Dr."},
            {"type": "Person", "name": "Peter Franke", "honorificPrefix": "Dr."},
            {"type": "Organization", "name": "Linguistic Engineering Team", "id": "https://oer-vlc.de/pluginfile.php/2/course/section/31/let.pdf"}
        ]
        meta["about"] = [{"id": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS}]


class VlcImport(OersiImporter):
    def __init__(self, config=None):
        super().__init__(config=config)
        self.loaded = False
        self.wstoken = self.config[self.get_name()]["wstoken"]  # a token is necessary to access moodle-webservices (has to be configured and provided by a moodle-admin)
        self.moodle_url = "https://oer-vlc.de"

    def get_name(self):
        return "Virtual Linguistics Campus"

    def should_import(self, record):
        shortname = record.find("KEY[@name='shortname']").findtext("VALUE")
        shortname_match = re.match("(VLC[1-9][0-9][1-9])|(VLC_MC[0-9][0-9])|(EDU21)|(ROB1\\d{2})|(ASTRO ?\\d+)", shortname)
        return record.find("KEY[@name='categoryid']").findtext("VALUE") != "0" and shortname_match

    def load_next(self):
        if self.loaded:
            return None
        headers = {"User-Agent": self.user_agent}
        params = {"wstoken": self.wstoken, "wsfunction": "core_course_get_courses_by_field"}
        resp = requests.get(self.moodle_url + "/webservice/rest/server.php", params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        record_xml = ET.fromstring(resp.text)
        all_courses_xml = record_xml.find("SINGLE/KEY[@name='courses']").findall("MULTIPLE/SINGLE")
        courses_xml = list(filter(lambda record: self.should_import(record), all_courses_xml))
        self.loaded = True
        return courses_xml

    def to_search_index_metadata(self, record):
        # is "categoryname" a candidate for a subject-mapping?
        # "contacts" - authors or contributors or ..?
        # "filters" - keywords? -> no, does not seem to fit
        # wsfunction=core_course_get_contents gibt mehr infos über Inhalte, unter anderem Autor jeder einzelnen Datei, license dort ist nicht brauchbar (steht immer 'cc')
        record_id = record.find("KEY[@name='id']").findtext("VALUE")
        files_xml = record.find("KEY[@name='overviewfiles']/MULTIPLE").findall("SINGLE")
        images_xml = list(filter(lambda record: record.find("KEY[@name='mimetype']").findtext("VALUE").startswith("image/"), files_xml))
        image = images_xml[0].find("KEY[@name='fileurl']").findtext("VALUE").replace("/webservice/pluginfile.php", "/pluginfile.php") if len(images_xml) > 0 else None
        language_code = record.find("KEY[@name='lang']").findtext("VALUE") if record.find("KEY[@name='lang']").findtext("VALUE") else "en"
        meta = {
            "@context": [
                "https://w3id.org/kim/amb/context.jsonld", {"@language": "de"}
            ],
            "type": ["LearningResource"],
            "id": self.moodle_url + "/course/view.php?id=" + record_id,
            "name": record.find("KEY[@name='displayname']").findtext("VALUE"),
            "image": image,
            "description": html2text.html2text(record.find("KEY[@name='summary']").findtext("VALUE"), bodywidth=0),
            "inLanguage": [language_code],
            "license": {"id": "https://creativecommons.org/licenses/by/4.0/"},
            "learningResourceType": [{"id": Lrt.COURSE}],
            "sourceOrganization": [{"type": "Organization", "name": "RWTH Aachen", "id": "https://ror.org/04xfq0f34"}],
            "mainEntityOfPage": [
                {
                    "id": self.moodle_url + "/course/view.php?id=" + record_id,
                    "provider": {
                        "id": self.moodle_url,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ],
            "conditionsOfAccess": {"id": ConditionsOfAccess.LOGIN}
        }
        set_course_specific_values(record, meta)
        return meta


if __name__ == "__main__":
    VlcImport().process()
