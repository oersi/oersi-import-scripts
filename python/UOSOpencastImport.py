from OpencastHelpers import OpencastImportBase
from UOSOpencastMapping import UOSOpencastMapping


class UOSOpencastImport(OpencastImportBase):
    def __init__(self):
        super().__init__("Opencast Universität Osnabrück", "video4.virtuos.uni-osnabrueck.de", mapping=UOSOpencastMapping(), append_description_from_series=True)

    def filter_resource(self, record):
        if not self.get_description(record):
            return False
        if not self.get_subjects(record):
            return False
        return True

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        meta["sourceOrganization"] = [{"type": "Organization", "name": "Universität Osnabrück", "id": "https://ror.org/04qmmjx98"}]
        if not meta["image"]:
            meta["image"] = "https://lernfunk.de/logos/UOSde.svg"
        meta["encoding"] = [
            {
                "type": "MediaObject",
                "embedUrl": "https://video4.virtuos.uni-osnabrueck.de/paella/ui/embed.html?id=" + str(record["mediapackage"]["id"])
            }
        ]
        return meta


if __name__ == "__main__":
    UOSOpencastImport().process()
